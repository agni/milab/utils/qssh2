#include "qssh_dir_deleter.h"
#include "qssh_dir.h"
#include "qsshsftp_p.h"

#include <libssh2_sftp.h>
#include <QTimer>
#include <QElapsedTimer>
#include <QVector>

/* Implementing the deleter as a QThread didn't work because
 * we need to access the ssh socket. However, Qt forbids to access
 * sockets across threads.
 * The current implementation relies on signals/slots only
 */

class QSshDirDeleterPrivate
{
private:
	Q_DECLARE_PUBLIC(QSshDirDeleter)
	Q_DISABLE_COPY(QSshDirDeleterPrivate)

	enum class State
	{
		ready     = 0,
		listing   = 1,
		finishing = 2
	};

	QSshDirDeleter *q_ptr{ nullptr };
	QSshSftp *sftp{ nullptr };
	State status{ State::ready };
	QSshDir *dir{ nullptr };
	QList<QSshFileInfo> entries;
	QVector<QSshDirDeleter *> subs;

	QSshDirDeleterPrivate(QSshDirDeleter *q_ptr, QSshSftp *sftp);
	bool rm(const QString &path);
	int waitForFinish(unsigned long msecs);

private slots:
	void process();
	void onEntries(const QList<QSshFileInfo> &entries);
	void onListDone();
	void onFinished(int error);
};

QSshDirDeleterPrivate::QSshDirDeleterPrivate(QSshDirDeleter *q_ptr, QSshSftp *sftp)
    : q_ptr(q_ptr), sftp(sftp), status(State::ready)
{}

bool QSshDirDeleterPrivate::rm(const QString &path)
{
	Q_Q(QSshDirDeleter);
	if (dir && dir->name() == path)
		return true; // operating on path already
	if (status > State::ready)
		return false; // already processing another path
	status = State::listing; // indicate deletion in process

	Q_ASSERT(!dir);
	dir = sftp->dir(path);
	q->connect(dir, &QSshDir::entries, q, [&](const QList<QSshFileInfo> &entries) { onEntries(entries); });
	q->connect(dir, &QSshDir::closed, q, [&]() { onListDone(); });
	dir->list();

	return true;
}

void QSshDirDeleterPrivate::onEntries(const QList<QSshFileInfo> &entries)
{
	this->entries += entries;
	QTimer::singleShot(0, [this]() { process(); });
}

/* It is important to factor out the actual processing from onEntries().
 * If onEntries(), which is triggered from QSshSftp::activate(), makes
 * QSshSftp calls itself (and thus triggers QSshSftp::activate() again)
 * libssh2 is sporadically intermixing different calls.
 * To this end, process needs to be triggered via QTimer::singleShot() only!
 */
void QSshDirDeleterPrivate::process()
{
	Q_Q(QSshDirDeleter);
	Q_ASSERT(entries.empty() || dir != nullptr);

	QSignalBlocker block(sftp); // block signals, particularly removed()
	QStringList removed;
	for (const QSshFileInfo &info : qAsConst(entries)) {
		if (info.name == "." || info.name == "..")
			continue;
		QString name = dir->name() + '/' + info.name;
		if (info.type == QSshFileInfo::FileTypeDirectory) {
			QSshDirDeleter *sub = new QSshDirDeleter(sftp);
			subs.push_back(sub);
			q->connect(sub, &QSshDirDeleter::finished, q, [&](const int error) { onFinished(error); });
			subs.back()->rm(name);
		} else {
			try {
				sftp->unlink(name);
				removed.push_back(info.name);
			} catch (const QSshSftp::exception &e) {
				qDebug() << "error deleting dir: \n" << e.error() << "\t" << e.what();
			}
		}
	}
	entries.clear();
	block.unblock();
	if (!removed.empty())
		Q_EMIT sftp->removed(dir->name(), removed);

	// can we finalize?
	if (status != State::finishing || !subs.empty())
		return;

	// finally remove directory
	try {
		sftp->rmdir(dir->name());
		status = State::ready;
	} catch (const QSshSftp::exception &e) {
		status = static_cast<State>(e.error());
		qDebug() << "error deleting dir: \n" << e.error() << "\t" << e.what();
	}
	dir->deleteLater();
	dir = nullptr;

	int error = static_cast<int>(status);
	status    = State::ready; // allow re-use of this deleter
	Q_EMIT q->finished(error);
}

void QSshDirDeleterPrivate::onListDone()
{
	status = State::finishing;
	QTimer::singleShot(0, [this]() { process(); });
}

void QSshDirDeleterPrivate::onFinished(int /*unused*/)
{
	Q_Q(QSshDirDeleter);
	Q_ASSERT(dynamic_cast<QSshDirDeleter *>(q->sender()));
	QSshDirDeleter *sub = static_cast<QSshDirDeleter *>(q->sender());
	subs.removeOne(sub);
	delete sub;
	QTimer::singleShot(0, [this]() { process(); });
}

int QSshDirDeleterPrivate::waitForFinish(unsigned long msecs)
{
	QElapsedTimer stopWatch;
	stopWatch.start();

	while (status > State::ready && msecs - stopWatch.elapsed() > 0) {
		sftp->d_ptr->waitForReadOrWrite(msecs - stopWatch.elapsed());
	}
	// return true, when we finished, false when we timed out
	return status <= State::ready;
}

QSshDirDeleter::QSshDirDeleter(QSshSftp *sftp, QObject *parent)
    : QObject(parent), d_ptr(new QSshDirDeleterPrivate(this, sftp))
{}

QSshDirDeleter::~QSshDirDeleter() = default;

bool QSshDirDeleter::rm(const QString &path)
{
	return d_ptr->rm(path);
}

int QSshDirDeleter::waitForFinish(unsigned long time)
{
	return d_ptr->waitForFinish(time);
}

QString QSshDirDeleter::path() const
{
	return d_ptr->dir ? d_ptr->dir->name() : QString();
}
