#include "qssh_file.h"
#include "qssh_dir.h"
#include <libssh2_sftp.h>
#include <QByteArray>

class QSshFilePrivate
{
public:
	QSshFilePrivate(QSshFile *q_ptr, const QString &name, QSshSftp *sftp);
	~QSshFilePrivate();

	QSshFile *const q_ptr;
	const QString name;
	QSshSftp *const sftp; // pointer to owning sftp object
	LIBSSH2_SFTP_HANDLE *sftp_handle; // libssh file handle
	int last_error = 0;
	unsigned long ssh_open_flags;
	QByteArray buffer; // buffer for received / to-send data
	quint64 last_write_size;

	enum STATE
	{
		IDLE,
		OPEN,
		READ,
		AT_END,
		WRITE,
		CLOSE
	};
	STATE state;

	bool activate();
	bool waitForReadOrWrite(int msecs);
	int lastError() const;
};

class QSshDirPrivate
{
public:
	QSshDirPrivate(QSshDir *q_ptr, const QString &name, QSshSftp *sftp);
	~QSshDirPrivate();

	QSshDir *const q_ptr;
	const QString name;
	QSshSftp *const sftp; // pointer to owning sftp object
	LIBSSH2_SFTP_HANDLE *sftp_handle; // libssh file handle
	int last_error = 0;

	enum STATE
	{
		IDLE,
		OPEN,
		OPENED,
		READ,
		CLOSE
	};
	STATE state, next;

	bool activate();
	bool waitForReadOrWrite(int msecs);
	int lastError() const;
};
