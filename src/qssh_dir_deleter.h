#pragma once

#include "qssh2_export.h"

#include <QObject>

class QSshSftp;
class QSshDirDeleterPrivate;
class QSSH2_EXPORT QSshDirDeleter : public QObject
{
	Q_OBJECT

public:
	QSshDirDeleter(QSshSftp *sftp, QObject *parent = nullptr);
	~QSshDirDeleter();
	bool rm(const QString &path);
	int waitForFinish(unsigned long time = ULONG_MAX);
	QString path() const;

signals:
	void finished(int error_code);

private:
	Q_DECLARE_PRIVATE(QSshDirDeleter)
	Q_DISABLE_COPY(QSshDirDeleter)
	const QScopedPointer<QSshDirDeleterPrivate> d_ptr;
};
