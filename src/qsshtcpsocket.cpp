#include "qsshtcpsocket.h"
#include "qsshchannel_p.h"

/*!
    \class QSshTcpSocket
    \inmodule QNetwork
    \brief The QSshTcpSocket class provides a TCP socket tunneled over an SSH connection

    QSshTcpSocket enables an SSH client to establish a TCP connection between the SSH server and
    a remote host. Traffic over this TCP connection is tunneled through the channel.

    Note that traffic between the SSH server and the remote host is unencrypted. Only
    communication between QSshClient and the SSH server is encrypted.

    QSshTcpSocket objects are created using the QSshClient::openTcpSocket() method.
*/

/*!
 *
 */
QSshTcpSocket::QSshTcpSocket(QSshClient *parent) : QSshChannel(parent) {}
