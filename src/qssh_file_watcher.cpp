#include "qssh_file_watcher.h"
#include "qssh_connection_manager.h"
#include <qsshsftp.h>
#include <libssh2_sftp.h>
#include <QHostInfo>
#include <QSshDir>
#include <QThread>
#include <QTimer>
#include <QUrl>
#include <QDir>
#include <QEventLoop>
#include <QPointer>

class QSshFileWatcherPrivate
{
	Q_DECLARE_PUBLIC(QSshFileWatcher)

protected:
	QSshFileWatcher *q_ptr;
	QSshConnectionManager *conn_manager;
	QUrl url;
	QSharedPointer<QSshClient> client;
	bool own_sftp; // do we own the sftp pointer?
	QPointer<QSshSftp> sftp;
	QStringList files;
	QTimer timer;

public:
	QSshFileWatcherPrivate(QSshFileWatcher *q_ptr, QSshConnectionManager *manager) : q_ptr(q_ptr), conn_manager(manager)
	{}
	virtual ~QSshFileWatcherPrivate()
	{
		if (own_sftp)
			delete sftp;
	}

	bool connect();
	void disconnect();

	void _q_onRemoteFileFound(const QList<QSshFileInfo> &entries, const QUrl &url, const QString &prefix,
	                          const QString &suffix);
};

QSshFileWatcher::QSshFileWatcher(QSshFileWatcherPrivate *d_ptr, QObject *parent) : QObject(parent), d_ptr(d_ptr)
{
	connect(&d_ptr->timer, &QTimer::timeout, this, &QSshFileWatcher::check);
	setInterval(1000);
}

QSshFileWatcher::QSshFileWatcher(const QPointer<QSshSftp> &sftp, QObject *parent)
    : QSshFileWatcher(new QSshFileWatcherPrivate(this, nullptr), parent)
{
	Q_D(QSshFileWatcher);
	d->sftp     = sftp;
	d->own_sftp = !sftp;
}

QSshFileWatcher::QSshFileWatcher(const QUrl &url, QSshConnectionManager *manager, QObject *parent)
    : QSshFileWatcher(new QSshFileWatcherPrivate(this, manager), parent)
{
	Q_D(QSshFileWatcher);
	d->url      = url;
	d->own_sftp = true;
}

QSshFileWatcher::~QSshFileWatcher()
{
	delete d_ptr;
}

void QSshFileWatcher::watch(const QString &path)
{
	Q_D(QSshFileWatcher);

	if (d->files.contains(path))
		return;
	d->files.push_back(path);

	d->connect();

	if (!d->timer.isActive())
		d->timer.start();
}

void QSshFileWatcher::unwatch(const QString &path)
{
	Q_D(QSshFileWatcher);

	d->files.removeOne(path);
	Q_ASSERT(!d->files.contains(path));

	// close connection after a while
	if (d->files.isEmpty() && d->own_sftp)
		QTimer::singleShot(10000, this, [d]() { d->disconnect(); });
}

void QSshFileWatcher::unwatchAll()
{
	Q_D(QSshFileWatcher);
	d->files.clear();
	// try to close connection after a while
	if (d->own_sftp)
		QTimer::singleShot(10000, this, [d]() { d->disconnect(); });
}

bool QSshFileWatcherPrivate::connect()
{
	if (QSshFileWatcher::isLocalHost(url) || (sftp && sftp->isConnected()) || !own_sftp)
		return true;

	if (!client)
		client = conn_manager->open(url, true);
	if (!client->isConnected()) {
		// wait for connection
		QObject::connect(client.data(), &QSshClient::connected, q_ptr, [this]() {
			QObject::disconnect(client.data(), &QSshClient::connected, q_ptr, nullptr);
			connect(); // continue connecting
		});
		return false;
	}

	if (!sftp) {
		sftp = client->openSftpChannel();
		QObject::connect(sftp.data(), &QSshSftp::connected, q_ptr,
		                 [this]() { QObject::disconnect(sftp.data(), &QSshSftp::connected, q_ptr, nullptr); });
		return false;
	}
	return sftp->isConnected();
}

void QSshFileWatcherPrivate::disconnect()
{
	if (files.isEmpty())
		sftp.clear();
}

void QSshFileWatcher::check()
{
	Q_D(QSshFileWatcher);
	d->connect(); // connect if not yet done

	QVector<QSshFileInfo> results;
	for (const auto &file : qAsConst(d->files))
		results.push_back(info(file));

	if (results.isEmpty())
		d->timer.stop();
	Q_EMIT update(results);
}

void QSshFileWatcher::setInterval(int msec)
{
	Q_D(QSshFileWatcher);
	d->timer.setInterval(msec);
}

int QSshFileWatcher::interval() const
{
	Q_D(const QSshFileWatcher);
	return d->timer.interval();
}

QSshFileInfo QSshFileWatcher::info(const QString &path)
{
	Q_D(QSshFileWatcher);
	bool local = !d->sftp && d->own_sftp;

	if (local)
		return QFileInfo(path);

	QSshFileInfo info;
	if (d->sftp && d->sftp->isConnected()) {
		try {
			return d->sftp->statFile(path);
		} catch (const QSshSftp::exception &e) {
			info.type = QSshFileInfo::FileTypeUnknown;
		}
	} else
		info.type = QSshFileInfo::FileTypeNotFetched;

	info.name     = path;
	info.size     = 0;
	info.hostName = d->url.host();
	return info;
}

class QSshThreadedFileWatcherPrivate : public QSshFileWatcherPrivate
{
	friend class QSshThreadedFileWatcher;

	QThread thread;

	QSshThreadedFileWatcherPrivate(QSshThreadedFileWatcher *q_ptr, QSshConnectionManager *manager)
	    : QSshFileWatcherPrivate(q_ptr, manager)
	{}
};

QSshThreadedFileWatcher::QSshThreadedFileWatcher(const QUrl &url, QSshConnectionManager *manager, QObject *parent)
    : QSshFileWatcher(new QSshThreadedFileWatcherPrivate(this, manager), parent)
{
	Q_D(QSshThreadedFileWatcher);
	d->url      = url;
	d->own_sftp = true;
	d->thread.start();
}

QSshThreadedFileWatcher::~QSshThreadedFileWatcher()
{
	Q_D(QSshThreadedFileWatcher);
	d->thread.quit();
	d->thread.wait();
}

void QSshThreadedFileWatcher::watch(const QString &path)
{
	// call QSshFileWatcher::watch in threaded event loop
	QTimer::singleShot(0, this, [this, path]() { QSshFileWatcher::watch(path); });
}

void QSshThreadedFileWatcher::unwatch(const QString &path)
{
	// call QSshFileWatcher::unwatch in threaded event loop
	QTimer::singleShot(0, this, [this, path]() { QSshFileWatcher::unwatch(path); });
}

void QSshThreadedFileWatcher::unwatchAll()
{
	// call QSshFileWatcher::unwatchAll in threaded event loop
	QTimer::singleShot(0, this, [this]() { QSshFileWatcher::unwatchAll(); });
}

void QSshThreadedFileWatcher::check()
{
	// call QSshFileWatcher::check in threaded event loop
	QTimer::singleShot(0, this, [this]() { QSshFileWatcher::check(); });
}

#include "moc_qssh_file_watcher.cpp"
