#pragma once

#include <QSshSftp>
#include <qssh2_export.h>

class QSSH2_EXPORT QSshExtendedInformation
{
public:
	enum Type
	{
		Dir,
		File,
		System
	};

	QSshExtendedInformation() {}
	QSshExtendedInformation(const QSshFileInfo &info) : mFileInfo(info) {}

	inline bool isDir() { return type() == Dir; }
	inline bool isFile() { return type() == File; }
	inline bool isSystem() { return type() == System; }

	bool operator==(const QSshExtendedInformation &fileInfo) const
	{
		return mFileInfo == fileInfo.mFileInfo && displayType == fileInfo.displayType &&
		       permissions() == fileInfo.permissions();
	}

	QFile::Permissions permissions() const { return mFileInfo.permissions; }

	Type type() const
	{
		if (mFileInfo.isDir()) {
			return QSshExtendedInformation::Dir;
		}
		if (mFileInfo.isFile()) {
			return QSshExtendedInformation::File;
		}
		if (mFileInfo.isSymLink()) {
			return QSshExtendedInformation::System;
		}
		return QSshExtendedInformation::System;
	}

	bool isSymLink(bool ignoreNtfsSymLinks = false) const
	{
		if (ignoreNtfsSymLinks) {
#ifdef Q_WS_WIN
			return !mFileInfo.suffix().compare(QLatin1String("lnk"), Qt::CaseInsensitive);
#endif
		}
		return mFileInfo.isSymLink();
	}

	bool isHidden() const
	{
		// return mFileInfo.isHidden(); // TODO do we need this feature? i think this can be done
		return false;
	}

	QSshFileInfo fileInfo() const { return mFileInfo; }

	QDateTime lastModified() const { return mFileInfo.mtime; }

	qint64 size() const
	{
		qint64 size = -1;
		if (type() == QSshExtendedInformation::Dir)
			size = 0;
		if (type() == QSshExtendedInformation::File)
			size = mFileInfo.size;
		if (mFileInfo.isSymLink())
			size = -1;
		return size;
	}

	QString displayType;
	QIcon icon;

private:
	QSshFileInfo mFileInfo;
};
