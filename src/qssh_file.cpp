#define NOMINMAX
#include "qssh_file_p.h"
#include "qsshsftp_p.h"
#include <qelapsedtimer.h>

QSshFilePrivate::QSshFilePrivate(QSshFile *q_ptr, const QString &name, QSshSftp *sftp)
    : q_ptr(q_ptr), name(name), sftp(sftp), sftp_handle(nullptr), last_write_size(0), state(IDLE)
{}

QSshFilePrivate::~QSshFilePrivate()
{
	// remove me from sftp's list
	sftp->d_ptr->files.removeOne(q_ptr);
}

bool QSshFilePrivate::activate()
{
	int rc = 0;
	char data[1024];

	switch (state) {
		case IDLE:
			return true;
		case OPEN:
			buffer.clear();
			sftp_handle = libssh2_sftp_open(sftp->d_ptr->sftp, qUtf8Printable(name), ssh_open_flags,
			                                LIBSSH2_SFTP_S_IRUSR | LIBSSH2_SFTP_S_IWUSR);
			if (!sftp_handle) {
				rc = libssh2_session_last_errno(sftp->d_ptr->session);
				if (rc < 0 && rc != LIBSSH2_ERROR_EAGAIN)
					state = IDLE;
			} else {
				state = ssh_open_flags & LIBSSH2_FXF_WRITE ? WRITE : READ;
				q_ptr->QIODevice::open(ssh_open_flags & LIBSSH2_FXF_WRITE ? QIODevice::WriteOnly : QIODevice::ReadOnly);
				Q_EMIT q_ptr->opened();
				return activate();
			}
			break;
		case READ:
			while ((rc = libssh2_sftp_read(sftp_handle, data, sizeof(data))) > 0)
				buffer.append(data, rc);
			if (buffer.size())
				Q_EMIT q_ptr->readyRead();

			if (rc == 0)
				state = AT_END;
			break;
		case WRITE:
			if (last_write_size == 0) // can write new chunk
				last_write_size = buffer.size();
			if ((rc = libssh2_sftp_write(sftp_handle, buffer.constData(), last_write_size)) > 0) {
				buffer.remove(0, rc);
				last_write_size = buffer.size();
				Q_EMIT q_ptr->bytesWritten(rc);

				// trigger next write
				if (last_write_size > 0)
					return activate();
			}
			break;
		case CLOSE:
			if ((rc = libssh2_sftp_close(sftp_handle)) == 0) {
				sftp_handle = nullptr;
				state       = IDLE;
				q_ptr->QIODevice::close();
			}
			break;
		case AT_END:
			break;
	}
	if (rc < 0 && rc != LIBSSH2_ERROR_EAGAIN) {
		if (rc == LIBSSH2_ERROR_SFTP_PROTOCOL)
			Q_EMIT q_ptr->error(last_error = libssh2_sftp_last_error(sftp->d_ptr->sftp));
		return false;
	}
	return true;
}

inline int QSshFilePrivate::lastError() const
{
	return last_error;
}

inline bool QSshFilePrivate::waitForReadOrWrite(int msecs)
{
	return sftp->d_ptr->waitForReadOrWrite(msecs);
}

QSshFile::QSshFile(const QString &file_name, QSshSftp *sftp) : d_ptr(new QSshFilePrivate(this, file_name, sftp)) {}

QSshFile::~QSshFile()
{
	QSshFile::close();
	while (d_ptr->state != QSshFilePrivate::IDLE)
		d_ptr->waitForReadOrWrite(10);

	delete d_ptr;
}

using Mapping = QMap<QIODevice::OpenModeFlag, unsigned long>;
static Mapping initModeFlagMapping()
{
	Mapping map;
	map.insert(QIODevice::WriteOnly, LIBSSH2_FXF_WRITE | LIBSSH2_FXF_CREAT);
	map.insert(QIODevice::ReadOnly, LIBSSH2_FXF_READ);
	map.insert(QIODevice::Truncate, LIBSSH2_FXF_TRUNC);
	map.insert(QIODevice::Append, LIBSSH2_FXF_APPEND);
	return map;
}

static unsigned long convert_flags(QIODevice::OpenMode &qt_flags)
{
	static const Mapping map = initModeFlagMapping();
	QIODevice::OpenMode handled;
	unsigned long result = 0;

	for (Mapping::const_iterator it = map.constBegin(), end = map.constEnd(); it != end; ++it) {
		if (qt_flags & it.key()) {
			handled |= it.key();
			result |= it.value();
		}
	}
	if (qt_flags != handled)
		throw std::invalid_argument(qPrintable(QString("invalid file mode: %1").arg(qt_flags)));

	return result;
}

bool QSshFile::open(QIODevice::OpenMode mode)
{
	if (isOpen())
		return false;
	d_ptr->ssh_open_flags = convert_flags(mode);

	d_ptr->state = QSshFilePrivate::OPEN;
	d_ptr->activate();
	return true;
}

void QSshFile::close()
{
	if (d_ptr->state == QSshFilePrivate::IDLE || d_ptr->state == QSshFilePrivate::CLOSE)
		return;

	waitForBytesWritten(100);
	d_ptr->buffer.clear();

	d_ptr->state = QSshFilePrivate::CLOSE;
	d_ptr->activate();
}

bool QSshFile::flush(int msecs)
{
	return waitForBytesWritten(msecs);
}

qint64 QSshFile::bytesAvailable() const
{
	return d_ptr->buffer.size() + QIODevice::bytesAvailable();
}

bool QSshFile::canReadLine() const
{
	return d_ptr->buffer.contains('\n') || QIODevice::canReadLine();
}

qint64 QSshFile::bytesToWrite() const
{
	return d_ptr->buffer.size() + QIODevice::bytesToWrite();
}

bool QSshFile::atEnd() const
{
	return d_ptr->state == QSshFilePrivate::AT_END;
}

int QSshFile::lastError() const
{
	return d_ptr->lastError();
}

bool QSshFile::waitForOpened(int msecs)
{
	if (isOpen())
		return true;

	QElapsedTimer stopWatch;
	stopWatch.start();
	do {
		d_ptr->waitForReadOrWrite(msecs - stopWatch.elapsed());
		if (isOpen())
			return true;
	} while (msecs == -1 || msecs - stopWatch.elapsed() > 0);
	return false;
}

bool QSshFile::waitForReadyRead(int msecs)
{
	if (bytesAvailable())
		return true;

	QElapsedTimer stopWatch;
	stopWatch.start();
	do {
		d_ptr->waitForReadOrWrite(msecs - stopWatch.elapsed());
		if (bytesAvailable())
			return true;
	} while (msecs == -1 || msecs - stopWatch.elapsed() > 0);
	return false;
}

bool QSshFile::waitForBytesWritten(int msecs)
{
	if (bytesToWrite() == 0)
		return true;

	QElapsedTimer stopWatch;
	stopWatch.start();
	do {
		d_ptr->waitForReadOrWrite(msecs - stopWatch.elapsed());
		if (bytesToWrite() == 0)
			return true;
	} while (msecs == -1 || msecs - stopWatch.elapsed() > 0);
	return false;
}

qint64 QSshFile::readData(char *data, qint64 max_size)
{
	int size = std::min(static_cast<int>(max_size), d_ptr->buffer.size());
	memcpy(data, d_ptr->buffer.constData(), size);
	d_ptr->buffer.remove(0, size);
	return size;
}

qint64 QSshFile::writeData(const char *data, qint64 max_size)
{
	d_ptr->buffer.append(data, max_size);
	d_ptr->activate();

	return 0;
}
