#pragma once

#include "qssh2_export.h"
#include "qsshsftp.h"

#include <QObject>
#include <QHostInfo>
#include <QUrl>

class QSshFileWatcherPrivate;
class QSshConnectionManager;

/** Watch files locally or remotely.
 *  Regularly fetch file infos for watched files and emit update() */
class QSSH2_EXPORT QSshFileWatcher : public QObject
{
	Q_OBJECT
	Q_DECLARE_PRIVATE(QSshFileWatcher)

protected:
	QSshFileWatcherPrivate *d_ptr;

public:
	QSshFileWatcher(const QPointer<QSshSftp> &sftp, QObject *parent = nullptr);
	QSshFileWatcher(const QUrl &url, QSshConnectionManager *manager, QObject *parent = nullptr);
	~QSshFileWatcher();

	/// Watch the given \a file, remotely or on local host.
	/// If not yet connected, the file is scheduled for later monitoring
	void watch(const QString &path);
	/// Unwatch a file. Both host, and filename must match a watched file entry
	void unwatch(const QString &path);
	/// Unwatch all watched files
	void unwatchAll();

	void setInterval(int msec);
	int interval() const;

	/// Fetches the QSshFileInfo for \a url, remotely or on local host
	/// If connection to host failed, QSshFileInfo will be empty and type will be FileTypeNotFetched
	QSshFileInfo info(const QString &path);

	static bool isLocalHost(const QUrl &url)
	{
		const QString &host = url.host();
		return host.isEmpty() || host == "localhost" || host == "127.0.0.1" || host == QHostInfo::localHostName();
	}

public slots:
	void check();

protected:
	QSshFileWatcher(QSshFileWatcherPrivate *d_ptr, QObject *parent);

signals:
	/// Emitted in response to check()
	void update(const QVector<QSshFileInfo> &);
};

class QSshThreadedFileWatcherPrivate;
/** Threaded version of QSshFileWatcher
 *  Process all events in an event loop running in a separate thread */
class QSSH2_EXPORT QSshThreadedFileWatcher : public QSshFileWatcher
{
	Q_OBJECT
	Q_DECLARE_PRIVATE(QSshThreadedFileWatcher)

public:
	QSshThreadedFileWatcher(const QUrl &url, QSshConnectionManager *manager, QObject *parent = nullptr);
	~QSshThreadedFileWatcher();

	void watch(const QString &path);
	void unwatch(const QString &path);
	void unwatchAll();
	void check();

private:
	// hide info() as it cannot run asynchronously in the thread
	using QSshFileWatcher::info;
};
