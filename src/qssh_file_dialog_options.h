/****************************************************************************
** This Class removes the need to use QPlatformDialogHelper in the
** QSshFileDialog class. Since QPlatformDialogHelper is not guaranteed to work
** in future releases of Qt. This helper class stores some settings for
** QSshFileDialog and is just a simple copy of the corresponding Qt Class.
**
****************************************************************************/
#pragma once
#include <QDir>
#include <QFileDialog>

class QUrl;
class QString;
class QStringList;

class QSshFileDialogOptionsPrivate;
class QSshFileDialogOptions
{
public:
	enum QSshDialogLabel
	{
		LookIn,
		FileName,
		FileType,
		Accept,
		Reject,
		DialogLabelCount
	};

	enum class FontDialogOption
	{
		NoButtons           = 0x00000001,
		DontUseNativeDialog = 0x00000002,
		ScalableFonts       = 0x00000004,
		NonScalableFonts    = 0x00000008,
		MonospacedFonts     = 0x00000010,
		ProportionalFonts   = 0x00000020
	};

	QSshFileDialogOptions();
	~QSshFileDialogOptions();
	QUrl initialDirectory() const;
	void setInitialDirectory(const QUrl &);

	void setLabelText(QSshDialogLabel label, const QString &text);
	QString labelText(QSshDialogLabel label) const;
	bool isLabelExplicitlySet(QSshDialogLabel label);

	QDir::Filters filter() const;
	void setFilter(QDir::Filters filters);

	void setNameFilters(const QStringList &filters);
	QStringList nameFilters() const;

	void setDefaultSuffix(const QString &suffix);
	QString defaultSuffix() const;

	void setViewMode(QFileDialog::ViewMode mode);
	QFileDialog::ViewMode viewMode() const;

	void setMimeTypeFilters(const QStringList &filters);
	QStringList mimeTypeFilters() const;

	QString initiallySelectedNameFilter() const;
	void setInitiallySelectedNameFilter(const QString &);

	QList<QUrl> initiallySelectedFiles() const;
	void setInitiallySelectedFiles(const QList<QUrl> &);

	void setFileMode(QFileDialog::FileMode mode);
	QFileDialog::FileMode fileMode() const;

	void setAcceptMode(QFileDialog::AcceptMode mode);
	QFileDialog::AcceptMode acceptMode() const;

	void setOption(QFileDialog::Options option, bool on = true);
	bool testOption(QFileDialog::Options option) const;
	void setOptions(QFileDialog::Options options);
	QFileDialog::Options options() const;

	static QString defaultNameFilterString();
	static QStringList cleanFilterList(const QString &filter);
	static const char filterRegExp[];

private:
	Q_DECLARE_PRIVATE(QSshFileDialogOptions)
	Q_DISABLE_COPY(QSshFileDialogOptions)
	const QScopedPointer<QSshFileDialogOptionsPrivate> d_ptr;
};
