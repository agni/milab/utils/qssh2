#include "qsshprocess.h"
#include "qsshchannel_p.h"

/*!
    \class QSshProcess
    \inmodule QNetwork
    \brief The QSshProcess class allows communication with a process invoked on an SSH server

    QSshProcess is an I/O device analogous to QProcess. It can invoke a command or SSH subsystem on
    the SSH server; the stdin, stdout, and stderr channels of the process are redirected through the
    channel.

    QSshProcess objects are created using the QSshClient::openProcessChannel() method.
*/

/*!
 * \enum QSshProcess::TerminalType
 * \value VanillaTerminal   A terminal with no special features
 * \value Vt102Terminal     A terminal that understands VT102 command codes
 * \value AnsiTerminal      A terminal that understands ANSI command codes
 */

/*!
 * \fn QSshProcess::finished ( int exitCode )
 *
 * This signal is emitted when the process on the SSH server finishes. The parameter is the exit code of
 * the process. After the process has finished, the read buffer QSshProcess will still contain any
 * data the process may have written before terminating.
 *
 * Another process or subsystem may be invoked on the same channel after the first process terminates.
 */

/*!
 * \fn QSshProcess::started ()
 *
 * This signal is emitted by QSshProcess when the remote process has started.
 */

QSshProcess::QSshProcess(QSshClient *parent) : QSshChannel(parent)
{
	d->openSession();
	connect(this, &QSshProcess::disconnected, this, &QSshProcess::onDisconnected);
}

/*!
 * \fn QSshProcess::onDisconnected
 *
 * Catches disconnected sent by QSshChannel after execution is finished.
 */

void QSshProcess::onDisconnected()
{
	Q_EMIT finished(libssh2_channel_get_exit_status(d->d_channel));
	d->d_state = d->OPEN; // make process OPEN, so it can be reused
}

/*!
 * Starts a login shell on the SSH server.
 *
 * If there is already a process running on this channel, the behavior is undefined.
 */
void QSshProcess::startShell()
{
	d->startShell();
}
/*!
 * Invokes a shell command on the SSH server.
 *
 * If there is already a process running on this channel, the behavior is undefined.
 */
void QSshProcess::start(const QString &cmd)
{
	d->start(cmd);
}
void QSshProcess::terminate()
{
	d->close();
}
/*!
 * Requests that a PTY be allocated for this channel on the remote host.
 *
 * This function must be invoked before starting the process that requires it.
 */
void QSshProcess::requestPty(TerminalType term)
{
	switch (term) {
		case VanillaTerminal:
			d->requestPty("vanilla");
			break;
			;
		case Vt102Terminal:
			d->requestPty("vt102");
			break;
			;
		case AnsiTerminal:
			d->requestPty("ansi");
			break;
			;
	}
}
