#include "qsshchannel_p.h"
#include "qsshsftp_p.h"
#include "qsshprocess.h"
#include "qsshtcpsocket.h"
#include <QTimer>
#include <QNetworkProxy>
#include <qpointer.h>

/*!
    \class QSshClient
    \inmodule QNetwork
    \brief The QSshClient class implements a Secure Shell client

    QSshClient allows connecting to any standard SSH server.

    It provides facilities for password authentication or public key authentication, and
    provides methods to open a shell or a TCP socket on the remote host.

    The passphrase can be set before connecting or it can be provided in response to the
    authenticationRequired() signal. This allows the password to be stored in advance or
    prompted for only when needed.

    QSshClient is based on the third-party library libssh2 (http://www.libssh2.org/)
    provided under the following license:
   \code
    * Copyright (c) 2004-2007 Sara Golemon <sarag@libssh2.org>
    * Copyright (c) 2005,2006 Mikhail Gusarov <dottedmag@dottedmag.net>
    * Copyright (c) 2006-2007 The Written Word, Inc.
    * Copyright (c) 2007 Eli Fant <elifantu@mail.ru>
    * Copyright (c) 2009 Daniel Stenberg
    * Copyright (C) 2008, 2009 Simon Josefsson
    * All rights reserved.
    *
    * Redistribution and use in source and binary forms,
    * with or without modification, are permitted provided
    * that the following conditions are met:
    *
    *   Redistributions of source code must retain the above
    *   copyright notice, this list of conditions and the
    *   following disclaimer.
    *
    *   Redistributions in binary form must reproduce the above
    *   copyright notice, this list of conditions and the following
    *   disclaimer in the documentation and/or other materials
    *   provided with the distribution.
    *
    *   Neither the name of the copyright holder nor the names
    *   of any other contributors may be used to endorse or
    *   promote products derived from this software without
    *   specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
    * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
    * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
    * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
    * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
    * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
    * OF SUCH DAMAGE.
    \endcode
*/

/*! \enum QSshClient::AuthenticationMethod
 *
 * \value PasswordAuthentication    Authenticate using a password
 * \value PublicKeyAuthentication   Authenticate using a public key
 */

/*! \enum QSshClient::Error

   \value AuthenticationError       An error occurred while authenticating.
   \value HostKeyUnknownError       The host key is unknown.
   \value HostKeyInvalidError       The host key is invalid.
   \value HostKeyMismatchError      The host key does not match the key recorded in the known hosts.
   \value ConnectionRefusedError    The connection was refused.
   \value UnexpectedShutdownError   The connection was unexpectedly shut down.
   \value HostNotFoundError         The host could not be found.
   \value SocketError               An error occurred on the underlying socket.
   \value UnknownError              An unknown error occurred.
 */

/*! \enum QSshClient::KnownHostsFormat
 *
 * \value OpenSslFormat             Use the .ssh/known_hosts file format.
 */

/*!
 * \fn QSshClient::connected()
 *
 * This signal is emitted when a connection to the SSH server has been successfully established.
 */

/*!
 * \fn QSshClient::disconnected()
 *
 * This signal is emitted when the connection to the SSH server has been closed.
 */

/*!
 * \fn QSshClient::error ( QSshClient::Error error )
 *
 * This signal is emitted after an error occurs. The error parameter describes the type of error that occurred.
 */

/*!
 * Constructs a new QSshClient with the specified parent.
 */
QSshClient::QSshClient(QObject *parent) : QObject(parent), d(new QSshClientPrivate)
{
	d->p = this;
	d->setProxy(QNetworkProxy::NoProxy);
}

/*!
 * Destroys the QSshClient object.
 */
QSshClient::~QSshClient()
{
	delete d;
}

_LIBSSH2_KNOWNHOSTS *QSshClient::init_knownhosts()
{
	static LIBSSH2_SESSION *dummy = nullptr;
	if (!dummy)
		dummy = libssh2_session_init_ex(nullptr, nullptr, nullptr, nullptr);
	LIBSSH2_KNOWNHOSTS *result = libssh2_knownhost_init(dummy);
	// libssh2_session_free(dummy);
	return result;
}
/*!
 * Attempts to make a connection to host on the given port as the specified user.
 *
 * QSshClient will attempt to authenticate first using the public key, if
 * provided, then using the password, if provided. If neither of these methods
 * succeed in authenticating, the authenticationRequired() signal will be emitted.
 * The public key and/or password can be changed in a slot directly connected to
 * this signal. After the signal has resolved, the public key and password will be
 * tried again.
 *
 * The connected() signal will be emitted when the connection is complete. At any
 * point, QSshClient can emit error() to signal that an error occurred.
 *
 * The host parameter can be an IP address or a host name. Host names will be
 * resolved by the server.
 */
void QSshClient::connectToHost(const QString &host, const QString &user, int port)
{
	// do not connect again to same host as same user on same port
	if (d->d_hostName == host && d->d_port == port && d->d_state > QSshClientPrivate::CONNECT)
		return;
	if (d->d_state > QSshClientPrivate::DISCONNECTED)
		disconnectFromHost();

	d->d_hostName = host;
	setUserName(user);
	d->d_port  = port;
	d->d_state = QSshClientPrivate::CONNECT;
	d->connectToHost(host, port);
}

/*!
 * Disconnects the current SSH connection.
 *
 * Any open channels will be closed. When the connection has been closed, the
 * disconnected() signal will be emitted.
 */
void QSshClient::disconnectFromHost()
{
	d->d_reset();
}

bool QSshClient::isConnected() const
{
	return d->d_state == QSshClientPrivate::CONNECTED;
}

/*!
 * If not yet connected, change userName for connection
 */
void QSshClient::setUserName(const QString &username)
{
	QString new_name;
	if (username.isEmpty()) {
// username is different on Linux and Windows
#if defined(Q_OS_WIN)
		new_name = qgetenv("USERNAME");
#else
		new_name = qgetenv("USER");
#endif
	} else
		new_name = username;
	if (d->d_userName == new_name || d->d_state >= QSshClientPrivate::CONNECTED)
		return;

	d->d_userName = new_name;

	// if we already started connecting, restart ssh protocol
	if (d->d_state >= QSshClientPrivate::STARTUP_SESSION) {
		QSignalBlocker blocker(this);
		d->d_reset();
		d->d_state = QSshClientPrivate::CONNECT;
		d->connectToHost(d->d_hostName, d->d_port);
	}
}

/*!
 * If not yet connected, change hostName and port for connection
 */
void QSshClient::setConnection(const QString &hostName, const int &port)
{
	if (d->d_state >= QSshClientPrivate::CONNECTED)
		return;

	if (d->d_hostName != hostName && !hostName.isEmpty())
		d->d_hostName = hostName;

	if (d->d_port != port)
		d->d_port = port;

	// if we already started connecting, restart ssh protocol
	if (d->d_state >= QSshClientPrivate::STARTUP_SESSION) {
		QSignalBlocker blocker(this);
		d->d_reset();
		d->d_state = QSshClientPrivate::CONNECT;
		d->connectToHost(d->d_hostName, d->d_port);
	}
}

/*!
 * Sets the password for the user. This password is also used for the passphrase of the private key.
 */
void QSshClient::setPassphrase(const QString &pass)
{
	d->d_failedMethods.removeAll(QSshClient::PasswordAuthentication);
	d->d_failedMethods.removeAll(QSshClient::PublicKeyAuthentication);
	d->d_passphrase   = pass;
	d->allow_continue = true;
	if (d->d_state > QSshClientPrivate::CONNECT) {
		QTimer::singleShot(0, d, SLOT(d_readyRead()));
	}
}

/*!
 * Sets a public and private key pair to use to authenticate to the SSH server.
 *
 * If the private key is secured with a passphrase, the passphrase set with setPassphrase() will be used.
 */
void QSshClient::setKeyFiles(const QString &publicKey, const QString &privateKey)
{
	d->d_failedMethods.removeAll(QSshClient::PublicKeyAuthentication);
	d->d_publicKey  = publicKey;
	d->d_privateKey = privateKey;
	if (d->d_state > QSshClientPrivate::CONNECT) {
		QTimer::singleShot(0, d, SLOT(d_readyRead()));
	}
}
/*!
 * Loads a list of known host signatures from a file.
 *
 * This list is used for host key verification during connection.
 */
bool QSshClient::loadKnownHosts(const QString &file, KnownHostsFormat c)
{
	Q_UNUSED(c);
	d->allow_continue = true;
	return (libssh2_knownhost_readfile(d->d_knownHosts, qPrintable(file), LIBSSH2_KNOWNHOST_FILE_OPENSSH) == 0);
}

/*!
 * Saves the current list of known host signatures to a file.
 *
 * \sa loadKnownHosts
 */
bool QSshClient::saveKnownHosts(const QString &file, KnownHostsFormat c) const
{
	Q_UNUSED(c);
	return (libssh2_knownhost_writefile(d->d_knownHosts, qPrintable(file), LIBSSH2_KNOWNHOST_FILE_OPENSSH) == 0);
}

/*!
 * Adds a host key to the list of known host signatures.
 *
 * This list is used for host key verification during connection.
 *
 * \sa loadKnownHosts
 * \sa saveKnownHosts
 */
bool QSshClient::addKnownHost(const QString &hostname, const QSshKey &key)
{
	int typemask = LIBSSH2_KNOWNHOST_TYPE_PLAIN | LIBSSH2_KNOWNHOST_KEYENC_RAW;
	switch (key.type) {
		case QSshKey::Dss:
			typemask |= LIBSSH2_KNOWNHOST_KEY_SSHDSS;
			break;
		case QSshKey::Rsa:
			typemask |= LIBSSH2_KNOWNHOST_KEY_SSHRSA;
			break;
		case QSshKey::Ecdsa256:
			typemask |= LIBSSH2_KNOWNHOST_KEY_ECDSA_256;
			break;
		case QSshKey::UnknownType:
			return false;
	};
	d->allow_continue = true;
	if (d->d_state > QSshClientPrivate::CONNECT) {
		QTimer::singleShot(0, d, SLOT(d_readyRead()));
	}

	return (libssh2_knownhost_add(d->d_knownHosts, qPrintable(hostname), nullptr, key.key.data(), key.key.size(),
	                              typemask, nullptr));
}

/*!
 * Adds a host key entry as extracted from libssh2_knownhost_get()
 */
bool QSshClientPrivate::addKnownHost(struct libssh2_knownhost *entry)
{
	char buf[8192];
	size_t len;
	if (libssh2_knownhost_writeline(d_knownHosts, entry, buf, 8192, &len, LIBSSH2_KNOWNHOST_FILE_OPENSSH) != 0)
		return false;
	if (libssh2_knownhost_readline(d_knownHosts, buf, len, LIBSSH2_KNOWNHOST_FILE_OPENSSH) != 0)
		return false;

	allow_continue = true;
	if (d_state > QSshClientPrivate::CONNECT) {
		QTimer::singleShot(0, this, SLOT(d_readyRead()));
	}
	return true;
}

/*!
 * Returns the host key of the currently connected server.
 */
QSshKey QSshClient::hostKey() const
{
	return d->d_hostKey;
}

/*!
 * Returns the hostname of the currently connected server.
 */
QString QSshClient::hostName() const
{
	return d->d_hostName;
}

int QSshClient::port() const
{
	return d->d_port;
}

QString QSshClient::userName() const
{
	return d->d_userName;
}

QString QSshClient::passPhrase() const
{
	return d->d_passphrase;
}

QString QSshClient::lastErrorMsg() const
{
	return d->d_errorMessage;
}

/*!
 * Opens a new SSH channel that can invoke a process or SSH subsystem on the SSH server.
 * The process's stdin, stdout, and stderr are piped through the channel.
 *
 * Returns NULL if an error occurs while opening the channel, such as not being connected to an SSH server.
 *
 * \sa QSshProcess
 */
QSshProcess *QSshClient::openProcessChannel()
{
	if (d->d_state != QSshClientPrivate::CONNECTED) {
		qWarning("cannot open channel before connected()");
		return nullptr;
	}
	QSshProcess *s = new QSshProcess(this);
	d->d_channels.append(s);
	return s;
}

/*!
 * Opens a new SSH channel and attempts to establish a TCP connection from the SSH server
 * to a remote host. Traffic on this TCP connection is tunneled through the channel.
 *
 * Note that traffic between the SSH server and the remote host is unencrypted. Only
 * communication between QSshClient and the SSH server is encrypted.
 *
 * Returns NULL if an error occurs while opening the channel, such as not being connected to an SSH server.
 *
 * \sa QSshTcpSocket
 */
QSshTcpSocket *QSshClient::openTcpSocket(const QString &hostName, quint16 port)
{
	if (d->d_state != QSshClientPrivate::CONNECTED) {
		qWarning("cannot open channel before connected()");
		return nullptr;
	}
	QSshTcpSocket *s = new QSshTcpSocket(this);
	d->d_channels.append(s);
	s->d->openTcpSocket(hostName, port);
	return s;
}

/*!
 * Opens a new SFTP channel to inspect the remote filesystem or transfer files.
 * Returns NULL if an error occurs while opening the channel, such as not being connected to an SSH server.
 *
 * \sa QSshSftp
 */
QSshSftp *QSshClient::openSftpChannel()
{
	if (d->d_state != QSshClientPrivate::CONNECTED) {
		qWarning("cannot open channel before connected()");
		return nullptr;
	}
	QSshSftp *s = new QSshSftp(this);
	d->d_sftp_channels.append(s);
	return s;
}

void QSshClient::processPendingEvents()
{
	d->d_readyRead();
}

static ssize_t q_p_libssh_recv(int socket, void *buffer, size_t length, int flags, void **abstract)
{
	Q_UNUSED(socket);
	Q_UNUSED(flags);
	QTcpSocket *c = reinterpret_cast<QTcpSocket *>(*abstract);
	int r         = c->read(reinterpret_cast<char *>(buffer), length);
	if (r == 0)
		return -EAGAIN;
	return r;
}

static ssize_t q_p_libssh_send(int socket, const void *buffer, size_t length, int flags, void **abstract)
{
	Q_UNUSED(socket);
	Q_UNUSED(flags);
	QTcpSocket *c = reinterpret_cast<QTcpSocket *>(*abstract);
	int r         = c->write(reinterpret_cast<const char *>(buffer), length);
	if (r == 0)
		return -EAGAIN;
	return r;
}

QSshClientPrivate::QSshClientPrivate()
    : d_session(nullptr), d_knownHosts(nullptr), d_state(DISCONNECTED), d_errorCode(0)
{
	connect(this, &QSshClientPrivate::connected, this, &QSshClientPrivate::d_connected);
	connect(this, qOverload<QAbstractSocket::SocketError>(&QSshClientPrivate::error), this, &QSshClientPrivate::d_error);
	connect(this, &QSshClientPrivate::disconnected, this, &QSshClientPrivate::d_disconnected);
	connect(this, &QSshClientPrivate::readyRead, this, &QSshClientPrivate::d_readyRead);

	Q_ASSERT(libssh2_init(0) == 0);
	// keep known hosts across disconnect / connect cycles
	d_knownHosts = QSshClient::init_knownhosts();
	Q_ASSERT(d_knownHosts);
	d_reset();
}

QSshClientPrivate::~QSshClientPrivate()
{
	teardown();
	libssh2_knownhost_free(d_knownHosts);
}

void QSshClientPrivate::d_connected()
{
	d_state = STARTUP_SESSION;
	d_readyRead();
}

void QSshClientPrivate::handleError(QSshClient::Error err, const std::function<void()> &handler)
{
	d_getLastError();
	// during error handling, we might destroy ourselves...
	QPointer<QSshClient> self(p);
	Q_EMIT p->error(err);
	if (self)
		handler();
}

void QSshClientPrivate::d_readyRead()
{
	allow_continue = true;
	switch (d_state) {
		case STARTUP_SESSION: {
			int sock = socketDescriptor();
			int ret  = 0;

			// 1) initalise ssh session. exchange banner and stuff.

			if ((ret = libssh2_session_startup(d_session, sock)) == LIBSSH2_ERROR_EAGAIN) {
				return;
			}
			if (ret) {
				handleError(QSshClient::UnexpectedShutdownError, [this]() { d_reset(); });
				return;
			}
			d_state = VERIFY_HOST;
			d_readyRead();
			break;
		}

		case VERIFY_HOST: {
			// 2) make sure remote is safe.
			size_t len;
			int type;
			const char *fingerprint = libssh2_session_hostkey(d_session, &len, &type);
			d_hostKey.key           = QByteArray(fingerprint, len);
			d_hostKey.hash          = QByteArray(libssh2_hostkey_hash(d_session, LIBSSH2_HOSTKEY_HASH_MD5), 16);
			switch (type) {
				case LIBSSH2_HOSTKEY_TYPE_RSA:
					d_hostKey.type = QSshKey::Rsa;
					break;
				case LIBSSH2_HOSTKEY_TYPE_DSS:
					d_hostKey.type = QSshKey::Dss;
					break;
				case LIBSSH2_HOSTKEY_TYPE_ECDSA_256:
					d_hostKey.type = QSshKey::Ecdsa256;
					break;
				default:
					d_hostKey.type = QSshKey::UnknownType;
			}
			QSshClient::Error err = QSshClient::AuthenticationError;
			if (fingerprint) {
				struct libssh2_knownhost *host;
#if LIBSSH2_VERSION_NUM >= 0x010206
				/* introduced in 1.2.6 */
				int check = libssh2_knownhost_checkp(d_knownHosts, qPrintable(d_hostName), d_port, (char *)fingerprint, len,
				                                     LIBSSH2_KNOWNHOST_TYPE_PLAIN | LIBSSH2_KNOWNHOST_KEYENC_RAW, &host);
#else
				/* 1.2.5 or older */
				int check = libssh2_knownhost_check(d_knownHosts, qPrintable(d_hostName), (char *)fingerprint, len,
				                                    LIBSSH2_KNOWNHOST_TYPE_PLAIN | LIBSSH2_KNOWNHOST_KEYENC_RAW, &host);
#endif

				switch (check) {
					case LIBSSH2_KNOWNHOST_CHECK_MATCH:
						d_state = TRY_NONE_AUTH;
						d_readyRead();
						return;
					case LIBSSH2_KNOWNHOST_CHECK_FAILURE:
						err = QSshClient::HostKeyInvalidError;
						break;
					case LIBSSH2_KNOWNHOST_CHECK_MISMATCH:
						err = QSshClient::HostKeyMismatchError;
						break;
					case LIBSSH2_KNOWNHOST_CHECK_NOTFOUND:
						err            = QSshClient::HostKeyUnknownError;
						allow_continue = false; // only continue when host was added
						break;
				}
			} else {
				err = QSshClient::HostKeyInvalidError;
			}
			handleError(err, [this]() {
				if (allow_continue)
					d_readyRead();
			});
			return;
			break;
		}

		case TRY_NONE_AUTH: {
			// 3) try auth type "none" and get a list of other methods
			//   in the likely case that the server doesnt like "none"

			QByteArray username = d_userName.toLocal8Bit();
			char *alist         = libssh2_userauth_list(d_session, username.data(), username.length());
			if (alist == nullptr) {
				if (libssh2_userauth_authenticated(d_session)) {
					// null auth ok
					d_state = CONNECTED;
					emit p->connected();
					return;
				} else if (libssh2_session_last_error(d_session, nullptr, nullptr, 0) == LIBSSH2_ERROR_EAGAIN) {
					return;
				} else {
					handleError(QSshClient::UnexpectedShutdownError, [this]() { d_reset(); });
					return;
				}
			}

			foreach (QByteArray m, QByteArray(alist).split(',')) {
				if (m == "publickey") {
					d_availableMethods << QSshClient::PublicKeyAuthentication;
				} else if (m == "password") {
					d_availableMethods << QSshClient::PasswordAuthentication;
				}
			}
			d_state = GET_NEXT_AUTH_METHOD;
			d_readyRead();
			break;
		}

		case GET_NEXT_AUTH_METHOD: {
#ifdef QSSH2_DEBUG
			qDebug("looking for auth option");
#endif
			if (d_availableMethods.contains(QSshClient::PublicKeyAuthentication) && !d_privateKey.isNull() &&
			    !d_failedMethods.contains(QSshClient::PublicKeyAuthentication)) {
				d_currentAuthTry = QSshClient::PublicKeyAuthentication;
				d_state          = TRY_AUTH_METHOD;
				d_readyRead();
				return;
			}
			if (d_availableMethods.contains(QSshClient::PasswordAuthentication) && !d_passphrase.isNull() &&
			    !d_failedMethods.contains(QSshClient::PasswordAuthentication)) {
				d_currentAuthTry = QSshClient::PasswordAuthentication;
				d_state          = TRY_AUTH_METHOD;
				d_readyRead();
				return;
			}
			allow_continue = false; // only continue when password was provided
			emit p->authenticationRequired(d_availableMethods);
			break;
		}

		case TRY_AUTH_METHOD: {
			int ret(0);
#ifdef QSSH2_DEBUG
			qDebug() << "trying" << d_currentAuthTry;
#endif
			if (d_currentAuthTry == QSshClient::PasswordAuthentication) {
				ret = libssh2_userauth_password(d_session, qPrintable(d_userName), qPrintable(d_passphrase));

			} else if (d_currentAuthTry == QSshClient::PublicKeyAuthentication) {
				ret = libssh2_userauth_publickey_fromfile(d_session, qPrintable(d_userName), qPrintable(d_publicKey),
				                                          qPrintable(d_privateKey), qPrintable(d_passphrase));
			}
			if (ret == LIBSSH2_ERROR_EAGAIN) {
				return;
			} else if (ret == 0) {
				d_state = CONNECTED;
				emit p->connected();
			} else {
				handleError(QSshClient::AuthenticationError, [this]() {
					d_failedMethods.append(d_currentAuthTry);
					d_state = GET_NEXT_AUTH_METHOD;
					d_readyRead();
				});
			}
			break;
		}

		case CONNECTED: {
			for (QList<QSshChannel *>::const_iterator i = d_channels.constBegin(), e = d_channels.constEnd(); i != e;
			     ++i) {
				bool ret = (*i)->d->activate();
				if (!ret) {
					d_getLastError();
				}
			}
			for (QList<QSshSftp *>::const_iterator i = d_sftp_channels.constBegin(), e = d_sftp_channels.constEnd();
			     i != e; ++i) {
				bool ret = (*i)->d_ptr->activate();
				if (!ret) {
					d_getLastError();
				}
			}
			break;
		}

		case DISCONNECT: {
			int ret = libssh2_session_free(d_session);
			if (ret == LIBSSH2_ERROR_EAGAIN)
				return;
			else if (ret == 0) {
				d_state   = DISCONNECTED;
				d_session = nullptr;
			}
			break;
		}

		default: {
#ifdef QSSH2_DEBUG
			qDebug("did not expect to receive data in this state");
#endif
			break;
		}
	} // switch
}

void QSshClientPrivate::teardown()
{
	// cannot use qDeleteAll() as channels deregister from their list when deleted!
	while (!d_channels.empty())
		delete d_channels.front();
	while (!d_sftp_channels.empty())
		delete d_sftp_channels.front();

	if (d_state > CONNECT) {
		libssh2_session_disconnect(d_session, "good bye!");
		// trigger session shutdown
		d_state = DISCONNECT;
		d_readyRead();
	}

	// wait for session shutdown
	while (d_state > CONNECT && this->state() >= QAbstractSocket::ConnectedState)
		waitForReadyRead(10);

	if (d_session)
		libssh2_session_free(d_session);
	d_session      = nullptr;
	d_errorCode    = 0;
	d_errorMessage = QString();
	d_failedMethods.clear();
	d_availableMethods.clear();

	if (state() != QAbstractSocket::UnconnectedState) {
		disconnectFromHost();
		waitForDisconnected(10);
	}
}

void QSshClientPrivate::d_reset()
{
#ifdef QSSH2_DEBUG
	qDebug("reset");
#endif
	teardown();

	// buildup
	d_session = libssh2_session_init_ex(nullptr, nullptr, nullptr, reinterpret_cast<void *>(this));
	libssh2_session_callback_set(d_session, LIBSSH2_CALLBACK_RECV, reinterpret_cast<void *>(&q_p_libssh_recv));
	libssh2_session_callback_set(d_session, LIBSSH2_CALLBACK_SEND, reinterpret_cast<void *>(&q_p_libssh_send));
	Q_ASSERT(d_session);

	libssh2_session_set_blocking(d_session, 0);
}

void QSshClientPrivate::d_disconnected()
{
	if (d_state != DISCONNECTED && d_state != DISCONNECT)
		qWarning("unexpected shutdown");

	// we might need a final call to libssh2_session_free()
	if (d_state == DISCONNECT)
		d_readyRead();
}

void QSshClientPrivate::d_error(QAbstractSocket::SocketError err)
{
	QSshClient::Error e = QSshClient::UnknownError;
	bool signal_error   = true;

	switch (err) {
		case ConnectionRefusedError:
			e = QSshClient::ConnectionRefusedError;
			break;
		case HostNotFoundError:
			e = QSshClient::HostNotFoundError;
			break;
		case SocketTimeoutError:
			return;
		case RemoteHostClosedError:
			if (d_state == DISCONNECT)
				signal_error = false;
			break;
		default:
			e = QSshClient::SocketError;
			break;
	}
	d_state = DISCONNECTED;
	if (signal_error)
		Q_EMIT p->error(e);
}

void QSshClientPrivate::d_getLastError()
{
	char *msg;
	int len        = 0;
	d_errorCode    = libssh2_session_last_error(d_session, &msg, &len, 0);
	d_errorMessage = QString::fromLocal8Bit(QByteArray::fromRawData(msg, len));
}
