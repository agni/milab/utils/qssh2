#pragma once

#include "qsshchannel.h"
#include "qssh2_export.h"

class QSSH2_EXPORT QSshProcess : public QSshChannel
{
	Q_OBJECT
public:
	enum TerminalType
	{
		VanillaTerminal,
		Vt102Terminal,
		AnsiTerminal
	};
	void requestPty(TerminalType term = VanillaTerminal);
	void startShell();
	void start(const QString &cmd);

signals:
	void finished(int exitCode);

public Q_SLOTS:
	void terminate();

private:
	QSshProcess(QSshClient *);
	friend class QSshClient;

private slots:
	void onDisconnected();
};
