#pragma once

#include <QDialog>
#include <QWidget>
#include "qssh2_export.h"
#include <QFileDialog>
#include <QSshFileSystemModel>
#include <QSshConnectionManager>
#include <QKeyEvent>
#include <QLineEdit>
#include <QComboBox>
#include <QStandardItemModel>
#include <QCompleter>

class Ui_QSshFileDialog;
class QSshFileDialogPrivate;
struct QSshFileDialogArgs;

class QSSH2_EXPORT QSshFileDialog : public QDialog
{
	Q_OBJECT
	const QScopedPointer<QSshFileDialogPrivate> d_ptr;

public:
	explicit QSshFileDialog(QSshConnectionManager *manager, const QUrl &url, QWidget *parent = nullptr,
	                        const QString &caption = QString(), const QString &directory = QString(),
	                        const QString &filter = QString());
	~QSshFileDialog();

	void setDirectory(const QString &directory);
	inline void setDirectory(const QDir &directory);
	QDir directory() const;

	void selectFile(const QString &filename);
	QStringList selectedFiles() const;

	void selectUrl(const QUrl &url);
	QList<QUrl> selectedUrls() const;

	void setNameFilter(const QString &filter);
	void setNameFilters(const QStringList &filters);
	QStringList nameFilters() const;
	void selectNameFilter(const QString &filter);
	QString selectedNameFilter() const;

#ifndef QT_NO_MIMETYPE
	void setMimeTypeFilters(const QStringList &filters);
	QStringList mimeTypeFilters() const;
	void selectMimeTypeFilter(const QString &filter);
#endif

	QDir::Filters filter() const;
	void setFilter(QDir::Filters filters);

	void setViewMode(QFileDialog::ViewMode mode);
	QFileDialog::ViewMode viewMode() const;

	void setFileMode(QFileDialog::FileMode mode);
	QFileDialog::FileMode fileMode() const;

	void setAcceptMode(QFileDialog::AcceptMode mode);
	QFileDialog::AcceptMode acceptMode() const;

	void setDefaultSuffix(const QString &suffix);
	QString defaultSuffix() const;

	void setHistory(const QStringList &paths);
	QStringList history() const;

	void setOption(QFileDialog::Option option, bool on = true);
	bool testOption(QFileDialog::Option option) const;
	void setOptions(QFileDialog::Options options);
	QFileDialog::Options options() const;

	using QDialog::open;
	void open(QObject *receiver, const char *member);

signals:
	void fileSelected(const QString &file);
	void filesSelected(const QStringList &files);
	void currentChanged(const QString &path);
	void directoryEntered(const QString &directory);

	void urlSelected(const QUrl &url);
	void urlsSelected(const QList<QUrl> &urls);
	void currentUrlChanged(const QUrl &url);
	void directoryUrlEntered(const QUrl &directory);

	void filterSelected(const QString &filter);

	void modelCreated();

public:
	static QString getOpenFileName(QSshConnectionManager *manager, const QUrl &url, QWidget *parent = 0,
	                               const QString &caption = QString(), const QString &dir = QString(),
	                               const QString &filter = QString(), QString *selectedFilter = 0,
	                               QFileDialog::Options options = 0);

	static QUrl getOpenFileUrl(QSshConnectionManager *manager, const QUrl &url, QWidget *parent = 0,
	                           const QString &caption = QString(), const QUrl &dir = QUrl(),
	                           const QString &filter = QString(), QString *selectedFilter = 0,
	                           QFileDialog::Options options = 0, const QStringList &supportedSchemes = QStringList());

	static QString getSaveFileName(QSshConnectionManager *manager, const QUrl &url, QWidget *parent = 0,
	                               const QString &caption = QString(), const QString &dir = QString(),
	                               const QString &filter = QString(), QString *selectedFilter = 0,
	                               QFileDialog::Options options = 0);

	static QUrl getSaveFileUrl(QSshConnectionManager *manager, const QUrl &url, QWidget *parent = 0,
	                           const QString &caption = QString(), const QUrl &dir = QUrl(),
	                           const QString &filter = QString(), QString *selectedFilter = 0,
	                           QFileDialog::Options options = 0, const QStringList &supportedSchemes = QStringList());

	static QString getExistingDirectory(QSshConnectionManager *manager, const QUrl &url, QWidget *parent = 0,
	                                    const QString &caption = QString(), const QString &dir = QString(),
	                                    QFileDialog::Options options = QFileDialog::ShowDirsOnly);

	static QUrl getExistingDirectoryUrl(QSshConnectionManager *manager, const QUrl &url, QWidget *parent = 0,
	                                    const QString &caption = QString(), const QUrl &dir = QUrl(),
	                                    QFileDialog::Options options        = QFileDialog::ShowDirsOnly,
	                                    const QStringList &supportedSchemes = QStringList());

	static QStringList getOpenFileNames(QSshConnectionManager *manager, const QUrl &url, QWidget *parent = 0,
	                                    const QString &caption = QString(), const QString &dir = QString(),
	                                    const QString &filter = QString(), QString *selectedFilter = 0,
	                                    QFileDialog::Options options = 0);

	static QList<QUrl> getOpenFileUrls(QSshConnectionManager *manager, const QUrl &url, QWidget *parent = 0,
	                                   const QString &caption = QString(), const QUrl &dir = QUrl(),
	                                   const QString &filter = QString(), QString *selectedFilter = 0,
	                                   QFileDialog::Options options        = 0,
	                                   const QStringList &supportedSchemes = QStringList());

protected:
	QSshFileDialog(QSshConnectionManager *manager, const QUrl &url, const QSshFileDialogArgs &args);
	void done(int result) Q_DECL_OVERRIDE;
	void accept() Q_DECL_OVERRIDE;

private:
	Q_DECLARE_PRIVATE(QSshFileDialog)
	Q_PRIVATE_SLOT(d_func(), void _q_showHeader(QAction *))
};

void QSshFileDialog::setDirectory(const QDir &adirectory)
{
	setDirectory(adirectory.absolutePath());
}

class QSshUrlModel : public QStandardItemModel
{
	Q_OBJECT

public:
	enum Roles
	{
		UrlRole     = Qt::UserRole + 1,
		EnabledRole = Qt::UserRole + 2
	};

	QSshUrlModel(QObject *parent = 0);

	QStringList mimeTypes() const Q_DECL_OVERRIDE;
	QMimeData *mimeData(const QModelIndexList &indexes) const Q_DECL_OVERRIDE;
#ifndef QT_NO_DRAGANDDROP
	bool canDrop(QDragEnterEvent *event);
	bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column,
	                  const QModelIndex &parent) Q_DECL_OVERRIDE;
#endif
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;

	void setUrls(const QList<QUrl> &list);
	void addUrls(const QList<QUrl> &urls, int row = -1, bool move = true);
	QList<QUrl> urls() const;
	void setFileSystemModel(QSshFileSystemModel *model);
	bool showFullPath;

private Q_SLOTS:
	void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
	void layoutChanged();

private:
	void setUrl(const QModelIndex &index, const QUrl &url, const QModelIndex &dirIndex);
	void changed(const QString &path);
	void addIndexToWatch(const QString &path, const QModelIndex &index);
	QSshFileSystemModel *fileSystemModel;
	QList<QPair<QModelIndex, QString> > watching;
	QList<QUrl> invalidUrls;
	QFileIconProvider iconProvider;
};

class QSshFileDialogLineEdit : public QLineEdit
{
public:
	QSshFileDialogLineEdit(QWidget *parent = 0) : QLineEdit(parent), d_ptr(0) {}
	void setFileDialogPrivate(QSshFileDialogPrivate *d_pointer) { d_ptr = d_pointer; }
	void keyPressEvent(QKeyEvent *e) Q_DECL_OVERRIDE;
	bool hideOnEsc;

private:
	QSshFileDialogPrivate *d_ptr;
};

class QSshFileDialogComboBox : public QComboBox
{
public:
	QSshFileDialogComboBox(QWidget *parent = 0) : QComboBox(parent), urlModel(0) {}
	void setFileDialogPrivate(QSshFileDialogPrivate *d_pointer);
	void showPopup() Q_DECL_OVERRIDE;
	void setHistory(const QStringList &paths);
	QStringList history() const { return m_history; }
	void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;

private:
	QSshUrlModel *urlModel;
	QSshFileDialogPrivate *d_ptr;
	QStringList m_history;
};

struct QSshFileDialogArgs
{
	QSshFileDialogArgs() : parent(0), mode(QFileDialog::AnyFile) {}

	QWidget *parent;
	QString caption;
	QUrl directory;
	QString selection;
	QString filter;
	QFileDialog::FileMode mode;
	QFileDialog::Options options;
};

/*!
   QSshFSCompleter that can deal with QSshFileSystemModel
  */
class QSSH2_EXPORT QSshFSCompleter : public QCompleter
{
public:
	explicit QSshFSCompleter(QSshFileSystemModel *model, QObject *parent = 0)
	    : QCompleter(model, parent), sourceModel(model)
	{
#if defined(Q_OS_WIN)
		setCaseSensitivity(Qt::CaseInsensitive);
#endif
	}
	QString pathFromIndex(const QModelIndex &index) const Q_DECL_OVERRIDE;
	QStringList splitPath(const QString &path) const Q_DECL_OVERRIDE;

	QSshFileSystemModel *sourceModel;
};
