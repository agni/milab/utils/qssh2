#pragma once

#include <QIODevice>
#include "qssh2_export.h"

class QSshClient;
class QSshChannelPrivate;
class QSSH2_EXPORT QSshChannel : public QIODevice
{
	Q_OBJECT

public:
	virtual ~QSshChannel();

protected:
	QSshChannel(QSshClient *);
	virtual qint64 readData(char *, qint64);
	virtual qint64 writeData(const char *, qint64);
	virtual bool isSequential() const;

	QSshChannelPrivate *d;
	friend class QSshChannelPrivate;
	friend class QSshClientPrivate;

signals:
	void connected();
	void disconnected();
};
