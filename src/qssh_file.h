#pragma once

#include <QIODevice>
#include "qssh2_export.h"

class QSshSftp;
class QSshFilePrivate;

class QSSH2_EXPORT QSshFile : public QIODevice
{
	Q_OBJECT
	friend class QSshSftp;
	friend class QSshSftpPrivate;

	QSshFilePrivate *d_ptr;

public:
	~QSshFile();

	bool open(OpenMode mode) Q_DECL_OVERRIDE;
	void close() Q_DECL_OVERRIDE;
	/// flush any buffered data to the file
	bool flush(int msecs);

	qint64 bytesAvailable() const Q_DECL_OVERRIDE;
	bool canReadLine() const;
	qint64 bytesToWrite() const Q_DECL_OVERRIDE;

	bool isSequential() const Q_DECL_OVERRIDE { return true; }
	bool atEnd() const Q_DECL_OVERRIDE;
	int lastError() const;

	bool waitForOpened(int msecs);
	bool waitForReadyRead(int msecs) Q_DECL_OVERRIDE;
	bool waitForBytesWritten(int msecs) Q_DECL_OVERRIDE;

protected:
	QSshFile(const QString &file_name, QSshSftp *sftp);
	qint64 readData(char *data, qint64 max_size) Q_DECL_OVERRIDE;
	qint64 writeData(const char *data, qint64 max_size) Q_DECL_OVERRIDE;

signals:
	void opened();
	void error(int sftp_error);
};
