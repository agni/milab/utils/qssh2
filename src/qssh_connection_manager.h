#pragma once

#include <qobject.h>
#include <qsshclient.h>
#include <qsharedpointer.h>
#include <qdialog.h>
#include <qurl.h>
#include <functional>

#include "qssh2_export.h"

struct QSSH2_EXPORT ConnectionInfo
{
	explicit ConnectionInfo(const QSshClient *client);
	ConnectionInfo(const QUrl &url);
	QString user, host;
	int port;
};

typedef QSharedPointer<QSshClient> QSshClientSharedPtr;

class QSshConnectionManagerPrivate;
class QSSH2_EXPORT QSshConnectionManager : public QObject
{
	Q_OBJECT
	QSshConnectionManagerPrivate *d_ptr;

public:
	explicit QSshConnectionManager(QObject *parent = 0);
	~QSshConnectionManager();
	static QSshConnectionManager &instance();

	bool loadKnownHosts(const QString &file, QSshClient::KnownHostsFormat c);
	bool saveKnownHosts(const QString &file, QSshClient::KnownHostsFormat c) const;

	using AcceptHostKeyFunction = std::function<bool(const QString &host, const QString &key)>;
	using AskPasswordFunction   = std::function<QUrl(const QSshClient *)>;
	void setAcceptHostKeyFunction(const AcceptHostKeyFunction &f);
	void setAskPasswordFunction(const AskPasswordFunction &f);

	/// default functions to accept a new host key and to ask for a password
	static bool guiAcceptHostKey(const QString &host, const QString &key);
	QUrl guiAskPassword(const QSshClient *client);

	/// find QSshClient for given connection info (empty if not available)
	QSshClientSharedPtr find(const ConnectionInfo &info);
	/// get existing or create new QSshClient for given connection
	QSshClientSharedPtr open(const ConnectionInfo &info, bool connect);
	/// Detach current connections (in all threads) matching info.
	/// This will not close existing connections, but mark them as not reusable
	bool remove(const QSshClient *client);
	/// Detach all current connections.
	void clear();

private slots:
	void authenticationRequired(const QList<QSshClient::AuthenticationMethod> &availableMethods);
	void onError(QSshClient::Error error);
	void onConnected();
	void onDisconnected();

signals:
	void authenticationCancelled(const QSshClient *);
	void connectionFailed(const QString &client, int port, QSshClient::Error error);
};

class Ui_QSshAskCredentials;
class QSSH2_EXPORT QSshAskCredentials : public QDialog
{
	Q_OBJECT

public:
	explicit QSshAskCredentials(const QString &host, const QString &username, bool user_editable, QWidget *parent = 0);
	~QSshAskCredentials();

	QString userName() const;
	QString password() const;

private:
	Ui_QSshAskCredentials *ui;
};
