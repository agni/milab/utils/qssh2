#include "qssh_connection_manager.h"
#include "qsshclient_p.h"
#include "ui_qssh_ask_credentials.h"

#include <qinputdialog.h>
#include <qmessagebox.h>
#include <qdir.h>
#include <qurl.h>
#include <qthread.h>
#include <qmutex.h>
#include <qpointer.h>
#include <qtimer.h>

ConnectionInfo::ConnectionInfo(const QSshClient *client)
    : user(client->userName()), host(client->hostName()), port(client->port())
{}

ConnectionInfo::ConnectionInfo(const QUrl &url) : user(url.userName()), host(url.host()), port(url.port(22)) {}

bool operator<(const ConnectionInfo &c1, const ConnectionInfo &c2)
{
	int r = c1.host.compare(c2.host);
	if (r != 0)
		return r < 0;

	if (c1.port != c2.port)
		return c1.port < c2.port;

	return c1.user.compare(c2.user) < 0;
}

bool matches(const ConnectionInfo &stored, const ConnectionInfo &test)
{
	return stored.host == test.host && stored.port == test.port && (test.user.isEmpty() || stored.user == test.user);
}

/// Connections (QSshClients) are bound to their creating thread (due to use of QSockets).
/// Hence, we maintain for each connection info (host, port, user) a map thread -> connections.
class QSshConnectionManagerPrivate
{
	friend class QSshConnectionManager;
	friend struct MatchClientOp;
	friend QDebug operator<<(QDebug d, const QSshConnectionManagerPrivate &me);

	// we cannot avoid to have multiple connections per thread
	using ClientMap = std::multimap<QThread *, QSshClientSharedPtr>;
	struct Connections
	{
		QString password;
		ClientMap clients;
	};

	using ConnectionMap = std::map<ConnectionInfo, Connections>;
	QSshConnectionManager *q_ptr;
	QMutex mutex;
	LIBSSH2_KNOWNHOSTS *known_hosts;
	ConnectionMap connections;
	QPointer<QSshClient> authenticating_client;
	uint auth_trial; // number of current authentication trial
	bool auth_show_user;
	QList<QPointer<QSshClient>> postponed;

	QSshConnectionManager::AcceptHostKeyFunction acceptHostKey;
	QSshConnectionManager::AskPasswordFunction askPassword;

public:
	QSshConnectionManagerPrivate(QSshConnectionManager *q_ptr) : q_ptr(q_ptr), mutex(QMutex::Recursive)
	{
		known_hosts = QSshClient::init_knownhosts();
	}
	~QSshConnectionManagerPrivate() { libssh2_knownhost_free(known_hosts); }

	// find connection info in available connections
	QSshClientSharedPtr find(const ConnectionInfo &info, QThread *thread);
	ConnectionMap::iterator find(const QSshClient *client);
	// move client from one connection branch to another in order to match
	// client (user) settings with key of ConnectionMap
	void move(const QString &old_user, const QSshClient *client);
	// start authentication if possible, or postpone client
	bool startAuthentication(QSshClient *client);
	// retrigger all pending clients waiting for authentication
	void processPending(const QSshClient *client);

	// add known host to global list known_hosts
	libssh2_knownhost *addKnownHost(const QString &host, const QSshKey &key);
	// add a single know_host entry into client
	bool addKnownHost(QSshClient *client, struct libssh2_knownhost *entry) { return client->d->addKnownHost(entry); }
	// sync global list of known_hosts into client
	void syncKnownHosts(QSshClient *client);
	// open connection to host and sync known hosts
	void connect(const QSshClientSharedPtr &client);
};

QDebug operator<<(QDebug d, const ConnectionInfo &info)
{
	return d << QString("%1@%2:%3").arg(info.user, info.host).arg(info.port);
}

QDebug operator<<(QDebug d, const QSshConnectionManagerPrivate &me)
{
	for (const auto &conn : me.connections) {
		d << conn.first << ":";
		for (const auto &client : conn.second.clients) {
			d << client.second.data();
		}
		d << "\n";
	}
	return d;
}

QSshConnectionManager::QSshConnectionManager(QObject *parent) : QObject(parent)
{
	d_ptr = new QSshConnectionManagerPrivate(this);
	setAcceptHostKeyFunction(guiAcceptHostKey);
	setAskPasswordFunction([this](const QSshClient *client) { return this->guiAskPassword(client); });
}

QSshConnectionManager::~QSshConnectionManager()
{
	delete d_ptr;
}

void QSshConnectionManager::setAcceptHostKeyFunction(const QSshConnectionManager::AcceptHostKeyFunction &f)
{
	d_ptr->acceptHostKey = f;
}

void QSshConnectionManager::setAskPasswordFunction(const QSshConnectionManager::AskPasswordFunction &f)
{
	d_ptr->askPassword = f;
}

QSshClientSharedPtr QSshConnectionManager::find(const ConnectionInfo &info)
{
	QMutexLocker locker(&d_ptr->mutex);
	return d_ptr->find(info, QThread::currentThread());
}

// find info, but allow matching of an empty user name to any existing connection
QSshClientSharedPtr QSshConnectionManagerPrivate::find(const ConnectionInfo &info, QThread *thread)
{
	// assumes mutex to be locked!

	// find info, but allow empty info.user to match non-empty stored one
	auto conn = connections.lower_bound(info);
	if (conn == connections.end() || !matches(conn->first, info))
		return QSshClientSharedPtr();

	auto client = conn->second.clients.find(thread);
	if (client == conn->second.clients.end())
		return QSshClientSharedPtr();

	return client->second;
}

struct MatchClientOp
{
	const QSshClient *client;
	MatchClientOp(const QSshClient *client) : client(client) {}
	bool operator()(const QSshConnectionManagerPrivate::ClientMap::value_type &pair)
	{
		return pair.second.data() == client;
	}
};

QSshConnectionManagerPrivate::ConnectionMap::iterator QSshConnectionManagerPrivate::find(const QSshClient *client)
{
	// assumes mutex to be locked!
	ConnectionInfo info(client);
	while (true) {
		auto conn = connections.find(info);
		if (conn != connections.end()) {
			auto &clients = conn->second.clients;
			if (std::find_if(clients.begin(), clients.end(), MatchClientOp(client)) != clients.end())
				return conn;
		}
		// on failure, try with empty user too
		if (!info.user.isEmpty())
			info.user.clear();
		else
			break;
	}
	return connections.end();
}

void QSshConnectionManagerPrivate::move(const QString &old_user, const QSshClient *client)
{
	// assumes mutex to be locked!
	// find old entry
	ConnectionInfo info(client);
	info.user = old_user;
	auto conn = connections.find(info); // find exactly
	if (conn == connections.end())
		return; // client might have been removed

	auto &old_clients = conn->second.clients;
	auto old          = std::find_if(old_clients.begin(), old_clients.end(), MatchClientOp(client));
	if (old == old_clients.end())
		return; // client might have been removed

	// insert old entry in new connection setting
	connections[ConnectionInfo(client)].clients.insert(*old);

	// remove old entry from old connection setting
	old_clients.erase(old);
}

void QSshConnectionManagerPrivate::processPending(const QSshClient *client)
{
	// assumes mutex to be locked
	if (authenticating_client == client) {
		if (!client->isConnected())
			Q_EMIT q_ptr->authenticationCancelled(client);
		authenticating_client = nullptr;
	} else
		return;

	// first clear postponed list
	QList<QPointer<QSshClient>> old;
	std::swap(old, postponed);

	// and then trigger continuation on all items
	for (QSshClient *client : qAsConst(old)) {
		if (client)
			syncKnownHosts(client);
	}
}

QSshClientSharedPtr QSshConnectionManager::open(const ConnectionInfo &info, bool connect)
{
	// return existing connection if available
	QSshClientSharedPtr client = find(info);
	if (!client) {
		// connection is not in list -> create new one
		client.reset(new QSshClient());
		QObject::connect(client.data(), &QSshClient::authenticationRequired, this,
		                 &QSshConnectionManager::authenticationRequired);
		QObject::connect(client.data(), &QSshClient::error, this, &QSshConnectionManager::onError);
		QObject::connect(client.data(), &QSshClient::connected, this, &QSshConnectionManager::onConnected);
		client->setConnection(info.host, info.port);
		{
			QMutexLocker locker(&d_ptr->mutex);
			d_ptr->connections[info].clients.insert(std::make_pair(client->thread(), client));
		}
	}
	// connect only when requested
	if (!client->isConnected() && connect)
		d_ptr->connect(client);
	return client;
}

void QSshConnectionManager::onConnected()
{
	Q_ASSERT(dynamic_cast<QSshClient *>(sender()));
	QSshClient *client = static_cast<QSshClient *>(sender());
	disconnect(client, &QSshClient::connected, this, &QSshConnectionManager::onConnected);

	QMutexLocker locker(&d_ptr->mutex);
	// retrieve settings from list
	auto conn = d_ptr->find(client);
	if (conn != d_ptr->connections.end())
		// store password in settings and clear it in client
		conn->second.password = client->passPhrase();
	client->setPassphrase("");

	d_ptr->processPending(client);
}

void QSshConnectionManager::onDisconnected()
{
	remove(qobject_cast<QSshClient *>(sender()));
}

// Removing the QSshClient doesn't yet destroy the instance,
// but only decreases the reference count of the shared pointer by 1.
// Only destroying the last QSharedPointer will eventually close the connection.
bool QSshConnectionManager::remove(const QSshClient *client)
{
	QMutexLocker locker(&d_ptr->mutex);

	disconnect(client, nullptr, this, nullptr); // do not receive further signals from client
	d_ptr->processPending(client);

	auto conn = d_ptr->find(client);
	if (conn == d_ptr->connections.end())
		return false;

	// remove client from list
	auto &clients = conn->second.clients;
	clients.erase(std::find_if(clients.begin(), clients.end(), MatchClientOp(client)));

	return true;
}

void QSshConnectionManager::clear()
{
	QMutexLocker locker(&d_ptr->mutex);
	// stop receiving signals from clients
	for (const auto &conn : d_ptr->connections) {
		for (const auto &cit : conn.second.clients) {
			QSshClient *client = cit.second.data();
			disconnect(client, nullptr, this, nullptr);
			if (!client->isConnected())
				Q_EMIT authenticationCancelled(client);
		}
	}
	d_ptr->connections.clear();
	d_ptr->postponed.clear();
	d_ptr->authenticating_client = nullptr;
}

bool QSshConnectionManagerPrivate::startAuthentication(QSshClient *client)
{
	Q_ASSERT(client);

	// assumes mutex to be locked!
	if (authenticating_client && authenticating_client != client) {
		// postpone authentication until current authentication is finished
		if (client && !postponed.contains(client))
			postponed.push_back(client);
		return false;
	}
	if (!authenticating_client)
		auth_trial = 0;
	authenticating_client = client;
	return true;
}

void QSshConnectionManager::authenticationRequired(const QList<QSshClient::AuthenticationMethod> & /*unused*/)
{
	Q_ASSERT(dynamic_cast<QSshClient *>(sender()));
	// notice if QSshClient gets deleted
	QPointer<QSshClient> client = static_cast<QSshClient *>(sender());

	QMutexLocker locker(&d_ptr->mutex);
	if (!d_ptr->startAuthentication(client))
		return; // postponed

	// try possibly updated password from connection settings
	ConnectionInfo info(client);
	auto conn = d_ptr->connections.find(info);
	if (conn == d_ptr->connections.end()) {
		// if not found, try again with empty username
		info.user.clear();
		conn = d_ptr->connections.find(info);
	}
	Q_ASSERT(conn != d_ptr->connections.end());

	if (client->passPhrase() != conn->second.password) {
		client->setPassphrase(conn->second.password);
		return;
	}
	locker.unlock();

	// ask user for credentials, may change user name
	const QString &old_user = conn->first.user;

	if (++d_ptr->auth_trial == 1)
		d_ptr->auth_show_user = old_user.isEmpty();

	QUrl url = d_ptr->askPassword(client);

	if (!client) // was client deleted executing askPassword()?
		return;

	if (url.password().isNull()) { // dialog cancelled
		remove(client);
	} else {
		if (old_user.isEmpty() && url.userName() == qgetenv("USER"))
			url.setUserName(QString());

		if (old_user != url.userName()) {
			// user name has changed: move client to correct connection entry
			locker.relock();
			client->setUserName(url.userName());
			d_ptr->move(old_user, client);
		}
		// continues connecting
		client->setPassphrase(url.password());
	}
}

struct libssh2_knownhost *QSshConnectionManagerPrivate::addKnownHost(const QString &host, const QSshKey &key)
{
	int typemask = LIBSSH2_KNOWNHOST_TYPE_PLAIN | LIBSSH2_KNOWNHOST_KEYENC_RAW;
	switch (key.type) {
		case QSshKey::Dss:
			typemask |= LIBSSH2_KNOWNHOST_KEY_SSHDSS;
			break;
		case QSshKey::Rsa:
			typemask |= LIBSSH2_KNOWNHOST_KEY_SSHRSA;
			break;
		case QSshKey::Ecdsa256:
			typemask |= LIBSSH2_KNOWNHOST_KEY_ECDSA_256;
			break;
		case QSshKey::UnknownType:
			return nullptr;
	};

	struct libssh2_knownhost *entry = nullptr;
	libssh2_knownhost_add(known_hosts, qPrintable(host), nullptr, key.key.data(), key.key.size(), typemask, &entry);
	return entry;
}

void QSshConnectionManagerPrivate::syncKnownHosts(QSshClient *client)
{
	// assumes mutex to be locked!
	struct libssh2_knownhost *cur = nullptr;
	while (true) {
		if (libssh2_knownhost_get(known_hosts, &cur, cur) != 0 || !cur)
			break;
		client->d->addKnownHost(cur);
	}
	// if there was no host to be added, we need to trigger manually:
	QTimer::singleShot(0, client->d, SLOT(d_readyRead()));
}

void QSshConnectionManagerPrivate::connect(const QSshClientSharedPtr &client)
{
	if (client->isConnected())
		return;
	{
		QMutexLocker locker(&mutex);
		syncKnownHosts(client.data());
	}
	client->connectToHost(client->hostName(), client->userName(), client->port());
}

void QSshConnectionManager::onError(QSshClient::Error err)
{
	Q_ASSERT(dynamic_cast<QSshClient *>(sender()));
	// notice if QSshClient gets deleted
	QPointer<QSshClient> client = static_cast<QSshClient *>(sender());

	if (err == QSshClient::HostKeyUnknownError) {
		QMutexLocker locker(&d_ptr->mutex);
		if (!d_ptr->startAuthentication(client))
			return; // postponed
		locker.unlock();

		QSshKey k = client->hostKey();
		if (d_ptr->acceptHostKey(client->hostName(), QString::fromLatin1(k.hash.toHex())) &&
		    client /* was client deleted meanwhile? */) {
			locker.relock();
			// enter known host key to our global list
			struct libssh2_knownhost *entry = d_ptr->addKnownHost(client->hostName(), k);
			// on success, enter it to client's list too
			if (entry)
				d_ptr->addKnownHost(client, entry);
			return;
		}
	} else if (err == QSshClient::AuthenticationError)
		return; // ask password again

	if (!client) { // was client deleted during user interaction, i.e. acceptHostKey()?
		return;
	} else {
		// only report, when error not in {HostKeyUnknownError, AuthenticationError}
		if (err != QSshClient::HostKeyUnknownError)
			Q_EMIT connectionFailed(client->hostName(), client->port(), err);

		remove(client);
	}
}

bool QSshConnectionManager::guiAcceptHostKey(const QString &host, const QString &key)
{
	return QMessageBox::question(nullptr, tr("SSH Authentication"),
	                             tr("Accept host key for %1?\nfingerprint: \n%2").arg(host, key),
	                             QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes;
}

QUrl QSshConnectionManager::guiAskPassword(const QSshClient *client)
{
	QSshAskCredentials dialog(client->hostName(), client->userName(), d_ptr->auth_show_user);
	connect(this, &QSshConnectionManager::authenticationCancelled, &dialog, &QDialog::reject);

	if (dialog.exec() == QDialog::Accepted) {
		QUrl result;
		result.setUserName(dialog.userName());
		result.setPassword(dialog.password());
		return result;
	}
	return QUrl();
}

bool QSshConnectionManager::loadKnownHosts(const QString &file, QSshClient::KnownHostsFormat c)
{
	Q_UNUSED(c);
	return (libssh2_knownhost_readfile(d_ptr->known_hosts, qPrintable(file), LIBSSH2_KNOWNHOST_FILE_OPENSSH) == 0);
}

bool QSshConnectionManager::saveKnownHosts(const QString &file, QSshClient::KnownHostsFormat c) const
{
	Q_UNUSED(c);
	return (libssh2_knownhost_writefile(d_ptr->known_hosts, qPrintable(file), LIBSSH2_KNOWNHOST_FILE_OPENSSH) == 0);
}

QSshConnectionManager &QSshConnectionManager::instance()
{
	static QSharedPointer<QSshConnectionManager> g_connection_manager;
	if (!g_connection_manager)
		g_connection_manager.reset(new QSshConnectionManager());
	return *g_connection_manager;
}

QSshAskCredentials::QSshAskCredentials(const QString &host, const QString &username, bool user_editable,
                                       QWidget *parent)
    : QDialog(parent), ui(new Ui_QSshAskCredentials)
{
	ui->setupUi(this);
	ui->hostLabel->setText(tr("Connecting to %1").arg(host));
	ui->usernameEdit->setPlaceholderText(qgetenv("USER"));
	ui->usernameEdit->setText(username);
	ui->passwordEdit->setEchoMode(QLineEdit::Password);
	ui->usernameEdit->setVisible(user_editable);
	ui->usernameLabel->setVisible(user_editable);
	ui->passwordEdit->setFocus();
}

QSshAskCredentials::~QSshAskCredentials()
{
	delete ui;
}

QString QSshAskCredentials::userName() const
{
	return ui->usernameEdit->text();
}

QString QSshAskCredentials::password() const
{
	return ui->passwordEdit->text();
}
