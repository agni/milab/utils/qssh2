/*!
    \class QSshChannel
    \inmodule QNetwork
    \brief The QSshChannel class provides common functionality for QSshClient channels

    QSshChannel is the base interface class for all I/O channels created by QSshClient.

    As a QIODevice subclass, QSshChannel exposes all of the normal methods common to Qt
    I/O classes, including the readyRead() signal and the read() and write() methods.

    QSshChannel is an interface class providing the foundations for the various channel
    types offered by QSsh. It is not intended to be instantiated directly nor is it
    intended to be subclassed by user code. Use the convenience methods on QSshClient
    such as QSshClient::openProcessChannel() and QSshClient::openTcpSocket().
*/

/*!
 * \fn QSshChannel::connected()
 *
 * This signal is emitted when the channel has been successfully opened.
 */

#include "qsshchannel.h"
#include "qsshchannel_p.h"

/*! \internal */
QSshChannel::QSshChannel(QSshClient *parent) : QIODevice(parent), d(new QSshChannelPrivate(this, parent)) {}

/*!
 * Destroys the QSshChannel object.
 */
QSshChannel::~QSshChannel()
{
	delete d;
}

QSshChannelPrivate::QSshChannelPrivate(QSshChannel *_p, QSshClient *c)
    : QObject(nullptr)
    , p(_p)
    , d_client(c)
    , d_channel(nullptr)
    , d_session(d_client->d->d_session)
    , d_read_stream_id(0)
    , d_write_stream_id(0)
    , d_state(IDLE)
{}

QSshChannelPrivate::~QSshChannelPrivate()
{
	d_client->d->d_channels.removeOne(p);
}

/*!
    \reimp
*/
qint64 QSshChannel::readData(char *buff, qint64 len)
{
	ssize_t ret = libssh2_channel_read_ex(d->d_channel, d->d_read_stream_id, buff, len);
	if (ret < 0) {
		if (ret == LIBSSH2_ERROR_EAGAIN) {
			return 0;
		} else {
#ifdef QSSH2_DEBUG
			qDebug() << "read err" << ret;
#endif
			return -1;
		}
	}
	if (ret == 0)
		Q_EMIT disconnected();
	return ret;
}

/*!
    \reimp
*/
qint64 QSshChannel::writeData(const char *buff, qint64 len)
{
	ssize_t ret = libssh2_channel_write_ex(d->d_channel, d->d_write_stream_id, buff, len);
	if (ret < 0) {
		if (ret == LIBSSH2_ERROR_EAGAIN) {
			return 0;
		} else {
#ifdef QSSH2_DEBUG
			qDebug() << "write err" << ret;
#endif
			return -1;
		}
	}
	return ret;
}
/*!
 * \reimp
 */
bool QSshChannel::isSequential() const
{
	return true;
}

bool QSshChannelPrivate::activate()
{
	int rc;
	switch (d_state) {
		case OPEN: // open session
			d_channel = libssh2_channel_open_session(d_session);
			if (d_channel == nullptr) {
				return libssh2_session_last_error(d_session, nullptr, nullptr, 0) == LIBSSH2_ERROR_EAGAIN;
			}
#ifdef QSSH2_DEBUG
			qDebug("session opened");
#endif
			d_state = READY;
			return activate();
			break;

		case READY: // transition to allow early cmd
			if (!d_next_actions.isEmpty()) {
				d_state = d_next_actions.takeFirst();
				return activate();
			} else
				return true;
			break;

		case REQUEST_PTY:
			rc = libssh2_channel_request_pty(d_channel, d_pty.data());
			if (rc) {
				if (rc == LIBSSH2_ERROR_EAGAIN) {
					return true;
				} else {
					qWarning("QSshChannel: pty allocation failed");
					return false;
				}
			}
#ifdef QSSH2_DEBUG
			qDebug("pty opened");
#endif
			d_state = READY;
			return activate();

		case START:
			rc = libssh2_channel_exec(d_channel, qPrintable(d_cmd));
			if (rc)
				return (rc == LIBSSH2_ERROR_EAGAIN);
#ifdef QSSH2_DEBUG
			qDebug("exec opened");
#endif
			p->setOpenMode(QIODevice::ReadWrite);
			d_state = READ_CHANNEL;
			emit p->connected();
			return true;

		case START_SHELL:
			rc = libssh2_channel_shell(d_channel);
			if (rc)
				return (rc == LIBSSH2_ERROR_EAGAIN);
#ifdef QSSH2_DEBUG
			qDebug("shell opened");
#endif
			p->setOpenMode(QIODevice::ReadWrite);
			d_state = READ_CHANNEL;
			emit p->connected();
			return true;

		case TCP_CHANNEL:
			d_channel = libssh2_channel_direct_tcpip(d_session, qPrintable(d_host), d_port);
			if (d_channel == nullptr) {
				return libssh2_session_last_error(d_session, nullptr, nullptr, 0) == LIBSSH2_ERROR_EAGAIN;
			}
#ifdef QSSH2_DEBUG
			qDebug("tcp channel opened");
#endif
			p->setOpenMode(QIODevice::ReadWrite);
			d_state = READ_CHANNEL;
			return activate();

		case READ_CHANNEL:
			emit p->readyRead();
			break;

		case CLOSE:
			rc = libssh2_channel_close(d_channel);
			if (rc)
				return (rc == LIBSSH2_ERROR_EAGAIN);
			else
				Q_EMIT p->disconnected();
			break;

		default:
			return true; // nothing to do
	}
	return true;
}

void QSshChannelPrivate::openSession()
{
	if (d_state != IDLE)
		return;

	d_state = OPEN;
	activate();
}

void QSshChannelPrivate::requestPty(const QByteArray &pty)
{
	if (d_state > REQUEST_PTY)
		return;

	d_pty = pty;
	if (d_state == READY) {
		d_state = REQUEST_PTY;
	} else {
		if (!d_next_actions.contains(REQUEST_PTY))
			d_next_actions.append(REQUEST_PTY);
	}
	activate();
}

void QSshChannelPrivate::start(const QString &cmd)
{
	if (d_state > REQUEST_PTY)
		return;

	d_cmd = cmd;
	if (d_state == READY) {
		d_state = START;
	} else {
		if (!d_next_actions.contains(START))
			d_next_actions.append(START);
	}
	activate();
}

void QSshChannelPrivate::startShell()
{
	if (d_state > REQUEST_PTY)
		return;

	if (d_state == READY) {
		d_state = START_SHELL;
	} else {
		if (!d_next_actions.contains(START_SHELL))
			d_next_actions.append(START_SHELL);
	}
	activate();
}

void QSshChannelPrivate::openTcpSocket(const QString &host, qint16 port)
{
	if (d_state != IDLE)
		return;

	d_host  = host;
	d_port  = port;
	d_state = TCP_CHANNEL;
	activate();
}

void QSshChannelPrivate::close()
{
	if (d_state != READ_CHANNEL)
		return;

	d_state = CLOSE;
	activate();
}
