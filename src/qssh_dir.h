#pragma once

#include <QObject>
#include "qsshsftp.h"
#include "qssh2_export.h"

class QSshSftp;
class QSshDirPrivate;

class QSSH2_EXPORT QSshDir : public QObject
{
	Q_OBJECT
	friend class QSshSftp;
	friend class QSshSftpPrivate;

	QSshDirPrivate *d_ptr;

public:
	~QSshDir();
	QString name() const;

	/// list contents of directory (non-blocking, signal by entries())
	void list();

	int lastError() const;

protected:
	QSshDir(const QString &name, QSshSftp *sftp);

signals:
	void entries(const QList<QSshFileInfo> &entries);
	void error(int sftp_error);
	void closed() const;
};
