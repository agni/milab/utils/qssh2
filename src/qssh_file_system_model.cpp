#include "qssh_file_system_model.h"

#include "QSshDir"
#include "QSshDirDeleter"
#include "QSshFile"
#include "QSshExtendedInformation"

#include <libssh2_sftp.h>

#include <algorithm>
#include <memory>

#include <QLocale>
#include <QMimeData>
#include <QUrl>
#include <QDebug>
#include <QMessageBox>
#include <QApplication>
#include <QPair>
#include <QDir>
#include <QIcon>
#include <QFileInfo>
#include <QTimer>
#include <QHash>
#include <QDateTime>
#include <QFileIconProvider>

#ifdef Q_OS_WIN
#include <QtCore/QVarLengthArray>
#include <qt_windows.h>
#endif

class QSSH2_EXPORT QSshFileSystemModelPrivate
{
	Q_DECLARE_PUBLIC(QSshFileSystemModel)
	Q_DISABLE_COPY(QSshFileSystemModelPrivate)
	friend class QSshFileDialogPrivate;

public:
	class QSshFileSystemNode
	{
	public:
		explicit QSshFileSystemNode(const QString &filename = QString(), QSshFileSystemNode *p = nullptr)
		    : fileName(filename), parent(p)
		{}
		~QSshFileSystemNode()
		{
			QHash<QString, QSshFileSystemNode *>::const_iterator i = children.constBegin();
			while (i != children.constEnd()) {
				delete i.value();
				++i;
			}
		}

		QString fileName;
#if defined(Q_OS_WIN) && !defined(Q_OS_WINCE)
		QString volumeName;
#endif

		inline qint64 size() const
		{
			if (info && !info->isDir())
				return info->size();
			return 0;
		}
		inline QString type() const
		{
			if (info)
				return info->displayType;
			return QLatin1String("");
		}
		inline QDateTime lastModified() const
		{
			if (info)
				return info->lastModified();
			return QDateTime();
		}
		inline QFile::Permissions permissions() const
		{
			if (info)
				return info->permissions();
			return nullptr;
		}
		inline bool isReadable() const { return ((permissions() & QFile::ReadUser) != 0); }
		inline bool isWritable() const { return ((permissions() & QFile::WriteUser) != 0); }
		inline bool isExecutable() const { return ((permissions() & QFile::ExeUser) != 0); }
		inline bool isDir() const
		{
			if (info)
				return info->isDir();
			return children.count() > 0;
		}
		inline bool isFile() const
		{
			if (info)
				return info->isFile();
			return true;
		}
		inline bool isSystem() const
		{
			if (info)
				return info->isSystem();
			return true;
		}
		inline bool isHidden() const { return info->fileInfo().name.startsWith("."); }
		inline bool isSymLink(bool /*unused*/) const { return info && info->isSymLink(); }
		inline bool caseSensitive() const { return true; }
		inline QIcon icon() const
		{
			if (info)
				return info->icon;
			return QIcon();
		}

		inline bool operator<(const QSshFileSystemNode &node) const
		{
			if (caseSensitive() || node.caseSensitive())
				return fileName < node.fileName;
			return QString::compare(fileName, node.fileName, Qt::CaseInsensitive) < 0;
		}
		inline bool operator>(const QString &name) const
		{
			if (caseSensitive())
				return fileName > name;
			return QString::compare(fileName, name, Qt::CaseInsensitive) > 0;
		}
		inline bool operator<(const QString &name) const
		{
			if (caseSensitive())
				return fileName < name;
			return QString::compare(fileName, name, Qt::CaseInsensitive) < 0;
		}

		inline bool operator!=(const QSshExtendedInformation &fileInfo) const { return !operator==(fileInfo); }

		bool operator==(const QString &name) const
		{
			if (caseSensitive())
				return fileName == name;
			return QString::compare(fileName, name, Qt::CaseInsensitive) == 0;
		}

		bool operator==(const QSshExtendedInformation &fileInfo) const { return info && (*info == fileInfo); }

		inline bool hasInformation() const { return info != nullptr; }

		void populate(const QSshExtendedInformation &fileInfo)
		{
			if (!info)
				info = std::make_unique<QSshExtendedInformation>(fileInfo.fileInfo());
			(*info) = fileInfo;
		}

		QSshExtendedInformation extendedInformation() const
		{
			if (!hasInformation())
				return QSshExtendedInformation();
			return (*info);
		}

		// children shouldn't normally be accessed directly, use node()
		inline int visibleLocation(const QString &childName) { return visibleChildren.indexOf(childName); }
		void updateIcon(QFileIconProvider *iconProvider, const QString &path)
		{
			if (info)
				info->icon = iconProvider->icon(QFileInfo(path));
			QHash<QString, QSshFileSystemNode *>::const_iterator iterator;
			for (iterator = children.constBegin(); iterator != children.constEnd(); ++iterator) {
				// On windows the root (My computer) has no path so we don't want to add a / for nothing (e.g. /C:/)
				if (!path.isEmpty()) {
					if (path.endsWith(QLatin1Char('/')))
						iterator.value()->updateIcon(iconProvider, path + iterator.value()->fileName);
					else
						iterator.value()->updateIcon(iconProvider, path + QLatin1Char('/') + iterator.value()->fileName);
				} else
					iterator.value()->updateIcon(iconProvider, iterator.value()->fileName);
			}
		}

		void retranslateStrings(QFileIconProvider *iconProvider, const QString &path)
		{
			if (info)
				info->displayType = iconProvider->type(QFileInfo(path));
			QHash<QString, QSshFileSystemNode *>::const_iterator iterator;
			for (iterator = children.constBegin(); iterator != children.constEnd(); ++iterator) {
				// On windows the root (My computer) has no path so we don't want to add a / for nothing (e.g. /C:/)
				if (!path.isEmpty()) {
					if (path.endsWith(QLatin1Char('/')))
						iterator.value()->retranslateStrings(iconProvider, path + iterator.value()->fileName);
					else
						iterator.value()->retranslateStrings(iconProvider,
						                                     path + QLatin1Char('/') + iterator.value()->fileName);
				} else
					iterator.value()->retranslateStrings(iconProvider, iterator.value()->fileName);
			}
		}

		bool populatedChildren{ false };
		bool isVisible{ false };
		QHash<QString, QSshFileSystemNode *> children;
		QList<QString> visibleChildren;
		int dirtyChildrenIndex{ -1 };
		QSshFileSystemNode *parent{ nullptr };
		std::unique_ptr<QSshExtendedInformation> info;
	};

	QSshFileSystemModelPrivate(QSshSftp *sftp, QSshFileSystemModel *q_ptr) : q_ptr(q_ptr), sftp(sftp)
	{
		Q_Q(QSshFileSystemModel);
		delayedSortTimer.setSingleShot(true);
		q->connect(
		    &delayedSortTimer, &QTimer::timeout, q_ptr, [&]() { _q_performDelayedSort(); }, Qt::QueuedConnection);
	}
	/*
	  \internal

	  Return true if index which is owned by node is hidden by the filter.
	*/
	inline bool isHiddenByFilter(QSshFileSystemNode *indexNode, const QModelIndex &index) const
	{
		return (indexNode != &root && !index.isValid());
	}
	QSshFileSystemNode *node(const QModelIndex &index) const;
	QSshFileSystemNode *node(const QString &path) const;
	inline QModelIndex index(const QString &path)
	{
		return index(node(path));
	} // TODO node() can throw -> maybe we should catch here and handle it
	QModelIndex index(const QSshFileSystemNode *const node) const;
	bool filtersAcceptsNode(const QSshFileSystemNode *node) const;
	bool passNameFilters(const QSshFileSystemNode *node) const;
	void removeNode(QSshFileSystemNode *parentNode, const QString &name);
	QSshFileSystemNode *addNode(QSshFileSystemNode *parentNode, const QString &fileName, const QSshFileInfo &info);
	void addVisibleFiles(QSshFileSystemNode *parentNode, const QStringList &newFiles);
	void removeVisibleFile(QSshFileSystemNode *parentNode, int visibleLocation);
	void sortChildren(int column, const QModelIndex &parent);

	inline int translateVisibleLocation(QSshFileSystemNode *parent, int row) const
	{
		if (sortOrder != Qt::AscendingOrder) {
			if (parent->dirtyChildrenIndex == -1)
				return parent->visibleChildren.count() - row - 1;

			if (row < parent->dirtyChildrenIndex)
				return parent->dirtyChildrenIndex - row - 1;
		}

		return row;
	}

	inline static QString myComputer()
	{
		// ### TODO We should query the system to find out what the string should be
		// XP == "My Computer",
		// Vista == "Computer",
		// OS X == "Computer" (sometime user generated) "Benjamin's PowerBook G4"
#ifdef Q_OS_WIN
		return "My Computer";
#else
		return "Computer";
#endif
	}

	inline void delayedSort()
	{
		if (!delayedSortTimer.isActive())
			delayedSortTimer.start(0);
	}

	static bool caseInsensitiveLessThan(const QString &s1, const QString &s2)
	{
		return QString::compare(s1, s2, Qt::CaseInsensitive) < 0;
	}

	static bool nodeCaseInsensitiveLessThan(const QSshFileSystemModelPrivate::QSshFileSystemNode &s1,
	                                        const QSshFileSystemModelPrivate::QSshFileSystemNode &s2)
	{
		return QString::compare(s1.fileName, s2.fileName, Qt::CaseInsensitive) < 0;
	}

	bool indexValid(const QModelIndex &index) const
	{
		return (index.row() >= 0) && (index.column() >= 0) && (index.model() == q_func());
	}

	QIcon icon(const QModelIndex &index) const;
	QString name(const QModelIndex &index) const;
	QString displayName(const QModelIndex &index) const;
	QString filePath(const QModelIndex &index) const;
	// returns the file name of path without the directory
	QString fileName(const QString &path);
	QString size(const QModelIndex &index) const;
	static QString size(qint64 bytes);
	QString type(const QModelIndex &index) const;
	QString time(const QModelIndex &index) const;
	uint uid(const QModelIndex &index) const;
	uint gid(const QModelIndex &index) const;
	static QString permToString(const QFileDevice::Permissions &permissions);
	static int naturalCompare(const QString &s1, const QString &s2, Qt::CaseSensitivity cs);
	void removeRows(const QModelIndex &parent, int first, int last);

	void _q_directoryChanged(const QString &directory, const QStringList &list);
	void _q_performDelayedSort();
	void _q_fileSystemChanged(const QString &path, const QList<QPair<QString, QSshFileInfo>> & /*updates*/);
	void _q_resolvedName(const QString &fileName, const QString &resolvedName);

	// Slots for sftp stuff
	void _q_onConnected();
	void _q_onDirEntries(const QList<QSshFileInfo> &entries);
	void _q_onDirError(int sftp_error);
	void _q_onDirDone();

	QSshFileSystemModel *q_ptr{ nullptr };
	QDir rootDir;
	QSshSftp *sftp{ nullptr };
	QFileIconProvider defaultProvider;
	QFileIconProvider *iconProvider{ &defaultProvider };
	QTimer delayedSortTimer;
	bool forceSort{ true };
	int sortColumn{ 0 };
	Qt::SortOrder sortOrder{ Qt::AscendingOrder };
	bool readOnly{ true };
	bool setRootPath{ false };
	QDir::Filters filters{ QDir::AllEntries | QDir::NoDotAndDotDot | QDir::AllDirs };
	bool nameFilterDisables{ true }; // FIXME: what does this mean? false on windows, true on mac and unix
	// This flag is an optimization for the QSshFileDialog
	// It enables a sort which is not recursive, it means we sort only what we see.
	bool disableRecursiveSort{ true }; // FIXME originally set to false, but in the fileDialog set to true anyway
	QList<QRegExp> nameFilters;
	QHash<QString, QString> resolvedSymLinks;
	QSshFileSystemNode root;
	QMap<QSshDir *, QModelIndex> _dirs; // Stores the dirs that are listing instead of QList<Fetching> toFetch
};

/*!
   \fn bool QSshFileSystemModel::remove(const QModelIndex &index)

   Removes the model item \a index from the file system model and \b{deletes the
   corresponding file from the file system}, returning true if successful. If the
   item cannot be removed, false is returned.

   \warning This function deletes files from the file system; it does \b{not}
   move them to a location where they can be recovered.

   \sa rmdir()
*/
bool QSshFileSystemModel::remove(const QModelIndex &aindex)
{
	Q_D(QSshFileSystemModel);
	const QString path = filePath(aindex);
	if (isDir(aindex) || !d->sftp->isConnected())
		return false;
	try {
		d->sftp->unlink(path);
	} catch (QSshSftp::exception e) {
		qDebug() << "error in rmDir(): " << e.what();
		return false;
	}
	// no error
	d->removeNode(d->node(aindex.parent()), path);
	return true;
}

/*!
  Constructs a file system model with the given \a parent.
*/
QSshFileSystemModel::QSshFileSystemModel(QSshSftp *sftp, QObject *parent)
    : QAbstractItemModel(parent), d_ptr(new QSshFileSystemModelPrivate(sftp, this))
{
	if (sftp->isConnected())
		d_ptr->_q_onConnected();
	else
		connect(sftp, &QSshSftp::connected, this, [&]() { d_ptr->_q_onConnected(); });
}

QSshFileSystemModel::~QSshFileSystemModel() = default;

/*!
   \reimp
*/
QModelIndex QSshFileSystemModel::index(int row, int column, const QModelIndex &parent) const
{
	Q_D(const QSshFileSystemModel);
	if (row < 0 || column < 0 || row >= rowCount(parent) || column >= columnCount(parent))
		return QModelIndex();

	// get the parent node
	QSshFileSystemModelPrivate::QSshFileSystemNode *parentNode =
	    (d->indexValid(parent) ? d->node(parent) :
	                             const_cast<QSshFileSystemModelPrivate::QSshFileSystemNode *>(&d->root));
	Q_ASSERT(parentNode);

	// now get the internal pointer for the index
	QString childName = parentNode->visibleChildren[d->translateVisibleLocation(parentNode, row)];
	const QSshFileSystemModelPrivate::QSshFileSystemNode *indexNode = parentNode->children.value(childName);
	Q_ASSERT(indexNode);

	return createIndex(row, column, const_cast<QSshFileSystemModelPrivate::QSshFileSystemNode *>(indexNode));
}

/*!
   \overload

   Returns the model item index for the given \a path and \a column.
*/
QModelIndex QSshFileSystemModel::index(const QString &path, int column) const
{
	Q_D(const QSshFileSystemModel);
	QSshFileSystemModelPrivate::QSshFileSystemNode *node = d->node(path);
	QModelIndex idx                                      = d->index(node);
	if (idx.column() != column)
		idx = idx.sibling(idx.row(), column);
	return idx;
}

/*!
   \internal

   Return the QSshFileSystemNode that goes to index.
  */
QSshFileSystemModelPrivate::QSshFileSystemNode *QSshFileSystemModelPrivate::node(const QModelIndex &index) const
{
	if (!index.isValid())
		return const_cast<QSshFileSystemNode *>(&root);
	QSshFileSystemModelPrivate::QSshFileSystemNode *indexNode =
	    static_cast<QSshFileSystemModelPrivate::QSshFileSystemNode *>(index.internalPointer());
	Q_ASSERT(indexNode);
	return indexNode;
}

/*!
   \internal

   Given a path return the matching QSshFileSystemNode or &root if invalid
*/
QSshFileSystemModelPrivate::QSshFileSystemNode *QSshFileSystemModelPrivate::node(const QString &path) const
{
	if (path.isEmpty() || path == myComputer() || path.startsWith(QLatin1Char(':')) || !sftp->isConnected())
		return const_cast<QSshFileSystemModelPrivate::QSshFileSystemNode *>(&root);

	// Construct the nodes up to the new root path if they need to be built
	QStringList pathElements = path.split(QLatin1Char('/'), QString::SkipEmptyParts);
	if ((pathElements.isEmpty()) && path != QLatin1String("/"))
		return const_cast<QSshFileSystemModelPrivate::QSshFileSystemNode *>(&root);
	QModelIndex index = QModelIndex(); // start with "My Computer"

	if (path.startsWith("/"))
		pathElements.prepend("/");
	QSshFileSystemModelPrivate::QSshFileSystemNode *parent = node(index);
	QString fetching;
	for (const QString &element : qAsConst(pathElements)) {
		if (fetching.isEmpty())
			fetching = element;
		else if (fetching == "/")
			fetching = fetching + element;
		else
			fetching = fetching + "/" + element;
		bool alreadyExisted = parent->children.contains(element);
		// we couldn't find the path element, we create a new node since we
		// _know_ that the path is valid
		if (alreadyExisted) {
			if ((parent->children.count() == 0) ||
			    (parent->caseSensitive() && parent->children.value(element)->fileName != element) ||
			    (!parent->caseSensitive() && parent->children.value(element)->fileName.toLower() != element.toLower()))
				alreadyExisted = false;
		}

		QSshFileSystemModelPrivate::QSshFileSystemNode *node;
		if (!alreadyExisted) {
			// Someone might call ::index("file://cookie/monster/doesn't/like/veggies"),
			// a path that doesn't exists, I.E. don't blindly create directories.
			QSshFileInfo info;
			try {
				info = sftp->statFile(fetching);
			} catch (QSshSftp::exception &e) {
				if (e.error() != LIBSSH2_ERROR_SFTP_PROTOCOL)
					throw;
				// the equivalent of !info.exists is not being able to statFile
				return const_cast<QSshFileSystemModelPrivate::QSshFileSystemNode *>(&root);
			}
			QSshFileSystemModelPrivate *p = const_cast<QSshFileSystemModelPrivate *>(this);
			node                          = p->addNode(parent, element, info);
		} else {
			node = parent->children.value(element);
		}

		Q_ASSERT(node);
		if (!node->isVisible) {
			// It has been filtered out
			if (alreadyExisted && node->hasInformation())
				return const_cast<QSshFileSystemModelPrivate::QSshFileSystemNode *>(&root);

			QSshFileSystemModelPrivate *p = const_cast<QSshFileSystemModelPrivate *>(this);
			p->addVisibleFiles(parent, QStringList(element));
		}
		parent = node;
	}
	return parent;
}

/*!
   Returns \c true if the model item \a index represents a directory;
   otherwise returns \c false.
*/
bool QSshFileSystemModel::isDir(const QModelIndex &index) const
{
	// This function is for public usage only because it could create a file info
	Q_D(const QSshFileSystemModel);
	if (!index.isValid())
		return true;
	QSshFileSystemModelPrivate::QSshFileSystemNode *n = d->node(index);
	if (n->hasInformation())
		return n->isDir();
	return fileInfo(index).isDir();
}

/*!
   Returns the size in bytes of \a index. If the file does not exist, 0 is returned.
  */
qint64 QSshFileSystemModel::size(const QModelIndex &index) const
{
	Q_D(const QSshFileSystemModel);
	if (!index.isValid())
		return 0;
	return d->node(index)->size();
}

/*!
   Returns the type of file \a index such as "Directory" or "JPEG file".
  */
QString QSshFileSystemModel::type(const QModelIndex &index) const
{
	Q_D(const QSshFileSystemModel);
	if (!index.isValid())
		return QString();
	return d->node(index)->type();
}

/*!
   Returns the date and time when \a index was last modified.
 */
QDateTime QSshFileSystemModel::lastModified(const QModelIndex &index) const
{
	Q_D(const QSshFileSystemModel);
	if (!index.isValid())
		return QDateTime();
	return d->node(index)->lastModified();
}

/*!
   \reimp
*/
QModelIndex QSshFileSystemModel::parent(const QModelIndex &index) const
{
	Q_D(const QSshFileSystemModel);
	if (!d->indexValid(index))
		return QModelIndex();

	QSshFileSystemModelPrivate::QSshFileSystemNode *indexNode = d->node(index);
	Q_ASSERT(indexNode != nullptr);
	QSshFileSystemModelPrivate::QSshFileSystemNode *parentNode = indexNode->parent;
	if (parentNode == nullptr || parentNode == &d->root)
		return QModelIndex();

	// get the parent's row
	QSshFileSystemModelPrivate::QSshFileSystemNode *grandParentNode = parentNode->parent;
	Q_ASSERT(grandParentNode->children.contains(parentNode->fileName));
	int visualRow = d->translateVisibleLocation(
	    grandParentNode,
	    grandParentNode->visibleLocation(grandParentNode->children.value(parentNode->fileName)->fileName));
	if (visualRow == -1)
		return QModelIndex();
	return createIndex(visualRow, 0, parentNode);
}

/*
   \internal

   return the index for node
*/
QModelIndex QSshFileSystemModelPrivate::index(const QSshFileSystemModelPrivate::QSshFileSystemNode *const node) const
{
	Q_Q(const QSshFileSystemModel);
	QSshFileSystemModelPrivate::QSshFileSystemNode *parentNode = (node ? node->parent : nullptr);
	if (node == &root || !parentNode)
		return QModelIndex();

	// get the parent's row
	Q_ASSERT(node);
	if (!node->isVisible)
		return QModelIndex();

	int visualRow = translateVisibleLocation(parentNode, parentNode->visibleLocation(node->fileName));
	return q->createIndex(visualRow, 0, const_cast<QSshFileSystemNode *>(node));
}

/*!
   \reimp
*/
bool QSshFileSystemModel::hasChildren(const QModelIndex &parent) const
{
	Q_D(const QSshFileSystemModel);
	if (parent.column() > 0)
		return false;

	if (!parent.isValid()) // drives
		return true;

	const QSshFileSystemModelPrivate::QSshFileSystemNode *indexNode = d->node(parent);
	Q_ASSERT(indexNode);
	return (indexNode->isDir());
}

/*!
   \reimp
 */
bool QSshFileSystemModel::canFetchMore(const QModelIndex &parent) const
{
	Q_D(const QSshFileSystemModel);
	const QSshFileSystemModelPrivate::QSshFileSystemNode *indexNode = d->node(parent);
	return (!indexNode->populatedChildren);
}

/*!
   \reimp
 */
void QSshFileSystemModel::fetchMore(const QModelIndex &parent)
{
	Q_D(QSshFileSystemModel);
	if (!parent.isValid() || !d->sftp->isConnected())
		return;
	if (!d->setRootPath)
		return;
	QSshFileSystemModelPrivate::QSshFileSystemNode *indexNode = d->node(parent);
	if (indexNode->populatedChildren)
		return;
	indexNode->populatedChildren = true;
	// this is supposed to fetch the info
	QSshDir *dir = d->sftp->dir(filePath(parent));
	d->_dirs.insert(dir, parent);

	QObject::connect(dir, &QSshDir::error, this, [d](int err) { d->_q_onDirError(err); });
	QObject::connect(dir, &QSshDir::entries, this,
	                 [d](const QList<QSshFileInfo> &entries) { d->_q_onDirEntries(entries); });
	QObject::connect(dir, &QSshDir::closed, this, [d]() { d->_q_onDirDone(); });
	dir->list();
}

/*!
   \reimp
*/
int QSshFileSystemModel::rowCount(const QModelIndex &parent) const
{
	Q_D(const QSshFileSystemModel);
	if (parent.column() > 0)
		return 0;

	if (!parent.isValid())
		return d->root.visibleChildren.count();

	const QSshFileSystemModelPrivate::QSshFileSystemNode *parentNode = d->node(parent);
	return parentNode->visibleChildren.count();
}

/*!
   \reimp
*/
int QSshFileSystemModel::columnCount(const QModelIndex &parent) const
{
	return (parent.column() > 0) ? 0 : 6;
}

/*!
   Returns the data stored under the given \a role for the item "My Computer".

   \sa Qt::ItemDataRole
 */
QVariant QSshFileSystemModel::myComputer(int role) const
{
	Q_D(const QSshFileSystemModel);
	switch (role) {
		case Qt::DisplayRole:
			return QSshFileSystemModelPrivate::myComputer();
		case Qt::DecorationRole:
			return d->iconProvider->icon(QFileIconProvider::Computer);
	}
	return QVariant();
}

/*!
   \reimp
*/
QVariant QSshFileSystemModel::data(const QModelIndex &index, int role) const
{
	Q_D(const QSshFileSystemModel);
	if (!index.isValid() || index.model() != this)
		return QVariant();

	switch (role) {
		case Qt::EditRole:
		case Qt::DisplayRole:
			switch (index.column()) {
				case 0:
					return d->displayName(index);
				case 1:
					return d->uid(index);
				case 2:
					return d->gid(index);
				case 3:
					return d->size(index);
				case 4:
					return d->permToString(permissions(index));
				case 5:
					return d->time(index);
				default:
					qWarning("data: invalid display value column %d", index.column());
					break;
			}
			break;
		case FilePathRole:
			return filePath(index);
		case FileNameRole:
			return d->name(index);
		case Qt::DecorationRole:
			if (index.column() == 0) {
				QIcon icon = d->icon(index);
				if (icon.isNull()) {
					if (d->node(index)->isDir())
						icon = d->iconProvider->icon(QFileIconProvider::Folder);
					else
						icon = d->iconProvider->icon(QFileIconProvider::File);
				}
				return icon;
			}
			break;
		case Qt::TextAlignmentRole:
			if (index.column() == 1)
				return Qt::AlignRight;
			break;
		case FilePermissions:
			int p = permissions(index);
			return p;
	}

	return QVariant();
}

/*!
   \internal
*/
QString QSshFileSystemModelPrivate::size(const QModelIndex &index) const
{
	if (!index.isValid())
		return QString();
	const QSshFileSystemNode *n = node(index);
	if (n->isDir()) {
		return QLatin1String("");
		// Windows   - ""
		// OS X      - "--"
		// Konqueror - "4 KB"
		// Nautilus  - "9 items" (the number of children)
	}
	return size(n->size());
}

QString QSshFileSystemModelPrivate::size(qint64 bytes)
{
	// According to the Si standard KB is 1000 bytes, KiB is 1024
	// but on windows sizes are calculated by dividing by 1024 so we do what they do.
	const qint64 kb = 1024;
	const qint64 mb = 1024 * kb;
	const qint64 gb = 1024 * mb;
	const qint64 tb = 1024 * gb;
	if (bytes >= tb)
		return QString("%1 TB").arg(QLocale().toString(qreal(bytes) / tb, 'f', 3));
	if (bytes >= gb)
		return QString("%1 GB").arg(QLocale().toString(qreal(bytes) / gb, 'f', 2));
	if (bytes >= mb)
		return QString("%1 MB").arg(QLocale().toString(qreal(bytes) / mb, 'f', 1));
	if (bytes >= kb)
		return QString("%1 KB").arg(QLocale().toString(bytes / kb));
	return QString("%1 bytes").arg(QLocale().toString(bytes));
}

/*!
   \internal
*/
QString QSshFileSystemModelPrivate::time(const QModelIndex &index) const
{
	if (!index.isValid())
		return QString();
	return node(index)->lastModified().toString(Qt::SystemLocaleDate);
}

uint QSshFileSystemModelPrivate::uid(const QModelIndex &index) const
{
	QSshFileSystemModelPrivate::QSshFileSystemNode *n = node(index);
	return n->info->fileInfo().uid;
}

uint QSshFileSystemModelPrivate::gid(const QModelIndex &index) const
{
	QSshFileSystemModelPrivate::QSshFileSystemNode *n = node(index);
	return n->info->fileInfo().gid;
}

QString QSshFileSystemModelPrivate::permToString(const QFileDevice::Permissions &permissions)
{
	QString perm;

	permissions.testFlag(QFile::ReadUser) ? perm.append("r") : perm.append("-");
	permissions.testFlag(QFile::WriteUser) ? perm.append("w") : perm.append("-");
	permissions.testFlag(QFile::ExeUser) ? perm.append("x") : perm.append("-");
	perm.append("  ");

	permissions.testFlag(QFile::ReadGroup) ? perm.append("r") : perm.append("-");
	permissions.testFlag(QFile::WriteGroup) ? perm.append("w") : perm.append("-");
	permissions.testFlag(QFile::ExeGroup) ? perm.append("x") : perm.append("-");
	perm.append("  ");

	permissions.testFlag(QFile::ReadOther) ? perm.append("r") : perm.append("-");
	permissions.testFlag(QFile::WriteOther) ? perm.append("w") : perm.append("-");
	permissions.testFlag(QFile::ExeOther) ? perm.append("x") : perm.append("-");
	return perm;
}

/*
   \internal
*/
QString QSshFileSystemModelPrivate::type(const QModelIndex &index) const
{
	if (!index.isValid())
		return QString();
	return node(index)->type();
}

/*!
   \internal
*/
QString QSshFileSystemModelPrivate::name(const QModelIndex &index) const
{
	if (!index.isValid())
		return QString();
	QSshFileSystemNode *dirNode = node(index);
	return dirNode->fileName;
}

/*!
   \internal
*/
QString QSshFileSystemModelPrivate::displayName(const QModelIndex &index) const
{
#if defined(Q_OS_WIN) && !defined(Q_OS_WINCE)
	QSshFileSystemNode *dirNode = node(index);
	if (!dirNode->volumeName.isNull())
		return dirNode->volumeName + QLatin1String(" (") + name(index) + QLatin1Char(')');
#endif
	return name(index);
}

/*!
   \internal
*/
QIcon QSshFileSystemModelPrivate::icon(const QModelIndex &index) const
{
	if (!index.isValid())
		return QIcon();
	return node(index)->icon();
}

/*!
   \reimp
*/
bool QSshFileSystemModel::setData(const QModelIndex &idx, const QVariant &value, int role)
{
	Q_D(QSshFileSystemModel);
	if (!idx.isValid() || idx.column() != 0 || role != Qt::EditRole || (flags(idx) & Qt::ItemIsEditable) == 0 ||
	    !d->sftp->isConnected()) {
		return false;
	}

	QString newName     = value.toString();
	QString newPathName = d->filePath(idx.parent()) + "/" + newName;
	QString oldPathName = d->filePath(idx);
	QString oldName     = idx.data().toString();
	if (newName == idx.data().toString())
		return true;

	if (newName.isEmpty() || newName.contains("/") || newName.contains("\"")) {
		QMessageBox::information(
		    nullptr, QSshFileSystemModel::tr("Invalid filename"),
		    QSshFileSystemModel::tr("<b>The name \"%1\" can not be used.</b><p>"
		                            "Try using another name, with fewer characters or no punctuations marks.")
		        .arg(newName),
		    QMessageBox::Ok);
		return false;
	}

	try {
		d->sftp->rename(oldPathName, newPathName);
	} catch (QSshSftp::exception e) {
		qDebug() << "error in rename(): " << e.what();
		return false;
	}
	/*
	  After re-naming something we don't want the selection to change
	  - can't remove rows and later insert
	  - can't quickly remove and insert
	  - index pointer can't change because treeview doesn't use persistant index's

	  - if this get any more complicated think of changing it to just
	  use layoutChanged
	*/

	QSshFileSystemModelPrivate::QSshFileSystemNode *indexNode  = d->node(idx);
	QSshFileSystemModelPrivate::QSshFileSystemNode *parentNode = indexNode->parent;
	int visibleLocation = parentNode->visibleLocation(parentNode->children.value(indexNode->fileName)->fileName);

	d->addNode(parentNode, newName, indexNode->info->fileInfo());
	parentNode->visibleChildren.removeAt(visibleLocation);
	QSshFileSystemModelPrivate::QSshFileSystemNode *oldValue = parentNode->children.value(oldName);
	parentNode->children[newName]                            = oldValue;
	oldValue->fileName                                       = newName;
	oldValue->parent                                         = parentNode;

	try {
		oldValue->populate(d->sftp->statFile(d->rootDir.absolutePath() + "/" + newName));
	} catch (QSshSftp::exception &e) {
		qDebug() << "can not statFile: " << d->rootDir.absolutePath() + "/" + newName << e.what();
		return false;
	}

	oldValue->isVisible = true;
	parentNode->children.remove(oldName);
	parentNode->visibleChildren.insert(visibleLocation, newName);

	d->delayedSort();
	emit fileRenamed(filePath(idx.parent()), oldName, newName);
	return true;
}

// checks if folderName exists as a child of prefix
bool QSshFileSystemModel::exists(const QString &prefix, const QString &folderName) const
{
	Q_D(const QSshFileSystemModel);
	QSshFileSystemModelPrivate::QSshFileSystemNode *node = d->node(prefix);
	return node->children.contains(folderName);
}

/*!
   \reimp
*/
QVariant QSshFileSystemModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	switch (role) {
		case Qt::DecorationRole:
			if (section == 0) {
				// ### TODO oh man this is ugly and doesn't even work all the way!
				// it is still 2 pixels off
				QImage pixmap(16, 1, QImage::Format_Mono);
				pixmap.fill(0);
				pixmap.setAlphaChannel(pixmap.createAlphaMask());
				return pixmap;
			}
			break;
		case Qt::TextAlignmentRole:
			return Qt::AlignLeft;
	}

	if (orientation != Qt::Horizontal || role != Qt::DisplayRole)
		return QAbstractItemModel::headerData(section, orientation, role);

	switch (section) {
		case 0:
			return "Name";
		case 1:
			return "uid";
		case 2:
			return "gid";
		case 3:
			return "size";
		case 4:
			return "permissions";
		case 5:
			return "Date Modified";
		default:
			return QVariant();
	}
	return QVariant();
}

/*!
   \reimp
*/
Qt::ItemFlags QSshFileSystemModel::flags(const QModelIndex &index) const
{
	Q_D(const QSshFileSystemModel);
	Qt::ItemFlags flags = QAbstractItemModel::flags(index);
	if (!index.isValid())
		return flags;

	QSshFileSystemModelPrivate::QSshFileSystemNode *indexNode = d->node(index);
	if (d->nameFilterDisables && !d->passNameFilters(indexNode)) {
		flags &= ~Qt::ItemIsEnabled;
		// ### TODO you shouldn't be able to set this as the current item, task 119433
		return flags;
	}
	flags |= Qt::ItemIsDragEnabled;
	if (d->readOnly)
		return flags;
	if ((index.column() == 0) && indexNode->permissions() & QFile::WriteUser) {
		flags |= Qt::ItemIsEditable;
		if (indexNode->isDir())
			flags |= Qt::ItemIsDropEnabled;
		else
			flags |= Qt::ItemNeverHasChildren;
	}
	return flags;
}

/*!
   \internal
*/
void QSshFileSystemModelPrivate::_q_performDelayedSort()
{
	Q_Q(QSshFileSystemModel);
	q->sort(sortColumn, sortOrder);
}

static inline QChar getNextChar(const QString &s, int location)
{
	return (location < s.length()) ? s.at(location) : QChar();
}

/*!
   Natural number sort, skips spaces.

   Examples:
   1, 2, 10, 55, 100
   01.jpg, 2.jpg, 10.jpg

   Note on the algorithm:
   Only as many characters as necessary are looked at and at most they all
   are looked at once.

   Slower then QString::compare() (of course)
  */
int QSshFileSystemModelPrivate::naturalCompare(const QString &s1, const QString &s2, Qt::CaseSensitivity cs)
{
	for (int l1 = 0, l2 = 0; l1 <= s1.count() && l2 <= s2.count(); ++l1, ++l2) {
		// skip spaces, tabs and 0's
		QChar c1 = getNextChar(s1, l1);
		while (c1.isSpace())
			c1 = getNextChar(s1, ++l1);
		QChar c2 = getNextChar(s2, l2);
		while (c2.isSpace())
			c2 = getNextChar(s2, ++l2);

		if (c1.isDigit() && c2.isDigit()) {
			while (c1.digitValue() == 0)
				c1 = getNextChar(s1, ++l1);
			while (c2.digitValue() == 0)
				c2 = getNextChar(s2, ++l2);

			int lookAheadLocation1 = l1;
			int lookAheadLocation2 = l2;
			int currentReturnValue = 0;
			// find the last digit, setting currentReturnValue as we go if it isn't equal
			for (QChar lookAhead1 = c1, lookAhead2 = c2;
			     (lookAheadLocation1 <= s1.length() && lookAheadLocation2 <= s2.length());
			     lookAhead1 = getNextChar(s1, ++lookAheadLocation1), lookAhead2 = getNextChar(s2, ++lookAheadLocation2)) {
				bool is1ADigit = !lookAhead1.isNull() && lookAhead1.isDigit();
				bool is2ADigit = !lookAhead2.isNull() && lookAhead2.isDigit();
				if (!is1ADigit && !is2ADigit)
					break;
				if (!is1ADigit)
					return -1;
				if (!is2ADigit)
					return 1;
				if (currentReturnValue == 0) {
					if (lookAhead1 < lookAhead2) {
						currentReturnValue = -1;
					} else if (lookAhead1 > lookAhead2) {
						currentReturnValue = 1;
					}
				}
			}
			if (currentReturnValue != 0)
				return currentReturnValue;
		}

		if (cs == Qt::CaseInsensitive) {
			if (!c1.isLower())
				c1 = c1.toLower();
			if (!c2.isLower())
				c2 = c2.toLower();
		}
		int r = QString::localeAwareCompare(c1, c2);
		if (r < 0)
			return -1;
		if (r > 0)
			return 1;
	}
	// The two strings are the same (02 == 2) so fall back to the normal sort
	return QString::compare(s1, s2, cs);
}

QString QSshFileSystemModelPrivate::fileName(const QString &path)
{
	int lastSeparator = path.lastIndexOf("/");
	return path.mid(lastSeparator + 1);
}

/*
   \internal
   Helper functor used by sort()
*/
class QSshFileSystemModelSorter
{
public:
	inline QSshFileSystemModelSorter(int column) : sortColumn(column) {}

	bool compareNodes(const QSshFileSystemModelPrivate::QSshFileSystemNode *l,
	                  const QSshFileSystemModelPrivate::QSshFileSystemNode *r) const
	{
		switch (sortColumn) {
			case 0: { // names
				// place directories before files
				bool left  = l->isDir();
				bool right = r->isDir();
				if (left ^ right)
					return left;
				return QSshFileSystemModelPrivate::naturalCompare(l->fileName, r->fileName, Qt::CaseInsensitive) < 0;
			}
			case 1: { // uid
				// Directories go first
				bool left  = l->isDir();
				bool right = r->isDir();
				if (left ^ right)
					return left;
				int compare = l->info->fileInfo().uid - r->info->fileInfo().uid;
				if (compare == 0)
					return QSshFileSystemModelPrivate::naturalCompare(l->fileName, r->fileName, Qt::CaseInsensitive) < 0;
				return compare < 0;
			}
			case 2: { // gid
				// Directories go first
				bool left  = l->isDir();
				bool right = r->isDir();
				if (left ^ right)
					return left;
				int compare = l->info->fileInfo().gid - r->info->fileInfo().gid;
				if (compare == 0)
					return QSshFileSystemModelPrivate::naturalCompare(l->fileName, r->fileName, Qt::CaseInsensitive) < 0;
				return compare < 0;
			}
			case 3: { // size
				// Directories go first
				bool left  = l->isDir();
				bool right = r->isDir();
				if (left ^ right)
					return left;
				qint64 sizeDifference = l->size() - r->size();
				if (sizeDifference == 0)
					return QSshFileSystemModelPrivate::naturalCompare(l->fileName, r->fileName, Qt::CaseInsensitive) < 0;
				return sizeDifference < 0;
			}
			case 4: { // permissions
				// Directories go first
				bool left  = l->isDir();
				bool right = r->isDir();
				if (left ^ right)
					return left;
				int compare =
				    QString::localeAwareCompare(QSshFileSystemModelPrivate::permToString(l->info->fileInfo().permissions),
				                                QSshFileSystemModelPrivate::permToString(r->info->fileInfo().permissions));
				if (compare == 0)
					return QSshFileSystemModelPrivate::naturalCompare(l->fileName, r->fileName, Qt::CaseInsensitive) < 0;
				return compare < 0;
			}
			case 5: {
				// Directories go first
				bool left  = l->isDir();
				bool right = r->isDir();
				if (left ^ right)
					return left;
				if (l->lastModified() == r->lastModified())
					return QSshFileSystemModelPrivate::naturalCompare(l->fileName, r->fileName, Qt::CaseInsensitive) < 0;
				return l->lastModified() < r->lastModified();
			}
		}
		return false;
	}

	bool operator()(const QPair<QSshFileSystemModelPrivate::QSshFileSystemNode *, int> &l,
	                const QPair<QSshFileSystemModelPrivate::QSshFileSystemNode *, int> &r) const
	{
		return compareNodes(l.first, r.first);
	}

private:
	int sortColumn;
};

/*
   \internal

   Sort all of the children of parent
*/
void QSshFileSystemModelPrivate::sortChildren(int column, const QModelIndex &parent)
{
	Q_Q(QSshFileSystemModel);
	QSshFileSystemModelPrivate::QSshFileSystemNode *indexNode = node(parent);
	if (indexNode->children.count() == 0)
		return;

	QList<QPair<QSshFileSystemModelPrivate::QSshFileSystemNode *, int>> values;
	QHash<QString, QSshFileSystemNode *>::const_iterator iterator;
	int i = 0;
	for (iterator = indexNode->children.constBegin(); iterator != indexNode->children.constEnd(); ++iterator) {
		if (filtersAcceptsNode(iterator.value())) {
			values.append(QPair<QSshFileSystemModelPrivate::QSshFileSystemNode *, int>((iterator.value()), i));
		} else {
			iterator.value()->isVisible = false;
		}
		i++;
	}
	QSshFileSystemModelSorter ms(column);
	std::sort(values.begin(), values.end(), ms);
	// First update the new visible list
	indexNode->visibleChildren.clear();
	// No more dirty item we reset our internal dirty index
	indexNode->dirtyChildrenIndex = -1;
	for (int i = 0; i < values.count(); ++i) {
		indexNode->visibleChildren.append(values.at(i).first->fileName);
		values.at(i).first->isVisible = true;
	}

	if (!disableRecursiveSort) {
		for (int i = 0; i < q->rowCount(parent); ++i) {
			const QModelIndex childIndex                              = q->index(i, 0, parent);
			QSshFileSystemModelPrivate::QSshFileSystemNode *indexNode = node(childIndex);
			// Only do a recursive sort on visible nodes
			if (indexNode->isVisible)
				sortChildren(column, childIndex);
		}
	}
}

/*!
   \reimp
*/
void QSshFileSystemModel::sort(int column, Qt::SortOrder order)
{
	Q_D(QSshFileSystemModel);
	if (!d->setRootPath)
		return; // rootPath has not been set so far
	if (d->sortOrder == order && d->sortColumn == column && !d->forceSort)
		return;

	emit layoutAboutToBeChanged();
	QModelIndexList oldList = persistentIndexList();
	QList<QPair<QSshFileSystemModelPrivate::QSshFileSystemNode *, int>> oldNodes;
	for (int i = 0; i < oldList.count(); ++i) {
		QPair<QSshFileSystemModelPrivate::QSshFileSystemNode *, int> pair(d->node(oldList.at(i)), oldList.at(i).column());
		oldNodes.append(pair);
	}

	if (!(d->sortColumn == column && d->sortOrder != order && !d->forceSort)) {
		// we sort only from where we are, don't need to sort all the model
		d->sortChildren(column, index(rootPath()));
		d->sortColumn = column;
		d->forceSort  = false;
	}
	d->sortOrder = order;

	QModelIndexList newList;
	for (int i = 0; i < oldNodes.count(); ++i) {
		QModelIndex idx = d->index(oldNodes.at(i).first);
		idx             = idx.sibling(idx.row(), oldNodes.at(i).second);
		newList.append(idx);
	}
	changePersistentIndexList(oldList, newList);
	emit layoutChanged();
}

/*!
   Returns a list of MIME types that can be used to describe a list of items
   in the model.
*/
QStringList QSshFileSystemModel::mimeTypes() const
{
	return QStringList(QLatin1String("text/uri-list"));
}

/*!
   Returns an object that contains a serialized description of the specified
   \a indexes. The format used to describe the items corresponding to the
   indexes is obtained from the mimeTypes() function.

   If the list of indexes is empty, 0 is returned rather than a serialized
   empty list.
*/
QMimeData *QSshFileSystemModel::mimeData(const QModelIndexList &indexes) const
{
	QList<QUrl> urls;
	QList<QModelIndex>::const_iterator it = indexes.begin();
	for (; it != indexes.end(); ++it)
		if ((*it).column() == 0)
			urls << QUrl::fromLocalFile(filePath(*it));
	QMimeData *data = new QMimeData();
	data->setUrls(urls);
	return data;
}

/*!
   Handles the \a data supplied by a drag and drop operation that ended with
   the given \a action over the row in the model specified by the \a row and
   \a column and by the \a parent index.

   \sa supportedDropActions()
*/
bool QSshFileSystemModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column,
                                       const QModelIndex &parent)
{
	Q_UNUSED(row);
	Q_UNUSED(column);
	Q_D(QSshFileSystemModel);
	if (!parent.isValid() || isReadOnly())
		return false;

	bool success = true;
	QString to   = filePath(parent) + QDir::separator();

	QList<QUrl> urls               = data->urls();
	QList<QUrl>::const_iterator it = urls.constBegin();

	switch (action) {
		case Qt::CopyAction:
			throw; // TODO implement copy or remove
			for (; it != urls.constEnd(); ++it) {
				QString fileName     = d->fileName((*it).toLocalFile());
				QSshFile *fileHandle = d->sftp->file(fileName);
				fileHandle->open(QIODevice::WriteOnly);
				if (!fileHandle->waitForOpened(500))
					qDebug() << "failed to open" << fileName;
				else
					qDebug() << __FUNCTION__ << "needs to be implemented";
				// fileHandle->write(data); //TODO writing data
				delete fileHandle;
				return false;
			}
			break;
		case Qt::LinkAction:
			for (; it != urls.constEnd(); ++it) {
				QString path     = (*it).toLocalFile();
				QString fileName = d->fileName(path);
				QString linkPath = to + fileName;
				try {
					success = d->sftp->link(path, linkPath) && success;
				} catch (QSshSftp::exception e) {
					qDebug() << "error in dropMimeData / Qt::LinkAction: " << e.what();
					return false;
				}
				d->node(linkPath); // adds the node
			}
			break;
		case Qt::MoveAction:
			for (; it != urls.constEnd(); ++it) {
				QString path     = (*it).toLocalFile();
				QString fileName = d->fileName(path);
				QString newPath  = to + fileName;
				try {
					success = d->sftp->rename(path, newPath) && success;
				} catch (QSshSftp::exception e) {
					qDebug() << "error in dropMimeData / Qt::MoveAction: " << e.what();
					return false;
				}
				if (success) {
					QModelIndex idx = index(path);
					d->removeNode(d->node(idx.parent()), path);
					d->node(newPath); // adds the node
				}
			}
			break;
		default:
			return false;
	}
	return success;
}

/*!
   \reimp
*/
Qt::DropActions QSshFileSystemModel::supportedDropActions() const
{
	return Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
}

/*!
   Returns the path of the item stored in the model under the
   \a index given.
*/
QString QSshFileSystemModel::filePath(const QModelIndex &index) const
{
	Q_D(const QSshFileSystemModel);
	if (!index.isValid())
		return d->rootDir.absolutePath();
	QString fullPath = d->filePath(index);
	return fullPath;
}

QString QSshFileSystemModelPrivate::filePath(const QModelIndex &index) const
{
	Q_Q(const QSshFileSystemModel);
	if (!index.isValid())
		return QString();
	Q_ASSERT(index.model() == q);

	QStringList path;
	QModelIndex idx = index;
	while (idx.isValid()) {
		QSshFileSystemModelPrivate::QSshFileSystemNode *dirNode = node(idx);
		if (dirNode)
			path.prepend(dirNode->fileName);
		idx = idx.parent();
	}
	QString fullPath = path.join("/");
	if (fullPath.startsWith("//"))
		fullPath.remove(1, 1);
	return fullPath;
}

/*!
   Create a directory with the \a name in the \a parent model index.
*/
QModelIndex QSshFileSystemModel::mkdir(const QModelIndex &parent, const QString &name)
{
	Q_D(QSshFileSystemModel);
	if (!parent.isValid() || !d->sftp->isConnected())
		return parent;
	QString pathName(filePath(parent) + "/" + name);
	QFile::Permissions perm =
	    QFile::ReadUser | QFile::WriteUser | QFile::ExeUser | QFile::ReadGroup | QFile::ExeGroup | QFile::ExeOther;
	try {
		d->sftp->mkdir(pathName, perm);
	} catch (QSshSftp::exception e) {
		qDebug() << "error in mkDir(): " << e.what();
		return QModelIndex();
	}

	QSshFileSystemModelPrivate::QSshFileSystemNode *parentNode = d->node(parent);
	d->addNode(parentNode, name, QSshFileInfo());
	Q_ASSERT(parentNode->children.contains(name));
	QSshFileSystemModelPrivate::QSshFileSystemNode *node = parentNode->children[name];

	try {
		node->populate(d->sftp->statFile(pathName));
	} catch (QSshSftp::exception &e) {
		qDebug() << "can not statFile: " << pathName << e.what();
		return QModelIndex();
	}
	d->addVisibleFiles(parentNode, QStringList(name));
	return d->index(node);
}

/*!
   Returns the complete OR-ed together combination of QFile::Permission for the \a index.
 */
QFile::Permissions QSshFileSystemModel::permissions(const QModelIndex &index) const
{
	Q_D(const QSshFileSystemModel);
	return d->node(index)->permissions();
}

QSshFileInfo QSshFileSystemModel::fileInfo(const QModelIndex &index) const
{
	Q_D(const QSshFileSystemModel);
	QSshFileSystemModelPrivate::QSshFileSystemNode *n = d->node(index);
	return n->extendedInformation().fileInfo();
}

/*!
   Sets the directory that is being displayed by the model to \a newPath. Any
   changes to files and directories within this directory will NOT be
   reflected in the model.

   If the path is changed, the rootPathChanged() signal will be emitted.

   \note This function does not change the structure of the model or
   modify the data available to views. In other words, the "root" of
   the model is \e not changed to include only files and directories
   within the directory specified by \a newPath in the file system.
  */
QModelIndex QSshFileSystemModel::setRootPath(const QString &newPath)
{
	if (newPath == ".")
		return QModelIndex();
	Q_D(QSshFileSystemModel);
	if (!d->sftp->isConnected()) {
		// schedule setRootPath for later, when we are connected
		connect(d->sftp, &QSshSftp::connected, this, [this, newPath]() { setRootPath(newPath); });
		return QModelIndex();
	}
	QSshDir *newPathDir = d->sftp->dir(newPath);
	d->setRootPath      = true;

	// user don't ask for the root path ("") but the conversion failed
	if (!newPath.isEmpty() && newPath.isEmpty())
		return d->index(rootPath());

	if (d->rootDir.path() == newPath)
		return d->index(rootPath());

	bool showDrives = (newPath.isEmpty() || newPath == d->myComputer());
	// check if the path exists
	bool exists = false;
	try {
		QSshFileInfo info = d->sftp->statFile(newPath);
		exists            = !info.name.isEmpty();
	} catch (QSshSftp::exception &e) {
		qDebug() << "can not statFile: " << newPath << e.what();
		return d->index(rootPath());
	}
	if (!showDrives && !exists)
		return d->index(rootPath());

	// We have a new valid root path
	d->rootDir = QDir(newPathDir->name());
	QModelIndex newRootIndex;
	if (showDrives) {
		// otherwise dir will become '/'
		d->rootDir.setPath("/");
	} else {
		newRootIndex = d->index(newPathDir->name());
	}
	fetchMore(newRootIndex); // TODO is this needed?
	emit rootPathChanged(newPath);
	d->forceSort = true;
	d->delayedSort();
	return newRootIndex;
}

/*!
   The currently set root path

   \sa rootDirectory()
*/
QString QSshFileSystemModel::rootPath() const
{
	Q_D(const QSshFileSystemModel);
	return d->rootDir.path();
}

/*!
   The currently set directory

   \sa rootPath()
*/
QDir QSshFileSystemModel::rootDirectory() const
{
	Q_D(const QSshFileSystemModel);
	QDir dir(d->rootDir);
	dir.setNameFilters(nameFilters());
	dir.setFilter(filter());
	return dir;
}

/*!
   Sets the \a provider of file icons for the directory model.
*/
void QSshFileSystemModel::setIconProvider(QFileIconProvider *provider)
{
	Q_D(QSshFileSystemModel);
	d->iconProvider = provider;
	d->root.updateIcon(provider, QString());
}

/*!
   Returns the file icon provider for this directory model.
*/
QFileIconProvider *QSshFileSystemModel::iconProvider() const
{
	Q_D(const QSshFileSystemModel);
	return d->iconProvider;
}

/*!
   Sets the directory model's filter to that specified by \a filters.

   Note that the filter you set should always include the QDir::AllDirs enum value,
   otherwise NewQSshFileSystemModel won't be able to read the directory structure.

   \sa QDir::Filters
*/
void QSshFileSystemModel::setFilter(QDir::Filters filters)
{
	Q_D(QSshFileSystemModel);
	if (d->filters == filters)
		return;
	d->filters = filters;
	// CaseSensitivity might have changed
	setNameFilters(nameFilters());
	d->forceSort = true;
	d->delayedSort();
}

/*!
   Returns the filter specified for the directory model.

   If a filter has not been set, the default filter is QDir::AllEntries |
   QDir::NoDotAndDotDot | QDir::AllDirs.

   \sa QDir::Filters
*/
QDir::Filters QSshFileSystemModel::filter() const
{
	Q_D(const QSshFileSystemModel);
	return d->filters;
}

/*!
   \property NewQSshFileSystemModel::readOnly
   \brief Whether the directory model allows writing to the file system

   If this property is set to false, the directory model will allow renaming, copying
   and deleting of files and directories.

   This property is \c true by default
*/
void QSshFileSystemModel::setReadOnly(bool enable)
{
	Q_D(QSshFileSystemModel);
	d->readOnly = enable;
}

bool QSshFileSystemModel::isReadOnly() const
{
	Q_D(const QSshFileSystemModel);
	return d->readOnly;
}

/*!
   \property NewQSshFileSystemModel::nameFilterDisables
   \brief Whether files that don't pass the name filter are hidden or disabled

   This property is \c true by default
*/
void QSshFileSystemModel::setNameFilterDisables(bool enable)
{
	Q_D(QSshFileSystemModel);
	if (d->nameFilterDisables == enable)
		return;
	d->nameFilterDisables = enable;
	d->forceSort          = true;
	d->delayedSort();
}

bool QSshFileSystemModel::nameFilterDisables() const
{
	Q_D(const QSshFileSystemModel);
	return d->nameFilterDisables;
}

/*!
   Sets the name \a filters to apply against the existing files.
*/
void QSshFileSystemModel::setNameFilters(const QStringList &filters)
{
	// Prep the regexp's ahead of time
	Q_D(QSshFileSystemModel);

	// We guarantee that rootPath will stick around
	QPersistentModelIndex root(index(rootPath()));
	QModelIndexList persistantList = persistentIndexList();
	for (int i = 0; i < persistantList.count(); ++i) {
		QSshFileSystemModelPrivate::QSshFileSystemNode *node;
		node = d->node(persistantList.at(i));
		while (node) {
			node = node->parent;
		}
	}

	d->nameFilters.clear();
	const Qt::CaseSensitivity caseSensitive = (filter() & QDir::CaseSensitive) ? Qt::CaseSensitive : Qt::CaseInsensitive;
	for (int i = 0; i < filters.size(); ++i) {
		d->nameFilters << QRegExp(filters.at(i), caseSensitive, QRegExp::Wildcard);
	}
	d->forceSort = true;
	d->delayedSort();
}

/*!
   Returns a list of filters applied to the names in the model.
*/
QStringList QSshFileSystemModel::nameFilters() const
{
	Q_D(const QSshFileSystemModel);
	QStringList filters;
	for (const auto &regexp : qAsConst(d->nameFilters))
		filters << regexp.pattern();
	return filters;
}

/*!
   \reimp
*/
bool QSshFileSystemModel::event(QEvent *event)
{
	Q_D(QSshFileSystemModel);
	if (event->type() == QEvent::LanguageChange) {
		d->root.retranslateStrings(d->iconProvider, QString());
		return true;
	}
	return QAbstractItemModel::event(event);
}

bool QSshFileSystemModel::rmDir(const QModelIndex &aindex)
{
	Q_D(QSshFileSystemModel);
	const QString path = filePath(aindex);
	if (!isDir(aindex) || !d->sftp->isConnected())
		return false;
	QSshFileSystemModelPrivate::QSshFileSystemNode *n = d->node(aindex);
	// can not remove non empty dirs, "." and ".." are alway there (after fetching)
	// the dir needs to be fetched to be able to delete it
	// thus we only delete if children.size is exactly 2
	if (canFetchMore(aindex))
		fetchMore(aindex);

	if (n->children.size() != 2) {
		if (QMessageBox::information(
		        nullptr, QSshFileSystemModel::tr("Directory is not empty"),
		        QSshFileSystemModel::tr("<b>The Directory \"%1\" is not empty.</b><p> It can not be deleted.")
		            .arg(d->name(aindex)),
		        QMessageBox::Ok))
			return false;
	}
	try {
		d->sftp->rmdir(path);
	} catch (QSshSftp::exception e) {
		qDebug() << "error in rmDir(): " << e.what();
		return false;
	}
	// no error
	d->removeNode(d->node(aindex.parent()), path);
	return true;
}
/*!
 * \brief NewQSshFileSystemModel::rmDirRecursive is there to recursively delete dirs
 * \param aindex starting index of deletion
 * \return
 */
bool QSshFileSystemModel::rmDirRecursive(const QModelIndex &aindex)
{
	Q_D(QSshFileSystemModel);
	// we need full pathName
	QString pathName = filePath(aindex);
	if (pathName == rootPath() || !d->sftp->isConnected())
		return false;
	if (pathName == "/") // safety valve dont delete "/"
		return false;
	QSshDirDeleter *deleter = new QSshDirDeleter(d->sftp, this);
	bool deleted            = deleter->rm(pathName);
	// no error
	d->removeNode(d->node(aindex.parent()), pathName);
	return deleted;
}

/*!
    \internal

   Performed quick listing and see if any files have been added or removed,
   then fetch more information on visible files.
 */
void QSshFileSystemModelPrivate::_q_directoryChanged(const QString &directory, const QStringList &files)
{
	QSshFileSystemModelPrivate::QSshFileSystemNode *parentNode = node(directory);
	if (parentNode->children.count() == 0)
		return;
	QStringList toRemove;
	QStringList newFiles = files;
	std::sort(newFiles.begin(), newFiles.end());
	QHash<QString, QSshFileSystemNode *>::const_iterator i = parentNode->children.constBegin();
	while (i != parentNode->children.constEnd()) {
		QStringList::iterator iterator = std::lower_bound(newFiles.begin(), newFiles.end(), i.value()->fileName);
		if ((iterator == newFiles.end()) || (i.value()->fileName < *iterator))
			toRemove.append(i.value()->fileName);

		++i;
	}
	for (int i = 0; i < toRemove.count(); ++i)
		removeNode(parentNode, toRemove[i]);
}

/*!
   \internal

   Adds a new file to the children of parentNode

   *WARNING* this will change the count of children
*/
QSshFileSystemModelPrivate::QSshFileSystemNode *QSshFileSystemModelPrivate::addNode(QSshFileSystemNode *parentNode,
                                                                                    const QString &fileName,
                                                                                    const QSshFileInfo &info)
{
	// In the common case, itemLocation == count() so check there first
	QSshFileSystemModelPrivate::QSshFileSystemNode *node =
	    new QSshFileSystemModelPrivate::QSshFileSystemNode(fileName, parentNode);
	node->populate(info);
	parentNode->children.insert(fileName, node);
	return node;
}

/*!
   \internal

   File at parentNode->children(itemLocation) has been removed, remove from the lists
   and emit signals if necessary

   *WARNING* this will change the count of children and could change visibleChildren
 */
void QSshFileSystemModelPrivate::removeNode(QSshFileSystemModelPrivate::QSshFileSystemNode *parentNode,
                                            const QString &name)
{
	Q_Q(QSshFileSystemModel);
	QModelIndex parent = index(parentNode);
	bool indexHidden   = isHiddenByFilter(parentNode, parent);

	QString base  = name.mid(filePath(parent).length()).section('/', 0, -1, QString::SectionSkipEmpty);
	int vLocation = parentNode->visibleLocation(base);
	if (vLocation >= 0 && !indexHidden)
		q->beginRemoveRows(parent, translateVisibleLocation(parentNode, vLocation),
		                   translateVisibleLocation(parentNode, vLocation));
	QSshFileSystemNode *node = parentNode->children.take(base);
	delete node;
	// cleanup sort files after removing rather then re-sorting which is O(n)
	if (vLocation >= 0)
		parentNode->visibleChildren.removeAt(vLocation);
	if (vLocation >= 0 && !indexHidden)
		q->endRemoveRows();
}

/*
   \internal
   Helper functor used by addVisibleFiles()
*/
class QSshFileSystemModelVisibleFinder
{
public:
	inline QSshFileSystemModelVisibleFinder(QSshFileSystemModelPrivate::QSshFileSystemNode *node,
	                                        QSshFileSystemModelSorter *sorter)
	    : parentNode(node), sorter(sorter)
	{}

	bool operator()(const QString & /*unused*/, const QString &r) const
	{
		return sorter->compareNodes(parentNode->children.value(name), parentNode->children.value(r));
	}

	QString name;

private:
	QSshFileSystemModelPrivate::QSshFileSystemNode *parentNode;
	QSshFileSystemModelSorter *sorter;
};

/*!
   \internal

   File at parentNode->children(itemLocation) was not visible before, but now should be
   and emit signals if necessary.

   *WARNING* this will change the visible count
 */
void QSshFileSystemModelPrivate::addVisibleFiles(QSshFileSystemNode *parentNode, const QStringList &newFiles)
{
	Q_Q(QSshFileSystemModel);
	QModelIndex parent = index(parentNode);
	bool indexHidden   = isHiddenByFilter(parentNode, parent);
	if (!indexHidden) {
		q->beginInsertRows(parent, parentNode->visibleChildren.count(),
		                   parentNode->visibleChildren.count() + newFiles.count() - 1);
	}

	if (parentNode->dirtyChildrenIndex == -1)
		parentNode->dirtyChildrenIndex = parentNode->visibleChildren.count();

	for (int i = 0; i < newFiles.count(); ++i) {
		parentNode->visibleChildren.append(newFiles.at(i));
		parentNode->children[newFiles.at(i)]->isVisible = true;
	}
	if (!indexHidden)
		q->endInsertRows();
}

/*!
   \internal

   File was visible before, but now should NOT be

   *WARNING* this will change the visible count
 */
void QSshFileSystemModelPrivate::removeVisibleFile(QSshFileSystemNode *parentNode, int vLocation)
{
	Q_Q(QSshFileSystemModel);
	if (vLocation == -1)
		return;
	QModelIndex parent = index(parentNode);
	bool indexHidden   = isHiddenByFilter(parentNode, parent);
	if (!indexHidden)
		q->beginRemoveRows(parent, translateVisibleLocation(parentNode, vLocation),
		                   translateVisibleLocation(parentNode, vLocation));
	parentNode->children[parentNode->visibleChildren.at(vLocation)]->isVisible = false;
	parentNode->visibleChildren.removeAt(vLocation);
	if (!indexHidden)
		q->endRemoveRows();
}

/*!
   \internal

   The sftp has received new information about files,
   update and emit dataChanged if it has actually changed.
 */
void QSshFileSystemModelPrivate::_q_fileSystemChanged(const QString &path,
                                                      const QList<QPair<QString, QSshFileInfo>> &updates)
{
	Q_Q(QSshFileSystemModel);
	QVector<QString> rowsToUpdate;
	QStringList newFiles;
	QSshFileSystemModelPrivate::QSshFileSystemNode *parentNode = node(path);
	QModelIndex parentIndex                                    = index(parentNode);
	for (int i = 0; i < updates.count(); ++i) {
		QString fileName = updates.at(i).first;
		Q_ASSERT(!fileName.isEmpty());
		QSshExtendedInformation info = updates.at(i).second;
		bool previouslyHere          = parentNode->children.contains(fileName);
		if (!previouslyHere) {
			addNode(parentNode, fileName, info.fileInfo());
		}
		QSshFileSystemModelPrivate::QSshFileSystemNode *node = parentNode->children.value(fileName);
		bool isCaseSensitive                                 = parentNode->caseSensitive();
		if (isCaseSensitive) {
			if (node->fileName != fileName)
				continue;
		} else {
			if (QString::compare(node->fileName, fileName, Qt::CaseInsensitive) != 0)
				continue;
		}
		if (isCaseSensitive) {
			Q_ASSERT(node->fileName == fileName);
		} else {
			node->fileName = fileName;
		}

		if (info.size() == -1 && !info.isSymLink()) {
			removeNode(parentNode, fileName);
			continue;
		}
		// We dont care if the info has changed, we need to update our list

		if (filtersAcceptsNode(node)) {
			if (!node->isVisible) {
				newFiles.append(fileName);
			} else {
				rowsToUpdate.append(fileName);
			}
		} else {
			if (node->isVisible) {
				int visibleLocation = parentNode->visibleLocation(fileName);
				removeVisibleFile(parentNode, visibleLocation);
			} else {
				// The file is not visible, don't do anything
			}
		}
	}

	// bundle up all of the changed signals into as few as possible.
	std::sort(rowsToUpdate.begin(), rowsToUpdate.end());
	QString min;
	QString max;
	for (const QString &value : rowsToUpdate) {
		max            = value;
		min            = value;
		int visibleMin = parentNode->visibleLocation(min);
		int visibleMax = parentNode->visibleLocation(max);
		if (visibleMin >= 0 && visibleMin < parentNode->visibleChildren.count() &&
		    parentNode->visibleChildren.at(visibleMin) == min && visibleMax >= 0) {
			QModelIndex bottom = q->index(translateVisibleLocation(parentNode, visibleMin), 0, parentIndex);
			QModelIndex top    = q->index(translateVisibleLocation(parentNode, visibleMax), 3, parentIndex);
			emit q->dataChanged(bottom, top);
		}
	}

	if (newFiles.count() > 0) {
		addVisibleFiles(parentNode, newFiles);
	}

	if (newFiles.count() > 0 || (sortColumn != 0 && rowsToUpdate.count() > 0)) {
		forceSort = true;
		delayedSort();
	}
}

/*!
   \internal
*/
void QSshFileSystemModelPrivate::_q_resolvedName(const QString &fileName, const QString &resolvedName)
{
	resolvedSymLinks[fileName] = resolvedName;
}

void QSshFileSystemModelPrivate::_q_onConnected()
{
	Q_Q(QSshFileSystemModel);
	QSshFileInfo info;
	while (true) {
		try {
			// root is empty -> dont fetch
			if (root.fileName.isEmpty())
				return;
			else
				info = sftp->statFile(root.fileName);
			break;
		} catch (const QSshSftp::exception &) {
			root.fileName = root.fileName.section('/', 0, -2);
			// avoid an infinite loop, usually "/" should be found
			if (root.fileName.isEmpty())
				return;
		}
	}
	if (info.name.isEmpty()) {
		rootDir   = QDir("/");
		info.name = rootDir.path();
	}

	q->beginResetModel();
	q->setRootPath(info.name);
	q->endResetModel();
}

/* TODO: removing a node doesn't yet stop listing of the directory.
 * This leads to additional entries in the associated QModelIndex.
 *	Hence, if a node is removed, any pending dir->list() should be stopped.
 */
void QSshFileSystemModelPrivate::_q_onDirEntries(const QList<QSshFileInfo> &entries)
{
	QSshDir *dir       = static_cast<QSshDir *>(q_ptr->sender());
	QModelIndex parent = _dirs.value(dir);

	QList<QPair<QString, QSshFileInfo>> updatedFiles;
	for (const auto &entry : entries)
		updatedFiles.append(QPair<QString, QSshFileInfo>(entry.name, entry));

	_q_fileSystemChanged(filePath(parent), updatedFiles);
}

void QSshFileSystemModelPrivate::_q_onDirError(int sftp_error)
{
	QSshDir *dir = static_cast<QSshDir *>(q_ptr->sender());
	qDebug() << "Access failed for" << qPrintable(dir->name()) << qPrintable(sftp_error_msg(sftp_error));
	_q_onDirDone();
}

void QSshFileSystemModelPrivate::_q_onDirDone()
{
	QSshDir *dir = static_cast<QSshDir *>(q_ptr->sender());
	_dirs.remove(dir);
	delete dir;
}

/*!
   \internal

   Returns \c false if node doesn't pass the filters otherwise true

   QDir::Modified is not supported
   QDir::Drives is not supported
*/
bool QSshFileSystemModelPrivate::filtersAcceptsNode(const QSshFileSystemNode *node) const
{
	// always accept drives
	if (node->parent == &root)
		return true;

	// If we don't know anything yet don't accept it
	if (!node->hasInformation())
		return false;

	const bool filterPermissions =
	    ((filters & QDir::PermissionMask) && (filters & QDir::PermissionMask) != QDir::PermissionMask);
	const bool hideDirs       = !(filters & (QDir::Dirs | QDir::AllDirs));
	const bool hideFiles      = !(filters & QDir::Files);
	const bool hideReadable   = !(!filterPermissions || (filters & QDir::Readable));
	const bool hideWritable   = !(!filterPermissions || (filters & QDir::Writable));
	const bool hideExecutable = !(!filterPermissions || (filters & QDir::Executable));
	const bool hideHidden     = !(filters & QDir::Hidden);
	const bool hideSystem     = !(filters & QDir::System);
	const bool hideDot        = (filters & QDir::NoDot);
	const bool hideDotDot     = (filters & QDir::NoDotDot);

	// Note that we match the behavior of entryList and not QFileInfo on this.
	bool isDot    = (node->fileName == QLatin1String("."));
	bool isDotDot = (node->fileName == QLatin1String(".."));
	if ((hideHidden && !(isDot || isDotDot) && node->isHidden()) || (hideSystem && node->isSystem()) ||
	    (hideDirs && node->isDir()) || (hideFiles && node->isFile()) || (hideReadable && node->isReadable()) ||
	    (hideWritable && node->isWritable()) || (hideExecutable && node->isExecutable()) || (hideDot && isDot) ||
	    (hideDotDot && isDotDot))
		return false;

	return nameFilterDisables || passNameFilters(node);
}

/*
   \internal

   Returns \c true if node passes the name filters and should be visible.
 */
bool QSshFileSystemModelPrivate::passNameFilters(const QSshFileSystemNode *node) const
{
	if (nameFilters.isEmpty())
		return true;

	// Check the name regularexpression filters
	if (!(node->isDir() && (filters & QDir::AllDirs))) {
		for (const auto &regexp : qAsConst(nameFilters)) {
			if (regexp.exactMatch(node->fileName))
				return true;
		}
		return false;
	}
	return true;
}

#include "moc_qssh_file_system_model.cpp"
