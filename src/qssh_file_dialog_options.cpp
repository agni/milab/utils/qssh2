#include "qssh_file_dialog_options.h"
#include <QUrl>
#include <QCoreApplication>
#include <QRegExp>
#include <QRegularExpressionMatch>

class QSshFileDialogOptionsPrivate
{
	Q_DECLARE_PUBLIC(QSshFileDialogOptions)
	Q_DISABLE_COPY(QSshFileDialogOptionsPrivate)
	QSshFileDialogOptionsPrivate(QSshFileDialogOptions *q_ptr) : q_ptr(q_ptr) {}

	QSshFileDialogOptions *q_ptr;
	QUrl initial_directory_;
	QString initially_selected_name_filter_;
	QList<QUrl> initially_selected_files_;
	QString labels_[QSshFileDialogOptions::DialogLabelCount];
	bool use_default_name_filters_{ true };
	QStringList name_filters_;
	QDir::Filters filters_{ QDir::AllEntries | QDir::NoDotAndDotDot | QDir::AllDirs };
	QString default_suffix_;
	QFileDialog::ViewMode view_mode_{ QFileDialog::Detail };
	QStringList mime_type_filters_;
	QFileDialog::FileMode file_mode_{ QFileDialog::AnyFile };
	QFileDialog::AcceptMode accept_mode_{ QFileDialog::AcceptOpen };
	QFileDialog::Options options_{ nullptr };
};
QSshFileDialogOptions::QSshFileDialogOptions() : d_ptr(new QSshFileDialogOptionsPrivate(this)) {}

QSshFileDialogOptions::~QSshFileDialogOptions() = default;

QUrl QSshFileDialogOptions::initialDirectory() const
{
	return d_func()->initial_directory_;
}

void QSshFileDialogOptions::setInitialDirectory(const QUrl &dir)
{
	d_func()->initial_directory_ = dir;
}

void QSshFileDialogOptions::setLabelText(QSshFileDialogOptions::QSshDialogLabel label, const QString &text)
{
	if (unsigned(label) < unsigned(DialogLabelCount))
		d_func()->labels_[label] = text;
}

QString QSshFileDialogOptions::labelText(QSshDialogLabel label) const
{
	return (unsigned(label) < unsigned(DialogLabelCount)) ? d_func()->labels_[label] : QString();
}

bool QSshFileDialogOptions::isLabelExplicitlySet(QSshDialogLabel label)
{
	return unsigned(label) < unsigned(DialogLabelCount) && !d_func()->labels_[label].isEmpty();
}

QDir::Filters QSshFileDialogOptions::filter() const
{
	return d_func()->filters_;
}

void QSshFileDialogOptions::setFilter(QDir::Filters filters)
{
	d_func()->filters_ = filters;
}

void QSshFileDialogOptions::setNameFilters(const QStringList &filters)
{
	d_func()->use_default_name_filters_ =
	    filters.size() == 1 && filters.first() == QSshFileDialogOptions::defaultNameFilterString();
	d_func()->name_filters_ = filters;
}

QStringList QSshFileDialogOptions::nameFilters() const
{
	return d_func()->use_default_name_filters_ ? QStringList(defaultNameFilterString()) : d_func()->name_filters_;
}

void QSshFileDialogOptions::setDefaultSuffix(const QString &suffix)
{
	Q_D(QSshFileDialogOptions);
	d->default_suffix_ = suffix;
	if (d->default_suffix_.size() > 1 && d->default_suffix_.startsWith(QLatin1Char('.')))
		d->default_suffix_.remove(0, 1); // Silently change ".txt" -> "txt".
}

QString QSshFileDialogOptions::defaultSuffix() const
{
	return d_func()->default_suffix_;
}

void QSshFileDialogOptions::setViewMode(QFileDialog::ViewMode mode)
{
	d_func()->view_mode_ = mode;
}

QFileDialog::ViewMode QSshFileDialogOptions::viewMode() const
{
	return d_func()->view_mode_;
}

void QSshFileDialogOptions::setMimeTypeFilters(const QStringList &filters)
{
	d_func()->mime_type_filters_ = filters;
}

QStringList QSshFileDialogOptions::mimeTypeFilters() const
{
	return d_func()->mime_type_filters_;
}

QString QSshFileDialogOptions::initiallySelectedNameFilter() const
{
	return d_func()->initially_selected_name_filter_;
}

void QSshFileDialogOptions::setInitiallySelectedNameFilter(const QString &filter)
{
	d_func()->initially_selected_name_filter_ = filter;
}

QList<QUrl> QSshFileDialogOptions::initiallySelectedFiles() const
{
	return d_func()->initially_selected_files_;
}

void QSshFileDialogOptions::setInitiallySelectedFiles(const QList<QUrl> &files)
{
	d_func()->initially_selected_files_ = files;
}

void QSshFileDialogOptions::setFileMode(QFileDialog::FileMode mode)
{
	d_func()->file_mode_ = mode;
}

QFileDialog::FileMode QSshFileDialogOptions::fileMode() const
{
	return d_func()->file_mode_;
}

void QSshFileDialogOptions::setAcceptMode(QFileDialog::AcceptMode mode)
{
	d_func()->accept_mode_ = mode;
}

QFileDialog::AcceptMode QSshFileDialogOptions::acceptMode() const
{
	return d_func()->accept_mode_;
}

void QSshFileDialogOptions::setOption(QFileDialog::Options option, bool on)
{
	if (!(d_func()->options_ & option) != !on)
		setOptions(d_func()->options_ ^ option);
}

bool QSshFileDialogOptions::testOption(QFileDialog::Options option) const
{
	return d_func()->options_ & option;
}

void QSshFileDialogOptions::setOptions(QFileDialog::Options options)
{
	if (options != d_func()->options_)
		d_func()->options_ = options;
}

QFileDialog::Options QSshFileDialogOptions::options() const
{
	return d_func()->options_;
}

QString QSshFileDialogOptions::defaultNameFilterString()
{
	return QCoreApplication::translate("QSshFileDialog", "All Files (*)");
}

const char QSshFileDialogOptions::filterRegExp[] = R"(^(.*)\(([a-zA-Z0-9_.,*? +;#\-\[\]@\{\}/!<>\$%&=^~:\|]*)\)$)";

QStringList QSshFileDialogOptions::cleanFilterList(const QString &filter)
{
	QRegularExpression regexp(QString::fromLatin1(filterRegExp));
	Q_ASSERT(regexp.isValid());
	QString f = filter;
	QRegularExpressionMatch match;
	filter.indexOf(regexp, 0, &match);
	if (match.hasMatch())
		f = match.captured(2);
	return f.split(QLatin1Char(' '), QString::SkipEmptyParts);
}
