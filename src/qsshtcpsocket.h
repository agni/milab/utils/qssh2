#pragma once

#include "qssh2_export.h"
#include "qsshchannel.h"

class QSSH2_EXPORT QSshTcpSocket : public QSshChannel
{
	Q_OBJECT
private:
	QSshTcpSocket(QSshClient *);
	friend class QSshClient;
};
