#pragma once

#include "qssh2_export.h"
#include <QFileInfo>
#include <QTime>

class QSSH2_EXPORT QSshFileInfo
{
public:
	enum FileType
	{
		FileTypeRegular,
		FileTypeDirectory,
		FileTypeSpecial,
		FileTypeSymlink,
		FileTypeUnknown,
		FileTypeNotFetched // used when not yet connected over ssh
	};
	QSshFileInfo(const QString &name = QString(), const QString &host = QString())
	    : hostName(host), name(name), type(FileTypeUnknown)
	{}
	QSshFileInfo(const QFileInfo &other) { *this = other; }
	QSshFileInfo &operator=(const QFileInfo &other);

	QString hostName;
	QString name;
	uint uid;
	uint gid;

	FileType type;
	quint64 size;
	QDateTime atime;
	QDateTime mtime;
	QFile::Permissions permissions;

	inline bool isDir() const { return type == FileTypeDirectory; }
	inline bool isFile() const { return type == FileTypeRegular; }
	inline bool isSymLink() const { return type == FileTypeSymlink; }
	bool isExecutable() const { return (type == FileTypeRegular) && (permissions & (QFile::ExeUser | QFile::ExeGroup)); }
	bool isWritable() const
	{
		return exists() && (permissions & (QFile::WriteGroup | QFile::WriteOther | QFile::WriteUser));
	}

	/// Return a directory's name or a file's parent directory
	inline QString absoluteDirPath() const
	{
		if (isDir())
			return name;
		else {
			int index = name.lastIndexOf("/");
			return name.left(index);
		}
	}

	// TODO: if FileTypeNotFetched, the file might exist anyway...
	inline bool exists() const { return (type != FileTypeNotFetched && type != FileTypeUnknown); }

	inline bool operator==(const QSshFileInfo &other) const { return (other.size == size) && other.name == name; }
};

class QSshClient;
class QSshSftpPrivate;
class QSshFile;
class QSshDir;

class QSSH2_EXPORT QSshSftp : public QObject
{
	Q_OBJECT
	friend class QSshClient;
	friend class QSshSftpPrivate;
	friend class QSshClientPrivate;
	friend class QSshFilePrivate;
	friend class QSshDirPrivate;
	friend class QSshDirDeleterPrivate;
	friend class QSshFileThread;

	QSshSftpPrivate *d_ptr;

public:
	enum ErrorCode
	{
		ERROR_NOT_EXISTING = -100,
		ERROR_DIR_IS_FILE  = -101,
		ERROR_FILE_IS_DIR  = -102,
	};
	class exception : public std::runtime_error
	{
		friend class QSshSftp;
		friend class QSshSftpPrivate;
		friend class QSshFilePrivate;
		friend class QSshFileThread;

		exception(void *session);
		exception(int err, const std::string &msg);
		exception();
		int err;

	public:
		int error() const { return err; }
	};
	~QSshSftp();

	bool isConnected() const;

	/// provide file info (blocking)
	QSshFileInfo statFile(const QString &path, int timeout = 1000 /*ms*/);
	/// create file handle
	QSshFile *file(const QString &name);
	/// create dir handle
	QSshDir *dir(const QString &name);

	void mkdir(const QString &path, const QFileDevice::Permissions &permissions);
	void mkdirRecursive(QString path, const QFileDevice::Permissions &permissions);
	QSshFileInfo findLatestExistingFolder(QString path);
	void rmdir(const QString &path);
	void unlink(const QString &path);
	bool rename(const QString &oldPathName, const QString &newPathName);
	bool link(const QString &fileName, const QString &linkName);

signals:
	void connected();
	void created(const QString &path);
	void removed(const QString &base, const QStringList &entries);
	void renamed(const QString &oldName, const QString &newName);

private:
	QSshSftp(QSshClient *);
	Q_DECLARE_PRIVATE(QSshSftp)
	Q_PRIVATE_SLOT(d_func(), void activate())
};

QString sftp_error_msg(int sftp_error);
