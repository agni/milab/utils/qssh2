#pragma once

#include "qssh2_export.h"
#include <QAbstractItemModel>
#include <QFileIconProvider>
#include <QDir>
#include <QSshSftp>
#include <QCompleter>
#include <QComboBox>
#include <QLineEdit>
#include <QFileDialog>

class QSshFileSystemModelPrivate;
class QSSH2_EXPORT QSshFileSystemModel : public QAbstractItemModel
{
	Q_OBJECT

	Q_PROPERTY(bool readOnly READ isReadOnly WRITE setReadOnly)
	Q_PROPERTY(bool nameFilterDisables READ nameFilterDisables WRITE setNameFilterDisables)

public:
	enum Roles
	{
		FileIconRole    = Qt::DecorationRole,
		FilePathRole    = Qt::UserRole + 1,
		FileNameRole    = Qt::UserRole + 2,
		FilePermissions = Qt::UserRole + 3
	};

	explicit QSshFileSystemModel(QSshSftp *sftp, QObject *parent = NULL);
	~QSshFileSystemModel();

	QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	QModelIndex index(const QString &path, int column = 0) const;
	QModelIndex parent(const QModelIndex &child) const Q_DECL_OVERRIDE;
	bool hasChildren(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	bool canFetchMore(const QModelIndex &parent) const Q_DECL_OVERRIDE;
	void fetchMore(const QModelIndex &parent) Q_DECL_OVERRIDE;

	int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

	QVariant myComputer(int role = Qt::DisplayRole) const;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;
	bool exists(const QString &prefix, const QString &folderName) const;

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) Q_DECL_OVERRIDE;

	QStringList mimeTypes() const Q_DECL_OVERRIDE;
	QMimeData *mimeData(const QModelIndexList &indexes) const Q_DECL_OVERRIDE;
	bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column,
	                  const QModelIndex &parent) Q_DECL_OVERRIDE;
	Qt::DropActions supportedDropActions() const Q_DECL_OVERRIDE;

	// QSshFileSystemModel specific API
	QModelIndex setRootPath(const QString &path);
	QString rootPath() const;
	QDir rootDirectory() const;

	void setIconProvider(QFileIconProvider *provider);
	QFileIconProvider *iconProvider() const;

	void setFilter(QDir::Filters filters);
	QDir::Filters filter() const;

	void setReadOnly(bool enable);
	bool isReadOnly() const;

	void setNameFilterDisables(bool enable);
	bool nameFilterDisables() const;

	void setNameFilters(const QStringList &filters);
	QStringList nameFilters() const;

	QString filePath(const QModelIndex &index) const;
	bool isDir(const QModelIndex &index) const;
	qint64 size(const QModelIndex &index) const;
	QString type(const QModelIndex &index) const;
	QDateTime lastModified(const QModelIndex &index) const;

	QModelIndex mkdir(const QModelIndex &parent, const QString &name);
	bool rmDir(const QModelIndex &index);
	bool rmDirRecursive(const QModelIndex &aindex);
	inline QString fileName(const QModelIndex &index) const;
	inline QIcon fileIcon(const QModelIndex &index) const;
	QFile::Permissions permissions(const QModelIndex &index) const;
	QSshFileInfo fileInfo(const QModelIndex &index) const;
	bool remove(const QModelIndex &index);

protected:
	bool event(QEvent *event) Q_DECL_OVERRIDE;

Q_SIGNALS:
	void rootPathChanged(const QString &newPath);
	void fileRenamed(const QString &path, const QString &oldName, const QString &newName);

private:
	Q_DECLARE_PRIVATE(QSshFileSystemModel)
	Q_DISABLE_COPY(QSshFileSystemModel)
	Q_PRIVATE_SLOT(d_func(), void _q_onDirEntries(QList<QSshFileInfo>))
	Q_PRIVATE_SLOT(d_func(), void _q_onDirError(int))
	Q_PRIVATE_SLOT(d_func(), void _q_onDirDone())
	const QScopedPointer<QSshFileSystemModelPrivate> d_ptr;
	friend class QSshFileDialogPrivate;
};

inline QString QSshFileSystemModel::fileName(const QModelIndex &aindex) const
{
	return aindex.data(Qt::DisplayRole).toString();
}
inline QIcon QSshFileSystemModel::fileIcon(const QModelIndex &aindex) const
{
	return qvariant_cast<QIcon>(aindex.data(Qt::DecorationRole));
}
