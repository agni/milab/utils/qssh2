#include "qsshclient.h"
#include "qsshclient_p.h"
#include "qsshchannel.h"

class QSshChannelPrivate : public QObject
{
public:
	QSshChannelPrivate(QSshChannel *, QSshClient *);
	~QSshChannelPrivate();

	QSshChannel *p;
	QSshClient *d_client;
	LIBSSH2_CHANNEL *d_channel;
	LIBSSH2_SESSION *d_session;
	int d_read_stream_id;
	int d_write_stream_id;
	enum STATE
	{
		IDLE, // after creating channel
		OPEN, // open channel, READY afterwards
		READY, // after open, channel can be used
		START, // start the given command, READ_CHANNEL afterwards
		START_SHELL, // start a shell, READ_CHANNEL afterwards
		REQUEST_PTY, // request a pty, READY afterwards
		TCP_CHANNEL  = 10, // open a tcp channel (only from IDLE state!), READ_CHANNEL afterwards
		READ_CHANNEL = 9999,
		CLOSE
	};
	STATE d_state;
	bool activate();

	QList<STATE> d_next_actions;
	QString d_cmd;
	QByteArray d_pty;
	void openSession();
	void requestPty(const QByteArray &pty);
	void start(const QString &cmd);
	void startShell();
	void close();

	void openTcpSocket(const QString &host, qint16 port);
	QString d_host;
	qint16 d_port;
};
