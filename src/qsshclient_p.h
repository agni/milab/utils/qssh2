#pragma once

#include "qsshclient.h"
#include "qsshchannel.h"
#include "qsshsftp.h"

#include <qtcpsocket.h>
#include <functional>

extern "C" {
#include <libssh2.h>
#include <errno.h>
}

class QSshClientPrivate : public QTcpSocket
{
	Q_OBJECT
public:
	QSshClientPrivate();
	~QSshClientPrivate();
	void teardown();
	void d_reset();
	void d_getLastError();
	void handleError(QSshClient::Error err, const std::function<void()> &handler);
	bool addKnownHost(libssh2_knownhost *entry);

	QSshClient *p;
	LIBSSH2_SESSION *d_session;
	LIBSSH2_KNOWNHOSTS *d_knownHosts;

	enum STATE
	{
		DISCONNECTED,
		CONNECT,
		STARTUP_SESSION,
		VERIFY_HOST,
		TRY_NONE_AUTH,
		GET_NEXT_AUTH_METHOD,
		TRY_AUTH_METHOD,
		CONNECTED,
		DISCONNECT
	};
	STATE d_state;
	bool allow_continue;
	QString d_hostName;
	int d_port;
	QSshKey d_hostKey;
	QString d_userName;
	QString d_passphrase;
	QString d_privateKey;
	QString d_publicKey;
	QString d_errorMessage;
	int d_errorCode;
	QList<QSshClient::AuthenticationMethod> d_availableMethods;
	QList<QSshClient::AuthenticationMethod> d_failedMethods;
	QSshClient::AuthenticationMethod d_currentAuthTry;

	QList<QSshChannel *> d_channels;
	QList<QSshSftp *> d_sftp_channels;

public slots:
	void d_readyRead();
	void d_connected();
	void d_disconnected();
	void d_error(QAbstractSocket::SocketError err);
};
