#include "qsshclient.h"
#include "qsshclient_p.h"
#include "qsshsftp.h"

#include <libssh2_sftp.h>

class QSshSftpPrivate
{
public:
	QSshSftpPrivate(QSshSftp *, QSshClient *);
	~QSshSftpPrivate();

	QSshSftp *q_ptr;
	QSshClient *client;
	LIBSSH2_SFTP *sftp;
	LIBSSH2_SESSION *session;
	QList<QSshFile *> files;
	QList<QSshDir *> dirs;

	QString mkdir_path;
	int mkdir_mode;
	QString rm_path;
	int rm_mode;
	QString rmdir_path;
	int rmdir_mode;
	QString rename_source;
	QString rename_destination;
	int rename_mode;
	QString link_linkName;
	QString link_fileName;
	int link_mode;

	enum STATE
	{
		IDLE,
		READY
	};
	STATE state;
	bool activate();
	bool waitForReadOrWrite(int msecs);
};

QSshFileInfo &setAttributes(QSshFileInfo &info, const LIBSSH2_SFTP_ATTRIBUTES &attrs);
