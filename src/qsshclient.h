#pragma once

#include <QObject>
#include <QList>
#include "qssh2_export.h"

class QSSH2_EXPORT QSshKey
{
public:
	enum Type
	{
		UnknownType,
		Rsa,
		Dss,
		Ecdsa256
	};
	QByteArray hash;
	QByteArray key;
	Type type;
};

class QSshProcess;
class QSshTcpSocket;
class QSshSftp;
class QSshClientPrivate;
struct _LIBSSH2_KNOWNHOSTS;

class QSSH2_EXPORT QSshClient : public QObject
{
	Q_OBJECT

public:
	enum AuthenticationMethod
	{
		PasswordAuthentication,
		PublicKeyAuthentication
	};
	enum KnownHostsFormat
	{
		OpenSslFormat
	};
	enum Error
	{
		AuthenticationError,
		HostKeyUnknownError,
		HostKeyInvalidError,
		HostKeyMismatchError,
		ConnectionRefusedError,
		UnexpectedShutdownError,
		HostNotFoundError,
		SocketError,
		UnknownError
	};
	Q_ENUM(Error)

	QSshClient(QObject *parent = 0);
	~QSshClient();

	static _LIBSSH2_KNOWNHOSTS *init_knownhosts();

	void connectToHost(const QString &hostname, const QString &username = QString(), int port = 22);
	void disconnectFromHost();
	bool isConnected() const;

	void setUserName(const QString &username);
	void setConnection(const QString &hostName, const int &port);
	void setPassphrase(const QString &pass);
	void setKeyFiles(const QString &publicKey, const QString &privateKey);

	bool loadKnownHosts(const QString &file, KnownHostsFormat c = OpenSslFormat);
	bool saveKnownHosts(const QString &file, KnownHostsFormat c = OpenSslFormat) const;
	bool addKnownHost(const QString &hostname, const QSshKey &key);

	QSshKey hostKey() const;
	QString hostName() const;
	int port() const;
	QString userName() const;
	QString passPhrase() const;

	QString lastErrorMsg() const;

	QSshProcess *openProcessChannel();
	QSshTcpSocket *openTcpSocket(const QString &hostName, quint16 port);
	QSshSftp *openSftpChannel();

signals:
	void connected();
	void disconnected();
	void error(QSshClient::Error error);
	void authenticationRequired(QList<QSshClient::AuthenticationMethod> availableMethods);

private slots:
	void processPendingEvents();

private:
	QSshClientPrivate *d;
	friend class QSshClientPrivate;
	friend class QSshChannelPrivate;
	friend class QSshSftpPrivate;
	friend class QSshConnectionManagerPrivate;
};
Q_DECLARE_METATYPE(QSshClient::Error)
Q_DECLARE_METATYPE(QSshClient::AuthenticationMethod)

#include <qdebug.h>
inline QDebug operator<<(QDebug d, const QSshClient::AuthenticationMethod m)
{
	switch (m) {
		case QSshClient::PasswordAuthentication:
			d << "QSshClient::PasswordAuthentication";
			break;
		case QSshClient::PublicKeyAuthentication:
			d << "QSshClient::PublicKeyAuthentication";
			break;
	}
	return d;
}
inline QDebug operator<<(QDebug d, const QSshKey::Type m)
{
	switch (m) {
		case QSshKey::Dss:
			d << "QSshKey::Dss";
			break;
		case QSshKey::Rsa:
			d << "QSshKey::Rsa";
			break;
		case QSshKey::Ecdsa256:
			d << "QSshKey::Ecdsa256";
			break;
		case QSshKey::UnknownType:
			d << "QSshKey::UnknownType";
			break;
	}
	return d;
}
inline QDebug operator<<(QDebug d, const QSshClient::Error m)
{
	switch (m) {
		case QSshClient::AuthenticationError:
			d << "QSshClient::AuthenticationError";
			break;
		case QSshClient::HostKeyUnknownError:
			d << "QSshClient::HostKeyUnknownError";
			break;
		case QSshClient::HostKeyInvalidError:
			d << "QSshClient::HostKeyInvalidError";
			break;
		case QSshClient::HostKeyMismatchError:
			d << "QSshClient::HostKeyMismatchError";
			break;
		case QSshClient::ConnectionRefusedError:
			d << "QSshClient::ConnectionRefusedError";
			break;
		case QSshClient::UnexpectedShutdownError:
			d << "QSshClient::UnexpectedShutdownError";
			break;
		case QSshClient::HostNotFoundError:
			d << "QSshClient::HostNotFoundError";
			break;
		case QSshClient::SocketError:
			d << "QSshClient::SocketError";
			break;
		case QSshClient::UnknownError:
		default:
			d << "QSshClient::UnknownError";
			break;
	}
	return d;
}
