#include "qsshsftp.h"
#include "qsshsftp_p.h"
#include "qssh_file_p.h"
#include "moc_qsshsftp.cpp"

#include <libssh2_sftp.h>
#include <qelapsedtimer.h>
#include <qtimer.h>
#include <QDebug>

const char *getLastError(LIBSSH2_SESSION *session)
{
	char *msg;
	libssh2_session_last_error(session, &msg, nullptr, 0);
	return msg;
}
QSshSftp::exception::exception(void *session)
    : std::runtime_error(getLastError(static_cast<LIBSSH2_SESSION *>(session)))
{
	err = libssh2_session_last_errno(static_cast<LIBSSH2_SESSION *>(session));
}

QSshSftp::exception::exception(int err, const std::string &msg) : std::runtime_error(msg), err(err) {}

QSshSftp::exception::exception() : std::runtime_error("connection not yet established")
{
	err = -ESRCH;
}

QSshSftp::QSshSftp(QSshClient *parent) : QObject(parent), d_ptr(new QSshSftpPrivate(this, parent)) {}

QSshSftp::~QSshSftp()
{
	delete d_ptr;
}

QSshSftpPrivate::QSshSftpPrivate(QSshSftp *p, QSshClient *c)
    : q_ptr(p), client(c), sftp(nullptr), session(client->d->d_session), state(IDLE)
{
	activate();
}

QSshSftpPrivate::~QSshSftpPrivate()
{
	if (sftp) {
		// trigger closing of files
		for (QList<QSshFile *>::const_iterator it = files.constBegin(), e = files.constEnd(); it != e; ++it) {
			(*it)->close();
		}
		// delete files (blocking)
		while (!files.empty())
			delete files.front();
		// delete dirs (blocking)
		while (!dirs.empty())
			delete dirs.front();
		libssh2_sftp_shutdown(sftp);
	}
	client->d->d_sftp_channels.removeOne(q_ptr);
}

bool QSshSftpPrivate::activate()
{
	if (state == IDLE) { // init sftp session
		sftp = libssh2_sftp_init(session);
		if (sftp == nullptr) {
			return libssh2_session_last_error(session, nullptr, nullptr, 0) == LIBSSH2_ERROR_EAGAIN;
		}
#ifdef QSSH2_DEBUG
		qDebug("sftp session opened");
#endif

		state = READY;
		Q_EMIT q_ptr->connected();
	} else if (state == READY) {
		bool success = true;
		// we might modify the list (remove current or append): reversely iterate over current size
		for (int i = files.size() - 1; success && i >= 0; --i) {
			if (!files.at(i)->d_ptr->activate())
				success = false;
		}
		// we might modify the list (remove current or append): reversely iterate over current size
		for (int i = dirs.size() - 1; success && i >= 0; --i) {
			if (!dirs.at(i)->d_ptr->activate())
				success = false;
		}
		if (!success) {
			// return on first error, but retrigger activate
			QTimer::singleShot(0, q_ptr, SLOT(activate()));
			return false;
		}

		int rc = 0;
		if (!mkdir_path.isEmpty()) {
			rc = libssh2_sftp_mkdir(sftp, qPrintable(mkdir_path), mkdir_mode);
			if (rc == 0 || rc != LIBSSH2_ERROR_EAGAIN) { // done or error
				mkdir_path.clear();
				mkdir_mode = rc;
			}
		}
		if (!rm_path.isEmpty()) {
			rc = libssh2_sftp_unlink(sftp, qPrintable(rm_path));
			if (rc == 0 || rc != LIBSSH2_ERROR_EAGAIN) { // done or error
				rm_path.clear();
				rm_mode = rc;
			}
		}
		if (!rmdir_path.isEmpty()) {
			rc = libssh2_sftp_rmdir(sftp, qPrintable(rmdir_path));
			if (rc == 0 || rc != LIBSSH2_ERROR_EAGAIN) { // done or error
				rmdir_path.clear();
				rmdir_mode = rc;
			}
		}
		if (!rename_source.isEmpty() && !rename_destination.isEmpty()) {
			rc = libssh2_sftp_rename(sftp, qPrintable(rename_source), qPrintable(rename_destination));
			if (rc == 0 || rc != LIBSSH2_ERROR_EAGAIN) { // done or error
				rename_source.clear();
				rename_destination.clear();
				rename_mode = rc;
			}
		}
		if (!link_fileName.isEmpty() && !link_linkName.isEmpty()) {
			rc = libssh2_sftp_symlink(sftp, qPrintable(link_fileName),
			                          link_linkName.toLocal8Bit().data()); // target is not allowed to be const (qPrintable
			                                                               // returns const)
			if (rc == 0 || rc != LIBSSH2_ERROR_EAGAIN) { // done or error
				link_fileName.clear();
				link_linkName.clear();
				link_mode = rc;
			}
		}
	}
	return true;
}

QSshFileInfo QSshSftp::findLatestExistingFolder(QString path)
{
	while (true) {
		auto info = statFile(path);
		if (info.type == QSshFileInfo::FileTypeDirectory)
			return info;
		if (path.isEmpty() || (path.size() == 2 && QString(path.at(1)) == ':'))
			break; // root not existing?
		// shorten path
		path.remove(path.lastIndexOf('/'), INT_MAX);
	}
	// on failure return empty FileInfo
	return QSshFileInfo();
}

using Mapping = QMap<QFileDevice::Permissions, unsigned long>;
static Mapping initDirFlagMapping()
{
	Mapping map;

	map.insert(QFileDevice::ReadUser, LIBSSH2_SFTP_S_IRUSR);
	map.insert(QFileDevice::WriteUser, LIBSSH2_SFTP_S_IWUSR);
	map.insert(QFileDevice::ExeUser, LIBSSH2_SFTP_S_IXUSR);

	map.insert(QFileDevice::ReadGroup, LIBSSH2_SFTP_S_IRGRP);
	map.insert(QFileDevice::WriteGroup, LIBSSH2_SFTP_S_IWGRP);
	map.insert(QFileDevice::ExeGroup, LIBSSH2_SFTP_S_IXGRP);

	map.insert(QFileDevice::ReadOther, LIBSSH2_SFTP_S_IROTH);
	map.insert(QFileDevice::WriteOther, LIBSSH2_SFTP_S_IWOTH);
	map.insert(QFileDevice::ExeOther, LIBSSH2_SFTP_S_IXOTH);
	return map;
}

static unsigned long convert_flags(const QFileDevice::Permissions &qt_flags)
{
	static const Mapping map = initDirFlagMapping();
	QFileDevice::Permissions handled;
	unsigned long result = 0;

	for (Mapping::const_iterator it = map.constBegin(), end = map.constEnd(); it != end; ++it) {
		if (qt_flags & it.key()) {
			handled |= it.key();
			result |= it.value();
		}
	}
	if (qt_flags != handled)
		throw std::invalid_argument(qPrintable(QString("invalid dir mode: %1").arg(qt_flags)));

	return result;
}

bool QSshSftp::isConnected() const
{
	return d_ptr->state >= QSshSftpPrivate::READY;
}

QSshFileInfo &setAttributes(QSshFileInfo &info, const LIBSSH2_SFTP_ATTRIBUTES &attrs)
{
	info.size     = attrs.flags & LIBSSH2_SFTP_ATTR_SIZE ? attrs.filesize : 0;
	info.uid      = attrs.uid;
	info.gid      = attrs.gid;

	switch (attrs.permissions & LIBSSH2_SFTP_S_IFMT) {
		case LIBSSH2_SFTP_S_IFSOCK:
		case LIBSSH2_SFTP_S_IFBLK:
		case LIBSSH2_SFTP_S_IFCHR:
		case LIBSSH2_SFTP_S_IFIFO:
			info.type = QSshFileInfo::FileTypeSpecial;
			break;
		case LIBSSH2_SFTP_S_IFLNK:
			info.type = QSshFileInfo::FileTypeSymlink;
			break;
		case LIBSSH2_SFTP_S_IFREG:
			info.type = QSshFileInfo::FileTypeRegular;
			break;
		case LIBSSH2_SFTP_S_IFDIR:
			info.type = QSshFileInfo::FileTypeDirectory;
			break;
		default:
			info.type = QSshFileInfo::FileTypeUnknown;
			break;
	}

	info.permissions = nullptr;
	if (attrs.permissions & LIBSSH2_SFTP_S_IXOTH)
		info.permissions |= QFile::ExeOther;
	if (attrs.permissions & LIBSSH2_SFTP_S_IWOTH)
		info.permissions |= QFile::WriteOther;
	if (attrs.permissions & LIBSSH2_SFTP_S_IROTH)
		info.permissions |= QFile::ReadOther;
	if (attrs.permissions & LIBSSH2_SFTP_S_IXGRP)
		info.permissions |= QFile::ExeGroup;
	if (attrs.permissions & LIBSSH2_SFTP_S_IWGRP)
		info.permissions |= QFile::WriteGroup;
	if (attrs.permissions & LIBSSH2_SFTP_S_IRGRP)
		info.permissions |= QFile::ReadGroup;
	if (attrs.permissions & LIBSSH2_SFTP_S_IXUSR)
		info.permissions |= QFile::ExeUser | QFile::ExeOwner;
	if (attrs.permissions & LIBSSH2_SFTP_S_IWUSR)
		info.permissions |= QFile::WriteUser | QFile::WriteOwner;
	if (attrs.permissions & LIBSSH2_SFTP_S_IRUSR)
		info.permissions |= QFile::ReadUser | QFile::ReadOwner;

	if (attrs.flags & LIBSSH2_SFTP_ATTR_ACMODTIME) {
		info.atime = QDateTime::fromMSecsSinceEpoch(1e3 * attrs.atime);
		info.mtime = QDateTime::fromMSecsSinceEpoch(1e3 * attrs.mtime);
	}
	return info;
}

QSshFileInfo QSshSftp::statFile(const QString &path, int timeout)
{
	QSshFileInfo result(path, d_ptr->client->hostName());

	if (path.isEmpty())
		return result;
	if (d_ptr->state < QSshSftpPrivate::READY)
		throw exception();

	QElapsedTimer stopWatch;
	stopWatch.start();

	LIBSSH2_SFTP_ATTRIBUTES attrs;
	QByteArray name = path.toUtf8();
	int rc;
	while ((rc = libssh2_sftp_stat_ex(d_ptr->sftp, name.constData(), name.size(), LIBSSH2_SFTP_LSTAT, &attrs)) ==
	       LIBSSH2_ERROR_EAGAIN) {
		int msecs_left = timeout - stopWatch.elapsed();
		if (msecs_left < 0)
			break;
		d_ptr->waitForReadOrWrite(msecs_left);
	}
	// ensure that any other events are processed
	QTimer::singleShot(0, this->d_ptr->client, SLOT(processPendingEvents()));

	if (rc) {
		if (rc == LIBSSH2_ERROR_SFTP_PROTOCOL)
			return result; // file not found
		else
			throw exception(d_ptr->session);
	}

	setAttributes(result, attrs);
	return result;
}

QSshFile *QSshSftp::file(const QString &name)
{
	if (d_ptr->state < QSshSftpPrivate::READY)
		throw exception();

	QSshFile *file = new QSshFile(name, this);
	d_ptr->files.append(file);
	return file;
}

QSshDir *QSshSftp::dir(const QString &name)
{
	if (d_ptr->state < QSshSftpPrivate::READY)
		throw exception();

	QSshDir *dir = new QSshDir(name, this);
	d_ptr->dirs.append(dir);
	return dir;
}

void QSshSftp::mkdir(const QString &path, const QFileDevice::Permissions &permissions)
{
	// wait for previous mkdir task to be finished, i.e. mkdir_path becoming empty
	while (!d_ptr->mkdir_path.isEmpty())
		d_ptr->waitForReadOrWrite(10);

	// nothing to do if dir already exists
	QSshFileInfo info = statFile(path);
	if (info.type == QSshFileInfo::FileTypeDirectory)
		return;
	if (info.exists())
		throw exception(ERROR_DIR_IS_FILE, path.toStdString() + " exists as file");

	// define new mkdir task
	d_ptr->mkdir_path = path;
	d_ptr->mkdir_mode = convert_flags(permissions);
	d_ptr->activate();

	// wait for finishing
	while (!d_ptr->mkdir_path.isEmpty())
		d_ptr->waitForReadOrWrite(10);

	if (d_ptr->mkdir_mode == 0) {
		Q_EMIT created(path);
	} else if (d_ptr->mkdir_mode < 0)
		throw exception(d_ptr->mkdir_mode, std::string("Failed to create ") + path.toStdString());
}

void QSshSftp::mkdirRecursive(QString path, const QFileDevice::Permissions &permissions)
{
	Q_D(QSshSftp);
	QSshFileInfo info    = findLatestExistingFolder(path);
	QStringList toCreate = path.remove(0, info.name.length() + 1).split("/");
	path                 = info.name;
	for (const auto &d : qAsConst(toCreate)) {
		if (d.isEmpty())
			continue;
		path.append('/');
		path.append(d);
		mkdir(path, permissions);
	}
}

void QSshSftp::rmdir(const QString &path)
{
	// wait for previous rmdir task to be finished, i.e. rmdir_path becoming empty
	while (!d_ptr->rmdir_path.isEmpty())
		d_ptr->waitForReadOrWrite(10);

	auto info = statFile(path);
	if (!info.exists())
		return;
	if (info.type != QSshFileInfo::FileTypeDirectory)
		throw exception(ERROR_DIR_IS_FILE, path.toStdString() + " is a file");

	d_ptr->rmdir_path = path;
	d_ptr->activate();

	// wait for finishing
	while (!d_ptr->rmdir_path.isEmpty())
		d_ptr->waitForReadOrWrite(10);

	if (d_ptr->rmdir_mode == 0) {
		Q_EMIT removed(path, QStringList());
	} else if (d_ptr->rmdir_mode < 0)
		throw exception(d_ptr->session);
}

void QSshSftp::unlink(const QString &path)
{
	// wait for previous rm task to be finished, i.e. rm_path becoming empty
	while (!d_ptr->rm_path.isEmpty())
		d_ptr->waitForReadOrWrite(10);

	auto info = statFile(path);
	if (info.type == QSshFileInfo::FileTypeDirectory)
		throw exception(ERROR_FILE_IS_DIR, path.toStdString() + " is a directory");
	if (!info.exists())
		return;

	d_ptr->rm_path = path;
	d_ptr->activate();

	// wait for finishing
	while (!d_ptr->rm_path.isEmpty())
		d_ptr->waitForReadOrWrite(10);

	if (d_ptr->rm_mode == 0) {
		Q_EMIT removed(path, QStringList());
	} else if (d_ptr->rm_mode < 0)
		throw exception(d_ptr->session);
}

bool QSshSftp::rename(const QString &oldPathName, const QString &newPathName)
{
	if (oldPathName == newPathName)
		return false;
	// wait for previous rename task to be finished, i.e. rename becoming empty
	while (!d_ptr->rename_source.isEmpty() && !d_ptr->rename_destination.isEmpty())
		d_ptr->waitForReadOrWrite(10);

	d_ptr->rename_source      = oldPathName;
	d_ptr->rename_destination = newPathName;
	d_ptr->activate();

	// wait for finishing
	while (!d_ptr->rename_source.isEmpty() && !d_ptr->rename_destination.isEmpty())
		d_ptr->waitForReadOrWrite(10);

	if (d_ptr->rename_mode == 0) {
		Q_EMIT renamed(oldPathName, newPathName);
		return true;
	} else if (d_ptr->rename_mode < 0) {
		throw exception(d_ptr->session);
	}
	return false;
}

/*
Creates a link named linkName that points to the file fileName.
 */
bool QSshSftp::link(const QString &fileName, const QString &linkName)
{
	Q_D(QSshSftp);
	d->link_linkName = linkName;
	d->link_fileName = fileName;
	d->activate();

	// wait for finishing
	while (!d->link_linkName.isEmpty() && !d->link_fileName.isEmpty())
		d->waitForReadOrWrite(10);

	if (d->link_mode == 0) {
		// Q_EMIT linked(fileName, linkName);
		return true;
	} else if (d_ptr->link_mode < 0) {
		throw exception(d->session);
	}
	return false;
}

// wait for any activity on the ssh socket connection
// return true, if there was any activity
// no other Qt events are processed!
bool QSshSftpPrivate::waitForReadOrWrite(int msecs)
{
	bool read  = client->d->waitForReadyRead(msecs);
	bool write = client->d->waitForBytesWritten(msecs);
	if (!read)
		activate();
	return read || write;
}

QSshFileInfo &QSshFileInfo::operator=(const QFileInfo &other)
{
	name     = other.absoluteFilePath();
	uid      = other.ownerId();
	gid      = other.groupId();
	hostName = "localhost";

	if (other.isDir())
		type = FileTypeDirectory;
	else if (other.isSymLink())
		type = FileTypeSymlink;
	else if (other.isFile())
		type = FileTypeRegular;
	else
		type = FileTypeUnknown;

	size        = other.size();
	atime       = other.lastRead();
	mtime       = other.lastModified();
	permissions = other.permissions();

	return *this;
}

/* Error codes  according to:
 * https://tools.ietf.org/html/draft-ietf-secsh-filexfer-13
 * http://libssh2.sourceforge.net/doc/#libssh2sftplasterror
 * */
QString sftp_error_msg(int sftp_error)
{
	switch (sftp_error) {
		case LIBSSH2_FX_OK:
			return QString::number(LIBSSH2_FX_OK) + " SSH_FX_OK. Indicates successful completion of the operation.";
		case LIBSSH2_FX_EOF:
			return QString::number(LIBSSH2_FX_EOF) + " SSH_FX_EOF. An attempt to read past the end-of-file was made; or, "
			                                         "there are no more directory entries to return.";
		case LIBSSH2_FX_NO_SUCH_FILE:
			return QString::number(LIBSSH2_FX_NO_SUCH_FILE) +
			       " SSH_FX_NO_SUCH_FILE. A reference was made to a file which does not exist.";
		case LIBSSH2_FX_PERMISSION_DENIED:
			return QString::number(LIBSSH2_FX_PERMISSION_DENIED) +
			       " SSH_FX_PERMISSION_DENIED. The user does not have sufficient permissions to perform the operation.";
		case LIBSSH2_FX_FAILURE:
			return QString::number(LIBSSH2_FX_FAILURE) +
			       " SSH_FX_FAILURE. An error occurred, but no specific error code exists to describe the failure.";
		case LIBSSH2_FX_BAD_MESSAGE:
			return QString::number(LIBSSH2_FX_BAD_MESSAGE) +
			       " SSH_FX_BAD_MESSAGE. A badly formatted packet or other SFTP protocol incompatibility was detected.";
		case LIBSSH2_FX_NO_CONNECTION:
			return QString::number(LIBSSH2_FX_NO_CONNECTION) +
			       " SSH_FX_NO_CONNECTION. There is no connection to the server.  This error MAY be used locally, but "
			       "MUST NOT be return by a server.";
		case LIBSSH2_FX_CONNECTION_LOST:
			return QString::number(LIBSSH2_FX_CONNECTION_LOST) +
			       " SSH_FX_CONNECTION_LOST. The connection to the server was lost.  This error MAY be used locally, but "
			       "MUST NOT be return by a server.";
		case LIBSSH2_FX_OP_UNSUPPORTED:
			return QString::number(LIBSSH2_FX_OP_UNSUPPORTED) +
			       " SSH_FX_OP_UNSUPPORTED. An attempted operation could not be completed by the server because the "
			       "server does not support the operation. \n This error MAY be generated locally by the client if e.g. "
			       "the version number exchange indicates that a required feature is not supported by the server, or it "
			       "may be returned by the server if the server does not implement an operation.";
		case LIBSSH2_FX_INVALID_HANDLE:
			return QString::number(LIBSSH2_FX_INVALID_HANDLE) + " SSH_FX_INVALID_HANDLE. The handle value was invalid.";
		case LIBSSH2_FX_NO_SUCH_PATH:
			return QString::number(LIBSSH2_FX_NO_SUCH_PATH) +
			       " SSH_FX_NO_SUCH_PATH. The file path does not exist or is invalid.";
		case LIBSSH2_FX_FILE_ALREADY_EXISTS:
			return QString::number(LIBSSH2_FX_FILE_ALREADY_EXISTS) +
			       "SSH_FX_FILE_ALREADY_EXISTS. The file already exists.";
		case LIBSSH2_FX_WRITE_PROTECT:
			return QString::number(LIBSSH2_FX_WRITE_PROTECT) +
			       " SSH_FX_WRITE_PROTECT. The file is on read-only media, or the media is write protected.";
		case LIBSSH2_FX_NO_MEDIA:
			return QString::number(LIBSSH2_FX_NO_MEDIA) + " SSH_FX_NO_MEDIA. The requested operation cannot be completed "
			                                              "because there is no media available in the drive.";
		case LIBSSH2_FX_NO_SPACE_ON_FILESYSTEM:
			return QString::number(LIBSSH2_FX_NO_SPACE_ON_FILESYSTEM) +
			       " SSH_FX_NO_SPACE_ON_FILESYSTEM. The requested operation cannot be completed because there is "
			       "insufficient free space on the filesystem.";
		case LIBSSH2_FX_QUOTA_EXCEEDED:
			return QString::number(LIBSSH2_FX_QUOTA_EXCEEDED) +
			       " SSH_FX_QUOTA_EXCEEDED. The operation cannot be completed because it would exceed the user's storage "
			       "quota.";
		case LIBSSH2_FX_UNKNOWN_PRINCIPAL:
			return QString::number(LIBSSH2_FX_UNKNOWN_PRINCIPAL) +
			       " SSH_FX_UNKNOWN_PRINCIPAL. A principal referenced by the request (either the 'owner', 'group', or "
			       "'who' field of an ACL), was unknown.  The error specific data contains the problematic names. The "
			       "format is one or more: \n \n string unknown-name \n \n Each string contains the name of a principal "
			       "that was unknown.";
		case LIBSSH2_FX_LOCK_CONFLICT:
			return QString::number(LIBSSH2_FX_LOCK_CONFLICT) +
			       " SSH_FX_LOCK_CONFLICT. The file could not be opened because it is locked by another process.";
		case LIBSSH2_FX_DIR_NOT_EMPTY:
			return QString::number(LIBSSH2_FX_DIR_NOT_EMPTY) + " SSH_FX_DIR_NOT_EMPTY. The directory is not empty.";
		case LIBSSH2_FX_NOT_A_DIRECTORY:
			return QString::number(LIBSSH2_FX_NOT_A_DIRECTORY) +
			       " SSH_FX_NOT_A_DIRECTORY. The specified file is not a directory.";
		case LIBSSH2_FX_INVALID_FILENAME:
			return QString::number(LIBSSH2_FX_INVALID_FILENAME) + " SSH_FX_INVALID_FILENAME. The filename is not valid.";
		case LIBSSH2_FX_LINK_LOOP:
			return QString::number(LIBSSH2_FX_LINK_LOOP) +
			       " SSH_FX_LINK_LOOP. Too many symbolic links encountered or, an SSH_FXF_NOFOLLOW open encountered a "
			       "symbolic link as the final component";
		default:
			return "unknown error";
	}
}
