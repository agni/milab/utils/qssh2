#include "qssh_file_p.h"
#include "qsshsftp_p.h"
#include <qelapsedtimer.h>

QSshDirPrivate::QSshDirPrivate(QSshDir *q_ptr, const QString &name, QSshSftp *sftp)
    : q_ptr(q_ptr), name(name), sftp(sftp), sftp_handle(nullptr), state(OPEN), next(IDLE)
{
	activate();
}

QSshDirPrivate::~QSshDirPrivate()
{
	// remove me from sftp's list
	sftp->d_ptr->dirs.removeOne(q_ptr);
}

bool QSshDirPrivate::activate()
{
	int rc = 0; // We need to initialize otherwise it causes erros on windows (sometimes)
	char data[1024];
	switch (state) {
		case OPENED:
			if (next != IDLE) {
				state = next;
				next  = IDLE;
				return activate();
			}
			break;
		case IDLE:
			return true;
		case OPEN:
			sftp_handle = libssh2_sftp_opendir(sftp->d_ptr->sftp, qUtf8Printable(name));
			if (!sftp_handle) {
				rc = libssh2_session_last_errno(sftp->d_ptr->session);
				if (rc < 0 && rc != LIBSSH2_ERROR_EAGAIN)
					state = IDLE;
			} else {
				state = OPENED;
				return activate();
			}
			break;
		case READ: {
			QList<QSshFileInfo> result;
			LIBSSH2_SFTP_ATTRIBUTES attrs;
			while ((rc = libssh2_sftp_readdir(sftp_handle, data, sizeof(data), &attrs)) > 0) {
				QSshFileInfo info(QString::fromUtf8(data), sftp->d_ptr->client->hostName());
				setAttributes(info, attrs);
				result << info;
			}
			if (!result.empty())
				Q_EMIT q_ptr->entries(result);

			if (rc == 0) { // finished reading
				state = CLOSE;
				return activate();
			} else if (rc == LIBSSH2_ERROR_BUFFER_TOO_SMALL)
				return true;
			break;
		}
		case CLOSE:
			if ((rc = libssh2_sftp_closedir(sftp_handle)) == 0) {
				sftp_handle = nullptr;
				state       = IDLE;
				Q_EMIT q_ptr->closed();
			}
	}
	if (rc < 0 && rc != LIBSSH2_ERROR_EAGAIN) {
		if (rc == LIBSSH2_ERROR_SFTP_PROTOCOL)
			Q_EMIT q_ptr->error(last_error = libssh2_sftp_last_error(sftp->d_ptr->sftp));
		return false;
	}
	return true;
}

inline int QSshDirPrivate::lastError() const
{
	return last_error;
}

inline bool QSshDirPrivate::waitForReadOrWrite(int msecs)
{
	return sftp->d_ptr->waitForReadOrWrite(msecs);
}

QSshDir::QSshDir(const QString &name, QSshSftp *sftp) : d_ptr(new QSshDirPrivate(this, name, sftp)) {}

QString QSshDir::name() const
{
	return d_ptr->name;
}

QSshDir::~QSshDir()
{
	if (d_ptr->state > QSshDirPrivate::IDLE) {
		d_ptr->state = QSshDirPrivate::CLOSE;
		d_ptr->activate();
	}

	while (d_ptr->state != QSshDirPrivate::IDLE)
		d_ptr->waitForReadOrWrite(10);

	delete d_ptr;
}

void QSshDir::list()
{
	d_ptr->next = QSshDirPrivate::READ;
	d_ptr->activate();
}

int QSshDir::lastError() const
{
	return d_ptr->lastError();
}
