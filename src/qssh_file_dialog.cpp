#include "qssh_file_dialog.h"
#include "ui_qssh_file_dialog.h"

#include <QUrl>
#include <QDir>
#include <QPushButton>
#include <QPointer>
#include <QDialogButtonBox>
#if defined(Q_OS_WIN)
#include <windows.h>
#else
#include <unistd.h>
#include <pwd.h>
#endif
#include "qssh_file_dialog_options.h"
#include "qdebug.h"
#include <QMimeDatabase>
#include <QMenu>
#include <QShortcut>
#include <QMessageBox>
#include <QStylePainter>
#include <QMimeData>

Q_GLOBAL_STATIC(QUrl, lastVisitedDir)

class QSshFileDialogPrivate
{
	Q_DECLARE_PUBLIC(QSshFileDialog)
	Q_DISABLE_COPY(QSshFileDialogPrivate)

public:
	QSshFileDialogPrivate(QSshFileDialog *q_ptr, QSshConnectionManager *manager, const QUrl &url);
	~QSshFileDialogPrivate();

	QSshFileDialog *q_ptr;
	QSharedPointer<QSshClient> client;
	QSshSftp *sftp;
	QUrl initialDirectory;
	QString _initialNameFilter;
	QUrl host;
	QSshFileSystemModel *fsModel;
	Ui_QSshFileDialog *ui;
	QScopedPointer<QSshFileDialogOptions> options;

	void createToolButtons();
	void createMenuActions();
	void createWidgets();

	void init(const QUrl &directory = QUrl(), const QString &nameFilter = QString(), const QString &caption = QString());
	QString getEnvironmentVariable(const QString &string);
	static QUrl workingDirectory(const QUrl &path);
	static QString initialSelection(const QUrl &url);
	QStringList typedFiles() const;
	QList<QUrl> userSelectedFiles() const;
	QStringList addDefaultSuffixToFiles(const QStringList &filesToFix) const;
	void setLabelTextControl(QSshFileDialogOptions::QSshDialogLabel label, const QString &text);
	inline void updateLookInLabel();
	inline void updateFileNameLabel();
	inline void updateFileTypeLabel();
	void updateOkButtonText(bool saveAsOnFolder = false);
	void updateCancelButtonText();

	inline QModelIndex rootIndex() const;
	inline QModelIndex select(const QModelIndex &index) const;

	static int maxNameLength(const QString &path);

	QDir::Filters filterForMode(QDir::Filters filters) const
	{
		const QFileDialog::FileMode fileMode = q_ptr->fileMode();
		if (fileMode == QFileDialog::DirectoryOnly) {
			filters |= QDir::Drives | QDir::AllDirs | QDir::Dirs;
			filters &= ~QDir::Files;
		} else {
			filters |= QDir::Drives | QDir::AllDirs | QDir::Files | QDir::Dirs;
		}
		return filters;
	}

	QAbstractItemView *currentView() const;

	static inline QString toInternal(const QString &path)
	{
#if defined(Q_OS_WIN)
		QString n(path);
		n.replace(QLatin1Char('\\'), QLatin1Char('/'));
#if defined(Q_OS_WINCE)
		if ((n.size() > 1) && (n.startsWith(QLatin1String("//"))))
			n = n.mid(1);
#endif
		return n;
#else // the compile should optimize away this
		return path;
#endif
	}

	static void setLastVisitedDirectory(const QUrl &dir);
	void retranslateWindowTitle();
	void retranslateStrings();
	void emitFilesSelected(const QStringList &files);

	void _q_goHome();
	void _q_pathChanged(const QString &newPath);
	void _q_navigateBackward();
	void _q_navigateForward();
	void _q_navigateToParent();
	void _q_createDirectory();
	void _q_showListView();
	void _q_showDetailsView();
	void _q_showContextMenu(const QPoint &position);
	void _q_renameCurrent();
	void _q_deleteCurrent();
	void _q_showHidden();
	void _q_showHeader(QAction * /*action*/);
	void _q_updateOkButton();
	void _q_currentChanged(const QModelIndex &index);
	void _q_enterDirectory(const QModelIndex &index);
	void _q_emitUrlSelected(const QUrl &file);
	void _q_emitUrlsSelected(const QList<QUrl> &files);
	void _q_nativeCurrentChanged(const QUrl &file);
	void _q_nativeEnterDirectory(const QUrl &directory);
	void _q_goToDirectory(const QString &path);
	void _q_useNameFilter(int index);
	void _q_selectionChanged();
	void _q_autoCompleteFileName(const QString &text);
	void _q_rowsInserted(const QModelIndex &parent);
	void _q_fileRenamed(const QString &path, const QString &oldName, const QString &newName);

	void _q_clientConnected();
	void _q_sftpConnected();

	QSshFSCompleter *completer;

	QString setWindowTitle;

	QStringList currentHistory;
	int currentHistoryLocation;

	QAction *renameAction;
	QAction *deleteAction;
	QAction *showHiddenAction;
	QAction *newFolderAction;

	bool useDefaultCaption;
	bool defaultFileTypes;

	inline bool usingWidgets() const;

	QPointer<QObject> receiverToDisconnectOnClose;
	QByteArray memberToDisconnectOnClose;
	QByteArray signalToDisconnectOnClose;
};

QSshFileDialogPrivate::QSshFileDialogPrivate(QSshFileDialog *q_ptr, QSshConnectionManager *manager, const QUrl &url)
    : q_ptr(q_ptr)
    , client(manager->open(url, true))
    , sftp(nullptr)
    , initialDirectory(url.fileName())
    , host(url)
    , fsModel(nullptr)
    , ui(nullptr)
    , options(new QSshFileDialogOptions)
    , completer(nullptr)
    , currentHistoryLocation(-1)
    , renameAction(nullptr)
    , deleteAction(nullptr)
    , showHiddenAction(nullptr)
    , newFolderAction(nullptr)
    , useDefaultCaption(true)
    , defaultFileTypes(true)
{
	Q_Q(QSshFileDialog);
	if (client->isConnected())
		_q_clientConnected();
	else
		q->connect(client.data(), &QSshClient::connected, q, [&]() { _q_clientConnected(); });
}

void QSshFileDialogPrivate::createToolButtons()
{
	Q_Q(QSshFileDialog);
	ui->backButton->setIcon(q->style()->standardIcon(QStyle::SP_ArrowBack, nullptr, q));
	ui->backButton->setAutoRaise(true);
	ui->backButton->setEnabled(false);
	q->connect(ui->backButton, &QToolButton::clicked, q, [&]() { _q_navigateBackward(); });

	ui->forwardButton->setIcon(q->style()->standardIcon(QStyle::SP_ArrowForward, nullptr, q));
	ui->forwardButton->setAutoRaise(true);
	ui->forwardButton->setEnabled(false);
	q->connect(ui->forwardButton, &QToolButton::clicked, q, [&]() { _q_navigateForward(); });

	ui->toParentButton->setIcon(q->style()->standardIcon(QStyle::SP_FileDialogToParent, nullptr, q));
	ui->toParentButton->setAutoRaise(true);
	ui->toParentButton->setEnabled(false);
	q->connect(ui->toParentButton, &QToolButton::clicked, q, [&]() { _q_navigateToParent(); });

	ui->listModeButton->setIcon(q->style()->standardIcon(QStyle::SP_FileDialogListView, nullptr, q));
	ui->listModeButton->setAutoRaise(true);
	ui->listModeButton->setDown(true);
	q->connect(ui->listModeButton, &QToolButton::clicked, q, [&]() { _q_showListView(); });

	ui->detailModeButton->setIcon(q->style()->standardIcon(QStyle::SP_FileDialogDetailedView, nullptr, q));
	ui->detailModeButton->setAutoRaise(true);
	q->connect(ui->detailModeButton, &QToolButton::clicked, q, [&]() { _q_showDetailsView(); });

	QSize toolSize(ui->fileNameEdit->sizeHint().height(), ui->fileNameEdit->sizeHint().height());
	ui->backButton->setFixedSize(toolSize);
	ui->listModeButton->setFixedSize(toolSize);
	ui->detailModeButton->setFixedSize(toolSize);
	ui->forwardButton->setFixedSize(toolSize);
	ui->toParentButton->setFixedSize(toolSize);

	ui->newFolderButton->setIcon(q->style()->standardIcon(QStyle::SP_FileDialogNewFolder, nullptr, q));
	ui->newFolderButton->setFixedSize(toolSize);
	ui->newFolderButton->setAutoRaise(true);
	ui->newFolderButton->setEnabled(false);
	q->connect(ui->newFolderButton, &QToolButton::clicked, q, [&]() { _q_createDirectory(); });
}

void QSshFileDialogPrivate::createMenuActions()
{
	Q_Q(QSshFileDialog);

	QAction *goHomeAction = new QAction(q);
	goHomeAction->setShortcut(Qt::CTRL + Qt::Key_H + Qt::SHIFT);
	q->connect(goHomeAction, &QAction::triggered, q, [&]() { _q_goHome(); });
	q->addAction(goHomeAction);

	// ### TODO add Desktop & Computer actions

	QAction *goToParent = new QAction(q);
	goToParent->setObjectName(QLatin1String("qt_goto_parent_action"));
	goToParent->setShortcut(Qt::CTRL + Qt::UpArrow);
	q->connect(goToParent, &QAction::triggered, q, [&]() { _q_navigateToParent(); });
	q->addAction(goToParent);

	renameAction = new QAction(q);
	renameAction->setEnabled(false);
	renameAction->setObjectName(QLatin1String("qt_rename_action"));
	q->connect(renameAction, &QAction::triggered, q, [&]() { _q_renameCurrent(); });

	deleteAction = new QAction(q);
	deleteAction->setEnabled(false);
	deleteAction->setObjectName(QLatin1String("qt_delete_action"));
	q->connect(deleteAction, &QAction::triggered, q, [&]() { _q_deleteCurrent(); });

	showHiddenAction = new QAction(q);
	showHiddenAction->setObjectName(QLatin1String("qt_show_hidden_action"));
	showHiddenAction->setCheckable(true);
	q->connect(showHiddenAction, &QAction::triggered, q, [&]() { _q_showHidden(); });

	newFolderAction = new QAction(q);
	newFolderAction->setObjectName(QLatin1String("qt_new_folder_action"));
	q->connect(newFolderAction, &QAction::triggered, q, [&]() { _q_createDirectory(); });
}

QSshFileDialogPrivate::~QSshFileDialogPrivate()
{
	delete ui;
}

QUrl QSshFileDialogPrivate::workingDirectory(const QUrl &url)
{
	if (!url.isEmpty()) {
		QUrl directory = url;
		if (!directory.isEmpty())
			return directory;
	}
	QUrl directory = *lastVisitedDir();
	if (!directory.isEmpty())
		return directory;
	return QUrl::fromLocalFile("/");
}

QString QSshFileDialogPrivate::initialSelection(const QUrl &url)
{
	if (url.isEmpty())
		return QString();
	// With remote URLs we can only assume.
	return url.toLocalFile();
}

/*!
   Sets the file dialog's current \a directory.
*/
void QSshFileDialog::setDirectory(const QString &directory)
{
	Q_D(QSshFileDialog);
	QString newDirectory = directory;
	// we remove .. and . from the given path if exist
	if (!directory.isEmpty())
		newDirectory = QDir::cleanPath(directory);

	if (!directory.isEmpty() && newDirectory.isEmpty())
		return;

	QUrl newDirUrl = QUrl::fromLocalFile(newDirectory);
	QSshFileDialogPrivate::setLastVisitedDirectory(newDirUrl);

	d->options->setInitialDirectory(QUrl::fromLocalFile(directory));
	if (!d->usingWidgets()) {
		return;
	}

	// Check for existing connections
	if (!d->client->isConnected()) {
		connect(d->client.data(), &QSshClient::connected, this, [this, directory]() { setDirectory(directory); });
		return;
	} else if (!d->sftp->isConnected()) {
		connect(d->sftp, &QSshSftp::connected, this, [this, directory]() { setDirectory(directory); });
		return;
	}

	if (d->fsModel->rootPath() == newDirectory)
		return;

	QModelIndex root = d->fsModel->setRootPath(newDirectory);
	d->ui->newFolderButton->setEnabled(d->fsModel->flags(root) & Qt::ItemIsDropEnabled);
	if (root != d->ui->listView->rootIndex()) {
		if (directory.endsWith(QLatin1Char('/')))
			d->completer->setCompletionPrefix(newDirectory);
		else
			d->completer->setCompletionPrefix(newDirectory + QLatin1Char('/'));
		d->ui->treeView->setRootIndex(root);
		d->ui->listView->setRootIndex(root);
	}
	d->ui->listView->selectionModel()->clear();
}

/*!
   Returns the directory currently being displayed in the dialog.
*/
QDir QSshFileDialog::directory() const
{
	Q_D(const QSshFileDialog);
	return d->fsModel->rootDirectory().absolutePath();
}

#ifdef Q_OS_UNIX
QString qt_tildeExpansion(const QString &path, bool *expanded = nullptr)
{
	if (expanded != nullptr)
		*expanded = false;
	if (!path.startsWith(QLatin1Char('~')))
		return path;
	QString ret        = path;
	QStringList tokens = ret.split(QDir::separator());
	if (tokens.first() == QLatin1String("~")) {
		ret.replace(0, 1, QDir::homePath());
	} else {
		QString userName = tokens.first();
		userName.remove(0, 1);
#if defined(Q_OS_VXWORKS)
		const QString homePath = QDir::homePath();
#elif defined(_POSIX_THREAD_SAFE_FUNCTIONS) && !defined(Q_OS_OPENBSD)
		passwd pw;
		passwd *tmpPw;
		char buf[200];
		const int bufSize = sizeof(buf);
		int err           = 0;
#if defined(Q_OS_SOLARIS) && (_POSIX_C_SOURCE - 0 < 199506L)
		tmpPw             = getpwnam_r(userName.toLocal8Bit().constData(), &pw, buf, bufSize);
#else
		err = getpwnam_r(userName.toLocal8Bit().constData(), &pw, buf, bufSize, &tmpPw);
#endif
		if (err || !tmpPw)
			return ret;
		const QString homePath = QString::fromLocal8Bit(pw.pw_dir);
#else
		passwd *pw = getpwnam(userName.toLocal8Bit().constData());
		if (!pw)
			return ret;
		const QString homePath = QString::fromLocal8Bit(pw->pw_dir);
#endif
		ret.replace(0, tokens.first().length(), homePath);
	}
	if (expanded != nullptr)
		*expanded = true;
	return ret;
}
#endif

QStringList QSshFileDialogPrivate::typedFiles() const
{
	Q_Q(const QSshFileDialog);
	QStringList files;
	QString editText = ui->fileNameEdit->text();
	if (editText.isEmpty())
		return files;
	if (!editText.contains(QLatin1Char('"'))) {
		files << editText;
	} else {
		// " is used to separate files like so: "file1" "file2" "file3" ...
		// ### need escape character for filenames with quotes (")
		QStringList tokens = editText.split(QLatin1Char('\"'));
		for (int i = 0; i < tokens.size(); ++i) {
			if ((i % 2) == 0)
				continue; // Every even token is a separator
			files << toInternal(tokens.at(i));
		}
	}
	if (files.isEmpty())
		return QStringList();
	return addDefaultSuffixToFiles(files);
}

QList<QUrl> QSshFileDialogPrivate::userSelectedFiles() const
{
	QList<QUrl> files;
	if (!ui->listView->selectionModel())
		return files;
	foreach (const QModelIndex &index, ui->listView->selectionModel()->selectedRows()) {
		QUrl url(QUrl::fromLocalFile(index.data(QSshFileSystemModel::FilePathRole).toString()));
		files.append(url);
	}
	if (files.isEmpty() && !ui->fileNameEdit->text().isEmpty())
		foreach (const QString &path, typedFiles())
			files.append(path);

	return files;
}

void QSshFileDialogPrivate::updateLookInLabel()
{
	if (options->isLabelExplicitlySet(QSshFileDialogOptions::QSshDialogLabel::LookIn))
		setLabelTextControl(QSshFileDialogOptions::LookIn, options->labelText(QSshFileDialogOptions::LookIn));
}

void QSshFileDialogPrivate::updateFileNameLabel()
{
	if (options->isLabelExplicitlySet(QSshFileDialogOptions::FileName)) {
		setLabelTextControl(QSshFileDialogOptions::FileName, options->labelText(QSshFileDialogOptions::FileName));
	} else {
		switch (q_func()->fileMode()) {
			case QFileDialog::DirectoryOnly:
			case QFileDialog::Directory:
				setLabelTextControl(QSshFileDialogOptions::FileName, "Directory:");
				break;
			default:
				setLabelTextControl(QSshFileDialogOptions::FileName, "File &name:");
				break;
		}
	}
}

void QSshFileDialogPrivate::updateFileTypeLabel()
{
	if (options->isLabelExplicitlySet(QSshFileDialogOptions::FileType))
		setLabelTextControl(QSshFileDialogOptions::FileType, options->labelText(QSshFileDialogOptions::FileType));
}

void QSshFileDialogPrivate::_q_useNameFilter(int index)
{
	Q_Q(QSshFileDialog);
	if (!fsModel) {
		q->connect(q, &QSshFileDialog::modelCreated, q, [&, index]() { _q_useNameFilter(index); });
		return;
	}
	Q_ASSERT(fsModel);
	QStringList nameFilters = options->nameFilters();
	if (index == nameFilters.size()) {
		QAbstractItemModel *comboModel = ui->fileTypeCombo->model();
		nameFilters.append(comboModel->index(comboModel->rowCount() - 1, 0).data().toString());
		options->setNameFilters(nameFilters);
	}
	QString nameFilter         = nameFilters.at(index);
	QStringList newNameFilters = QSshFileDialogOptions::cleanFilterList(nameFilter);
	if (q->acceptMode() == QFileDialog::AcceptSave) {
		QString newNameFilterExtension;
		if (newNameFilters.count() > 0) {
			const QString &check = newNameFilters.at(0);
			if (check.isEmpty())
				newNameFilterExtension = QLatin1String("");
			else {
				int lastSeparator      = check.lastIndexOf(".");
				newNameFilterExtension = check.mid(lastSeparator + 1);
			}
		}

		QString fileName  = ui->fileNameEdit->text();
		int lastSeparator = fileName.lastIndexOf(".");
		QString extension;
		fileName.isEmpty() ? extension = QLatin1String("") : extension = fileName.mid(lastSeparator + 1);
		const QString fileNameExtension                                = extension;
		if (!fileNameExtension.isEmpty() && !newNameFilterExtension.isEmpty()) {
			const int fileNameExtensionLength = fileNameExtension.count();
			fileName.replace(fileName.count() - fileNameExtensionLength, fileNameExtensionLength, newNameFilterExtension);
			ui->listView->clearSelection();
			ui->fileNameEdit->setText(fileName);
		}
	}
	fsModel->setNameFilters(newNameFilters);
}

/*!
   \internal

   This is called when the model index corresponding to the current file is changed
   from \a index to \a current.
*/
void QSshFileDialogPrivate::_q_selectionChanged()
{
	const QFileDialog::FileMode fileMode = q_func()->fileMode();
	QModelIndexList indexes              = ui->listView->selectionModel()->selectedRows();
	bool stripDirs = (fileMode != QFileDialog::DirectoryOnly && fileMode != QFileDialog::Directory);

	QStringList allFiles;
	for (int i = 0; i < indexes.count(); ++i) {
		if (stripDirs && fsModel->isDir((indexes.at(i))))
			continue;
		allFiles.append(indexes.at(i).data().toString());
	}
	if (allFiles.count() > 1)
		for (int i = 0; i < allFiles.count(); ++i) {
			allFiles.replace(i, QString(QLatin1Char('"') + allFiles.at(i) + QLatin1Char('"')));
		}

	QString finalFiles = allFiles.join(QLatin1Char(' '));
	if (!finalFiles.isEmpty() && !ui->fileNameEdit->hasFocus() && ui->fileNameEdit->isVisible())
		ui->fileNameEdit->setText(finalFiles);
	else
		_q_updateOkButton();
}

void QSshFileDialogPrivate::_q_autoCompleteFileName(const QString &text)
{
	if (text.startsWith(QLatin1String("//")) || text.startsWith(QLatin1Char('\\'))) {
		ui->listView->selectionModel()->clearSelection();
		return;
	}

	QStringList multipleFiles = typedFiles();
	if (multipleFiles.count() > 0) {
		QModelIndexList oldFiles = ui->listView->selectionModel()->selectedRows();
		QModelIndexList newFiles;
		for (int i = 0; i < multipleFiles.count(); ++i) {
			QModelIndex idx = fsModel->index(multipleFiles.at(i));
			if (oldFiles.contains(idx))
				oldFiles.removeAll(idx);
			else
				newFiles.append(idx);
		}
		for (int i = 0; i < newFiles.count(); ++i)
			select(newFiles.at(i));
		if (ui->fileNameEdit->hasFocus())
			for (int i = 0; i < oldFiles.count(); ++i)
				ui->listView->selectionModel()->select(oldFiles.at(i),
				                                       QItemSelectionModel::Toggle | QItemSelectionModel::Rows);
	}
}

/*!
   \internal

   When parent is root and rows have been inserted when none was there before
   then select the first one.
*/
void QSshFileDialogPrivate::_q_rowsInserted(const QModelIndex &parent)
{
	if (!ui->treeView || parent != ui->treeView->rootIndex() || !ui->treeView->selectionModel() ||
	    ui->treeView->selectionModel()->hasSelection() || ui->treeView->model()->rowCount(parent) == 0)
		return;
}

void QSshFileDialogPrivate::_q_fileRenamed(const QString &path, const QString &oldName, const QString &newName)
{
	const QFileDialog::FileMode fileMode = q_func()->fileMode();
	if (fileMode == QFileDialog::Directory || fileMode == QFileDialog::DirectoryOnly) {
		if (path == fsModel->rootPath() && ui->fileNameEdit->text() == oldName)
			ui->fileNameEdit->setText(newName);
	}
}

void QSshFileDialogPrivate::_q_clientConnected()
{
	Q_Q(QSshFileDialog);
	sftp = client->openSftpChannel();
	if (sftp->isConnected())
		_q_sftpConnected();
	else
		q->connect(sftp, &QSshSftp::connected, q, [&]() { _q_sftpConnected(); });
}

void QSshFileDialogPrivate::_q_sftpConnected()
{
	Q_Q(QSshFileDialog);

	fsModel = new QSshFileSystemModel(sftp, q);
	fsModel->setFilter(options->filter());
	fsModel->setNameFilterDisables(false);
	Q_EMIT q->modelCreated();

	completer = new QSshFSCompleter(fsModel, q);
	ui->fileNameEdit->setCompleter(completer);

	ui->lookInCombo->setFileDialogPrivate(this);
	q->connect(ui->lookInCombo, qOverload<const QString &>(&QSshFileDialogComboBox::activated), q,
	           [this](const QString &path) { _q_goToDirectory(path); });

	q->connect(fsModel, &QSshFileSystemModel::fileRenamed, q,
	           [this](const QString &path, const QString &oldName, const QString &newName) {
		           _q_fileRenamed(path, oldName, newName);
	           });
	q->connect(fsModel, &QSshFileSystemModel::rootPathChanged, q,
	           [this](const QString &newPath) { _q_pathChanged(newPath); });
	q->connect(fsModel, &QSshFileSystemModel::rowsInserted, q,
	           [this](const QModelIndex &parent) { _q_rowsInserted(parent); });
	fsModel->setReadOnly(false);
	ui->treeView->setModel(fsModel);
	ui->listView->setModel(fsModel);

	QScopedPointer<QItemSelectionModel> selModel(ui->treeView->selectionModel()); // ensures deletion
	ui->treeView->setSelectionModel(ui->listView->selectionModel());

	// Selections
	QItemSelectionModel *selections = ui->listView->selectionModel();
	q->connect(selections, &QItemSelectionModel::selectionChanged, q, [&]() { _q_selectionChanged(); });
	q->connect(selections, &QItemSelectionModel::currentChanged, q,
	           [&](QModelIndex index) { _q_currentChanged(index); });

	QAbstractItemModel *abstractModel = fsModel;

	QActionGroup *showActionGroup = new QActionGroup(q);
	showActionGroup->setExclusive(false);

	QObject::connect(showActionGroup, &QActionGroup::triggered, q, [this](QAction *action) { _q_showHeader(action); });

	QHeaderView *treeHeader = ui->treeView->header();
	for (int i = 1; i < abstractModel->columnCount(QModelIndex()); ++i) {
		QAction *showHeader = new QAction(showActionGroup);
		showHeader->setCheckable(true);
		showHeader->setChecked(true);
		treeHeader->addAction(showHeader);
	}

	QFontMetrics fm(q->font());
	treeHeader->resizeSection(0, fm.width(QLatin1String(" some data has really long names so this is big")));
	treeHeader->resizeSection(1, fm.width(QLatin1String(" 123456 ")));
	treeHeader->resizeSection(2, fm.width(QLatin1String(" 123456 ")));
	treeHeader->resizeSection(3, fm.width(QLatin1String("123.12 bytes")));
	treeHeader->resizeSection(4, fm.width(QLatin1String(" rwx rwx rwx ")));
	treeHeader->resizeSection(5, fm.width(QLatin1String(" 07.07.17 12:00 ")));
	treeHeader->setContextMenuPolicy(Qt::ActionsContextMenu);
	if (!_initialNameFilter.isEmpty())
		q->setNameFilter(_initialNameFilter);
	q->selectFile(initialSelection(initialDirectory));

	retranslateStrings();
	q->setFileMode(QFileDialog::AnyFile);
	q->setAcceptMode(QFileDialog::AcceptOpen);
}

bool QSshFileDialogPrivate::usingWidgets() const
{
	return ui;
}

void QSshFileDialogPrivate::_q_updateOkButton()
{
	Q_Q(QSshFileDialog);
	QPushButton *button = ui->buttonBox->button((q->acceptMode() == QFileDialog::AcceptOpen) ? QDialogButtonBox::Open :
	                                                                                           QDialogButtonBox::Save);
	if (!button)
		return;
	const QFileDialog::FileMode fileMode = q->fileMode();

	bool enableButton    = true;
	bool isOpenDirectory = false;

	QStringList files    = q->selectedFiles();
	QString lineEditText = ui->fileNameEdit->text();

	if (lineEditText.startsWith(QLatin1String("//")) || lineEditText.startsWith(QLatin1Char('\\'))) {
		button->setEnabled(true);
		updateOkButtonText();
		return;
	}

	if (files.isEmpty()) {
		enableButton = false;
	} else if (lineEditText == QLatin1String("..")) {
		isOpenDirectory = true;
	} else {
		if (!fsModel)
			return;
		switch (fileMode) {
			case QFileDialog::DirectoryOnly:
			case QFileDialog::Directory: {
				QString fn      = files.first();
				QModelIndex idx = fsModel->index(fn);
				if (!idx.isValid())
					idx = fsModel->index(getEnvironmentVariable(fn));
				if (!idx.isValid() || !fsModel->isDir(idx))
					enableButton = false;
				break;
			}
			case QFileDialog::AnyFile: {
				QString fn = files.first();
				QSshFileInfo info;
				try {
					info = sftp->statFile(fn);
				} catch (QSshSftp::exception &e) {
					qDebug() << "can not statFile to  updateButton" << fn << e.what();
				}
				QModelIndex idx = fsModel->index(fn);
				QString fileDir;
				QString fileName;
				if (info.isDir()) {
					fileDir = info.name;
				} else {
					fileDir  = fn.mid(0, fn.lastIndexOf(QLatin1Char('/')));
					fileName = fn.mid(fileDir.length() + 1);
				}
				if (lineEditText.contains(QLatin1String(".."))) {
					fileDir  = info.name;
					fileName = info.name;
				}

				if (fileDir == q->directory().canonicalPath() && fileName.isEmpty()) {
					enableButton = false;
					break;
				}
				if (idx.isValid() && fsModel->isDir(idx)) {
					isOpenDirectory = true;
					enableButton    = true;
					break;
				}
				if (!idx.isValid()) {
					int maxLength = maxNameLength(fileDir);
					enableButton  = maxLength < 0 || fileName.length() <= maxLength;
				}
				break;
			}
			case QFileDialog::ExistingFile:
			case QFileDialog::ExistingFiles:
				for (int i = 0; i < files.count(); ++i) {
					QModelIndex idx = fsModel->index(files.at(i));
					if (!idx.isValid())
						idx = fsModel->index(getEnvironmentVariable(files.at(i)));
					if (!idx.isValid()) {
						enableButton = false;
						break;
					}
					if (idx.isValid() && fsModel->isDir(idx)) {
						isOpenDirectory = true;
						break;
					}
				}
				break;
			default:
				break;
		}
	}

	button->setEnabled(enableButton);
	updateOkButtonText(isOpenDirectory);
}

void QSshFileDialogPrivate::_q_currentChanged(const QModelIndex &index)
{
	_q_updateOkButton();
	emit q_func()->currentChanged(index.data(QSshFileSystemModel::FilePathRole).toString());
}

void QSshFileDialogPrivate::updateOkButtonText(bool saveAsOnFolder)
{
	Q_Q(QSshFileDialog);
	// 'Save as' at a folder: Temporarily change to "Open".
	if (saveAsOnFolder) {
		setLabelTextControl(QSshFileDialogOptions::Accept, "&Open");
	} else if (options->isLabelExplicitlySet(QSshFileDialogOptions::Accept)) {
		setLabelTextControl(QSshFileDialogOptions::Accept, options->labelText(QSshFileDialogOptions::Accept));
		return;
	} else {
		switch (q->fileMode()) {
			case QFileDialog::DirectoryOnly:
			case QFileDialog::Directory:
				setLabelTextControl(QSshFileDialogOptions::Accept, "&Choose");
				break;
			default:
				setLabelTextControl(QSshFileDialogOptions::Accept,
				                    q->acceptMode() == QFileDialog::AcceptOpen ? "&Open" : "&Save");
				break;
		}
	}
}

void QSshFileDialogPrivate::updateCancelButtonText()
{
	if (options->isLabelExplicitlySet(QSshFileDialogOptions::Reject))
		setLabelTextControl(QSshFileDialogOptions::Reject, options->labelText(QSshFileDialogOptions::Reject));
}

/*
   Returns the file system model index that is the root index in the
   views
*/
QModelIndex QSshFileDialogPrivate::rootIndex() const
{
	return ui->listView->rootIndex();
}

QModelIndex QSshFileDialogPrivate::select(const QModelIndex &index) const
{
	Q_ASSERT(index.isValid() ? index.model() == fsModel : true);

	if (index.isValid() && !ui->listView->selectionModel()->isSelected(index))
		ui->listView->selectionModel()->select(index, QItemSelectionModel::Select | QItemSelectionModel::Rows);
	return index;
}

void QSshFileDialogPrivate::setLabelTextControl(QSshFileDialogOptions::QSshDialogLabel label, const QString &text)
{
	if (!ui)
		return;
	switch (label) {
		case QSshFileDialogOptions::LookIn:
			ui->lookInLabel->setText(text);
			break;
		case QSshFileDialogOptions::FileName:
			ui->fileNameLabel->setText(text);
			break;
		case QSshFileDialogOptions::FileType:
			ui->fileTypeLabel->setText(text);
			break;
		case QSshFileDialogOptions::Accept:
			if (q_func()->acceptMode() == QFileDialog::AcceptOpen) {
				if (QPushButton *button = ui->buttonBox->button(QDialogButtonBox::Open))
					button->setText(text);
			} else {
				if (QPushButton *button = ui->buttonBox->button(QDialogButtonBox::Save))
					button->setText(text);
			}
			break;
		case QSshFileDialogOptions::Reject:
			if (QPushButton *button = ui->buttonBox->button(QDialogButtonBox::Cancel))
				button->setText(text);
			break;
		case QSshFileDialogOptions::DialogLabelCount:
			throw;
	}
}

QStringList QSshFileDialogPrivate::addDefaultSuffixToFiles(const QStringList &filesToFix) const
{
	QStringList files;
	for (int i = 0; i < filesToFix.size(); ++i) {
		QString name = toInternal(filesToFix.at(i));
		QSshFileInfo info;
		try {
			info = sftp->statFile(name); // statFile in case file got deleted
		} catch (QSshSftp::exception &e) {
			Q_UNUSED(e);
			// here we catch the exception but an exception might not be a mistake here
			// if the file does not exist, it simply means we have to prepend our
			// root path to "name"
		}

		// if the filename has no suffix, add the default suffix
		const QString defaultSuffix = options->defaultSuffix();
		if (!defaultSuffix.isEmpty() && !info.isDir() && name.lastIndexOf(QLatin1Char('.')) == -1)
			name += QLatin1Char('.') + defaultSuffix;
		if (info.name.startsWith("/") ||
		    (info.name.size() > 1 && QString(info.name.at(1)) == QString(":"))) // TODO here we should actually check if
		                                                                        // the path is absolute
			files.append(name);
		else {
			// at this point the path should only have Qt path separators.
			// This check is needed since we might be at the root directory
			// and on Windows it already ends with slash.
			QString path = fsModel->rootPath();
			if (!path.endsWith(QLatin1Char('/')))
				path += QLatin1Char('/');
			path += name;
			files.append(path);
		}
	}
	return files;
}

void QSshFileDialogPrivate::setLastVisitedDirectory(const QUrl &dir)
{
	*lastVisitedDir() = dir;
}

void QSshFileDialogPrivate::retranslateWindowTitle()
{
	Q_Q(QSshFileDialog);
	if (!useDefaultCaption || setWindowTitle != q->windowTitle())
		return;
	if (q->acceptMode() == QFileDialog::AcceptOpen) {
		const QFileDialog::FileMode fileMode = q->fileMode();
		if (fileMode == QFileDialog::DirectoryOnly || fileMode == QFileDialog::Directory)
			q->setWindowTitle(QSshFileDialog::tr("Find Directory"));
		else
			q->setWindowTitle(QSshFileDialog::tr("Open"));
	} else
		q->setWindowTitle(QSshFileDialog::tr("Save As"));

	setWindowTitle = q->windowTitle();
}

void QSshFileDialogPrivate::retranslateStrings()
{
	Q_Q(QSshFileDialog);
	/* WIDGETS */
	if (defaultFileTypes)
		q->setNameFilter(QSshFileDialog::tr("All Files (*)"));

	QList<QAction *> actions          = ui->treeView->header()->actions();
	QAbstractItemModel *abstractModel = fsModel;
	int total                         = qMin(abstractModel->columnCount(QModelIndex()), actions.count() + 1);
	for (int i = 1; i < total; ++i) {
		actions.at(i - 1)->setText(QSshFileDialog::tr("Show ") +
		                           abstractModel->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString());
	}

	/* MENU ACTIONS */
	renameAction->setText(QSshFileDialog::tr("&Rename"));
	deleteAction->setText(QSshFileDialog::tr("&Delete"));
	showHiddenAction->setText(QSshFileDialog::tr("Show &hidden files"));
	newFolderAction->setText(QSshFileDialog::tr("&New Folder"));
	ui->retranslateUi(q);
	updateLookInLabel();
	updateFileNameLabel();
	updateFileTypeLabel();
	updateCancelButtonText();
}

void QSshFileDialogPrivate::emitFilesSelected(const QStringList &files)
{
	Q_Q(QSshFileDialog);
	emit q->filesSelected(files);
	if (files.count() == 1)
		emit q->fileSelected(files.first());
}

void QSshFileDialogPrivate::createWidgets()
{
	if (ui)
		return;
	Q_Q(QSshFileDialog);

	ui = new Ui_QSshFileDialog();
	ui->setupUi(q);

	QList<QUrl> initialBookmarks;
	initialBookmarks << QUrl(QLatin1String("file:")) << QUrl::fromLocalFile(QDir::homePath());

	q->connect(ui->buttonBox, &QDialogButtonBox::accepted, q, [q]() { q->accept(); });
	q->connect(ui->buttonBox, &QDialogButtonBox::rejected, q, [q]() { q->reject(); });

	ui->lookInCombo->setInsertPolicy(QComboBox::NoInsert);
	ui->lookInCombo->setDuplicatesEnabled(false);

	// filename
	ui->fileNameEdit->setFileDialogPrivate(this);
	ui->fileNameLabel->setBuddy(ui->fileNameEdit);

	ui->fileNameEdit->setInputMethodHints(Qt::ImhNoPredictiveText);

	q->connect(ui->fileNameEdit, &QSshFileDialogLineEdit::textChanged, q,
	           [this](const QString &text) { _q_autoCompleteFileName(text); });
	q->connect(ui->fileNameEdit, &QSshFileDialogLineEdit::textChanged, q, [this]() { _q_updateOkButton(); });
	q->connect(ui->fileNameEdit, &QSshFileDialogLineEdit::returnPressed, q, [q]() { q->accept(); });

	// filetype
	ui->fileTypeCombo->setDuplicatesEnabled(false);
	ui->fileTypeCombo->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);
	ui->fileTypeCombo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	q->connect(ui->fileTypeCombo, qOverload<int>(&QComboBox::activated), q,
	           [this](int index) { _q_useNameFilter(index); });
	q->connect(ui->fileTypeCombo, qOverload<const QString &>(&QComboBox::activated), q, &QSshFileDialog::filterSelected);
	ui->listView->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->listView->setWrapping(true);
	ui->listView->setResizeMode(QListView::Adjust);
	ui->listView->setEditTriggers(QAbstractItemView::EditKeyPressed);
	ui->listView->setContextMenuPolicy(Qt::CustomContextMenu);
#ifndef QT_NO_DRAGANDDROP
	ui->listView->setDragDropMode(QAbstractItemView::InternalMove);
#endif

	q->connect(ui->listView, &QListView::activated, q, [&](QModelIndex index) { _q_enterDirectory(index); });
	q->connect(ui->listView, &QListView::customContextMenuRequested, q,
	           [&](QPoint position) { _q_showContextMenu(position); });
	QShortcut *shortcut = new QShortcut(ui->listView);
	shortcut->setKey(QKeySequence(QLatin1String("Delete")));
	q->connect(shortcut, &QShortcut::activated, q, [&]() { _q_deleteCurrent(); });

	// ui->treeView->setFileDialogPrivate(this);
	// in QFileDialog this is done in the custom treeViews setFileDialogPrivate method
	ui->treeView->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->treeView->setRootIsDecorated(false);
	ui->treeView->setItemsExpandable(false);
	ui->treeView->setSortingEnabled(true);
	ui->treeView->header()->setSortIndicator(0, Qt::AscendingOrder);
	ui->treeView->header()->setStretchLastSection(false);
	ui->treeView->setTextElideMode(Qt::ElideMiddle);
	ui->treeView->setEditTriggers(QAbstractItemView::EditKeyPressed);
	ui->treeView->setContextMenuPolicy(Qt::CustomContextMenu);
	ui->treeView->setDragDropMode(QAbstractItemView::InternalMove);

	q->connect(ui->treeView, &QTreeView::activated, q, [&](QModelIndex index) { _q_enterDirectory(index); });
	q->connect(ui->treeView, &QTreeView::customContextMenuRequested, q,
	           [&](QPoint position) { _q_showContextMenu(position); });

	shortcut = new QShortcut(ui->treeView);
	shortcut->setKey(QKeySequence(QLatin1String("Delete")));
	q->connect(shortcut, &QShortcut::activated, q, [&]() { _q_deleteCurrent(); });

	ui->splitter->setStretchFactor(ui->splitter->indexOf(ui->splitter->widget(1)), QSizePolicy::Expanding);

	createToolButtons();
	createMenuActions();

	// Initial widget states from options
	q->setViewMode(static_cast<QFileDialog::ViewMode>(options->viewMode()));

	if (!options->mimeTypeFilters().isEmpty())
		q->setMimeTypeFilters(options->mimeTypeFilters());
	else if (!options->nameFilters().isEmpty())
		q->setNameFilters(options->nameFilters());
	q->selectNameFilter(options->initiallySelectedNameFilter());
	q->setDefaultSuffix(options->defaultSuffix());
	if (options->initiallySelectedFiles().count() == 1)
		q->selectFile(options->initiallySelectedFiles().first().fileName());
	foreach (QUrl url, options->initiallySelectedFiles())
		q->selectUrl(url);
	ui->fileNameEdit->selectAll();
	_q_updateOkButton();
	q->resize(q->sizeHint());
}

void QSshFileDialogPrivate::init(const QUrl &directory, const QString &nameFilter, const QString &caption)
{
	Q_Q(QSshFileDialog);
	if (!caption.isEmpty()) {
		useDefaultCaption = false;
		setWindowTitle    = caption;
		q->setWindowTitle(caption);
	}
	initialDirectory   = directory;
	_initialNameFilter = nameFilter;
	createWidgets();
	q->resize(q->sizeHint());
}

QString QSshFileDialogPrivate::getEnvironmentVariable(const QString &string)
{
#ifdef Q_OS_UNIX
	if (string.size() > 1 && string.startsWith(QLatin1Char('$'))) {
		return QString::fromLocal8Bit(qgetenv(string.mid(1).toLatin1().constData()));
	}
#else
	if (string.size() > 2 && string.startsWith(QLatin1Char('%')) && string.endsWith(QLatin1Char('%'))) {
		return QString::fromLocal8Bit(qgetenv(string.mid(1, string.size() - 2).toLatin1().constData()));
	}
#endif
	return string;
}

int QSshFileDialogPrivate::maxNameLength(const QString &path)
{
#if defined(Q_OS_UNIX)
	return ::pathconf(QFile::encodeName(path).data(), _PC_NAME_MAX);
#elif defined(Q_OS_WIN)
	DWORD maxLength;
	const QString drive = path.left(3);
	if (::GetVolumeInformation(reinterpret_cast<const LPCSTR>(drive.utf16()), NULL, 0, NULL, &maxLength, NULL, NULL,
	                           0) == false)
		return -1;
	return maxLength;
#else
	Q_UNUSED(path);
#endif
	return -1;
}

QAbstractItemView *QSshFileDialogPrivate::currentView() const
{
	if (!ui->stackedWidget)
		return nullptr;
	if (ui->stackedWidget->currentWidget() == ui->listView->parent())
		return ui->listView;
	return ui->treeView;
}

/*!
   \internal

   This is called when the user double clicks on a file with the corresponding
   model item \a index.
*/
void QSshFileDialogPrivate::_q_enterDirectory(const QModelIndex &index)
{
	Q_Q(QSshFileDialog);
	// My Computer or a directory
	QString path = index.data(QSshFileSystemModel::FilePathRole).toString();
	if (path.isEmpty() || fsModel->isDir(index)) {
		const QFileDialog::FileMode fileMode = q->fileMode();
		q->setDirectory(path);
		emit q->directoryEntered(path);
		if (fileMode == QFileDialog::Directory || fileMode == QFileDialog::DirectoryOnly) {
			// ### find out why you have to do both of these.
			ui->fileNameEdit->setText(QString());
			ui->fileNameEdit->clear();
		}
	} else {
		// Do not accept when shift-clicking to multi-select a file in environments with single-click-activation (KDE)
		if (!q->style()->styleHint(QStyle::SH_ItemView_ActivateItemOnSingleClick) ||
		    q->fileMode() != QFileDialog::ExistingFiles || !(QGuiApplication::keyboardModifiers() & Qt::CTRL)) {
			q->accept();
		}
	}
}

void QSshFileDialogPrivate::_q_emitUrlSelected(const QUrl &file)
{
	Q_Q(QSshFileDialog);
	emit q->urlSelected(file);
	if (file.isLocalFile())
		emit q->fileSelected(file.toLocalFile());
}

void QSshFileDialogPrivate::_q_emitUrlsSelected(const QList<QUrl> &files)
{
	Q_Q(QSshFileDialog);
	emit q->urlsSelected(files);
	QStringList localFiles;
	foreach (const QUrl &file, files)
		if (file.isLocalFile())
			localFiles.append(file.toLocalFile());
	if (!localFiles.isEmpty())
		emit q->filesSelected(localFiles);
}

void QSshFileDialogPrivate::_q_nativeCurrentChanged(const QUrl &file)
{
	Q_Q(QSshFileDialog);
	emit q->currentUrlChanged(file);
	if (file.isLocalFile())
		emit q->currentChanged(file.toLocalFile());
}

void QSshFileDialogPrivate::_q_nativeEnterDirectory(const QUrl &directory)
{
	Q_Q(QSshFileDialog);
	emit q->directoryUrlEntered(directory);
	if (!directory.isEmpty()) { // Windows native dialogs occasionally emit signals with empty strings.
		*lastVisitedDir() = directory;
		if (directory.isLocalFile())
			emit q->directoryEntered(directory.toLocalFile());
	}
}

void QSshFileDialogPrivate::_q_goToDirectory(const QString &path)
{
	Q_Q(QSshFileDialog);
	QModelIndex index = ui->lookInCombo->model()->index(ui->lookInCombo->currentIndex(), ui->lookInCombo->modelColumn(),
	                                                    ui->lookInCombo->rootModelIndex());
	QString path2     = path;
	if (!index.isValid())
		index = fsModel->index(path);
	else {
		path2 = index.data(Qt::UserRole + 1).toUrl().toLocalFile();
		index = fsModel->index(path2);
	}
	bool exists = false;
	try {
		QSshFileInfo info = sftp->statFile(path2); // statFile in case file got deleted
		exists            = !info.name.isEmpty();
	} catch (QSshSftp::exception &e) {
		qDebug() << "can not statFile to go to directory: " << path2 << e.what();
	}
	if (exists || path2.isEmpty() || path2 == fsModel->myComputer().toString()) {
		_q_enterDirectory(index);
	} else {
		QString message = QSshFileDialog::tr("%1\nDirectory not found.\nPlease verify the "
		                                     "correct directory name was given.");
		QMessageBox::warning(q, q->windowTitle(), message.arg(path2));
	}
}

void QSshFileDialogPrivate::_q_navigateToParent()
{
	Q_Q(QSshFileDialog);
	QModelIndex root = rootIndex();
	QString newPath  = root.parent().data(QSshFileSystemModel::FilePathRole).toString();
	q->setDirectory(newPath);
	emit q->directoryEntered(newPath);
}

/*!
   \internal
   Creates a new directory, first asking the user for a suitable name.
*/
void QSshFileDialogPrivate::_q_createDirectory()
{
	Q_Q(QSshFileDialog);
	ui->listView->clearSelection();

	QString newFolderString = QSshFileDialog::tr("New Folder");
	QString folderName      = newFolderString;
	QString prefix          = q->directory().absolutePath() + QDir::separator();
	if (fsModel->exists(prefix, folderName)) {
		qlonglong suffix = 2;
		while (fsModel->exists(prefix, folderName)) {
			folderName = newFolderString + QString::number(suffix++);
		}
	}
	QModelIndex parent = rootIndex();
	QModelIndex index  = fsModel->mkdir(parent, folderName);
	if (!index.isValid())
		return;

	index = select(index);
	if (index.isValid()) {
		ui->treeView->setCurrentIndex(index);
		currentView()->edit(index);
	}
}

void QSshFileDialogPrivate::_q_showListView()
{
	ui->listModeButton->setDown(true);
	ui->detailModeButton->setDown(false);
	ui->treeView->hide();
	ui->listView->show();
	ui->stackedWidget->setCurrentWidget(ui->listView->parentWidget());
	ui->listView->doItemsLayout();
}

void QSshFileDialogPrivate::_q_showDetailsView()
{
	ui->listModeButton->setDown(false);
	ui->detailModeButton->setDown(true);
	ui->listView->hide();
	ui->treeView->show();
	ui->stackedWidget->setCurrentWidget(ui->treeView->parentWidget());
	ui->treeView->doItemsLayout();
}

void QSshFileDialogPrivate::_q_showContextMenu(const QPoint &position)
{
	Q_Q(QSshFileDialog);
	QAbstractItemView *view = nullptr;
	if (q->viewMode() == QFileDialog::Detail)
		view = ui->treeView;
	else
		view = ui->listView;
	QModelIndex index = view->indexAt(position);
	index             = index.sibling(index.row(), 0);

	QMenu menu(view);
	if (index.isValid()) {
		// file context menu
		const bool ro = fsModel && fsModel->isReadOnly();
		QFile::Permissions p(index.parent().data(QSshFileSystemModel::FilePermissions).toInt());
		renameAction->setEnabled(!ro && p & QFile::WriteUser);
		menu.addAction(renameAction);
		deleteAction->setEnabled(!ro && p & QFile::WriteUser);
		menu.addAction(deleteAction);
		menu.addSeparator();
	}
	menu.addAction(showHiddenAction);
	if (ui->newFolderButton->isVisible()) {
		newFolderAction->setEnabled(ui->newFolderButton->isEnabled());
		menu.addAction(newFolderAction);
	}
	menu.exec(view->viewport()->mapToGlobal(position));
}

/*!
   \internal
*/
void QSshFileDialogPrivate::_q_renameCurrent()
{
	Q_Q(QSshFileDialog);
	QModelIndex index = ui->listView->currentIndex();
	index             = index.sibling(index.row(), 0);
	if (q->viewMode() == QFileDialog::List)
		ui->listView->edit(index);
	else
		ui->treeView->edit(index);
}

void QSshFileDialogPrivate::_q_deleteCurrent()
{
	Q_Q(QSshFileDialog);
	if (fsModel->isReadOnly())
		return;

	QModelIndexList list = ui->listView->selectionModel()->selectedRows();
	for (int i = list.count() - 1; i >= 0; --i) {
		QModelIndex index = list.at(i);
		if (index == ui->listView->rootIndex())
			continue;

		QString fileName = index.data(QSshFileSystemModel::FileNameRole).toString();
		bool isDir       = fsModel->isDir(index);

		QFile::Permissions p(index.parent().data(QSshFileSystemModel::FilePermissions).toInt());
		if (!(p & QFile::WriteUser) &&
		    (QMessageBox::warning(
		         q, QSshFileDialog::tr("Delete"),
		         QSshFileDialog::tr("'%1' is write protected.\nDo you want to delete it anyway?").arg(fileName),
		         QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::No))
			return;
		else if (QMessageBox::warning(q, QSshFileDialog::tr("Delete"),
		                              QSshFileDialog::tr("Are you sure you want to delete '%1'?").arg(fileName),
		                              QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::No)
			return;

		if (isDir) {
			if (!fsModel->rmDirRecursive(index)) {
				QMessageBox::warning(q, q->windowTitle(), QSshFileDialog::tr("Could not delete directory."));
			}
		} else {
			fsModel->remove(index); // index points to a file
		}
	}
}

void QSshFileDialogPrivate::_q_showHidden()
{
	Q_Q(QSshFileDialog);
	QDir::Filters dirFilters = q->filter();
	if (showHiddenAction->isChecked())
		dirFilters |= QDir::Hidden;
	else
		dirFilters &= ~QDir::Hidden;
	q->setFilter(dirFilters);
}

void QSshFileDialogPrivate::_q_showHeader(QAction *action)
{
	Q_Q(QSshFileDialog);
	QActionGroup *actionGroup = qobject_cast<QActionGroup *>(q->sender());
	ui->treeView->header()->setSectionHidden(actionGroup->actions().indexOf(action) + 1, !action->isChecked());
}

void QSshFileDialogPrivate::_q_navigateForward()
{
	Q_Q(QSshFileDialog);
	if (!currentHistory.isEmpty() && currentHistoryLocation < currentHistory.size() - 1) {
		++currentHistoryLocation;
		QString nextHistory = currentHistory.at(currentHistoryLocation);
		q->setDirectory(nextHistory);
	}
}

void QSshFileDialogPrivate::_q_navigateBackward()
{
	Q_Q(QSshFileDialog);
	if (!currentHistory.isEmpty() && currentHistoryLocation > 0) {
		--currentHistoryLocation;
		QString previousHistory = currentHistory.at(currentHistoryLocation);
		q->setDirectory(previousHistory);
	}
}

void QSshFileDialogPrivate::_q_goHome()
{
	Q_Q(QSshFileDialog);
	q->setDirectory(QDir::homePath());
}

void QSshFileDialogPrivate::_q_pathChanged(const QString &newPath)
{
	Q_Q(QSshFileDialog);
	QString root(fsModel->rootPath());
	ui->toParentButton->setEnabled(!(root == "/")); // "/" is upmost parent
	q->setHistory(q->history());

	if (currentHistoryLocation < 0 ||
	    currentHistory.value(currentHistoryLocation) != QDir::toNativeSeparators(newPath)) {
		while (currentHistoryLocation >= 0 && currentHistoryLocation + 1 < currentHistory.count()) {
			currentHistory.removeLast();
		}
		currentHistory.append(QDir::toNativeSeparators(newPath));
		++currentHistoryLocation;
	}
	ui->forwardButton->setEnabled(currentHistory.size() - currentHistoryLocation > 1);
	ui->backButton->setEnabled(currentHistoryLocation > 0);
}

QSshFileDialog::QSshFileDialog(QSshConnectionManager *manager, const QUrl &url, QWidget *parent, const QString &caption,
                               const QString &directory, const QString &filter)
    : QDialog(parent), d_ptr(new QSshFileDialogPrivate(this, manager, url))

{
	d_ptr->init(QUrl::fromLocalFile(directory), filter, caption);
}

QSshFileDialog::~QSshFileDialog() = default;

void QSshFileDialog::setFileMode(QFileDialog::FileMode mode)
{
	Q_D(QSshFileDialog);
	// Check for existing connections
	if (!d->client->isConnected()) {
		connect(d->client.data(), &QSshClient::connected, this, [this, mode]() { setFileMode(mode); });
		return;
	} else if (!d->sftp->isConnected()) {
		connect(d->sftp, &QSshSftp::connected, this, [this, mode]() { setFileMode(mode); });
		return;
	}
	d->options->setFileMode(static_cast<QFileDialog::FileMode>(mode));

	// keep ShowDirsOnly option in sync with fileMode (BTW, DirectoryOnly is obsolete)
	setOption(QFileDialog::ShowDirsOnly, mode == QFileDialog::DirectoryOnly);

	if (!d->usingWidgets())
		return;

	d->retranslateWindowTitle();

	// set selection mode and behavior
	QAbstractItemView::SelectionMode selectionMode;
	if (mode == QFileDialog::ExistingFiles)
		selectionMode = QAbstractItemView::ExtendedSelection;
	else
		selectionMode = QAbstractItemView::SingleSelection;
	d->ui->listView->setSelectionMode(selectionMode);
	d->ui->treeView->setSelectionMode(selectionMode);
	// set filter
	d->fsModel->setFilter(d->filterForMode(filter()));
	// setup file type for directory
	if (mode == QFileDialog::DirectoryOnly || mode == QFileDialog::Directory) {
		d->ui->fileTypeCombo->clear();
		d->ui->fileTypeCombo->addItem(tr("Directories"));
		d->ui->fileTypeCombo->setEnabled(false);
	}
	d->updateFileNameLabel();
	d->updateOkButtonText();
	d->ui->fileTypeCombo->setEnabled(!testOption(QFileDialog::ShowDirsOnly));
	d->_q_updateOkButton();
}

void QSshFileDialog::setOption(QFileDialog::Option option, bool on)
{
	const QFileDialog::Options previousOptions = options();
	if (!(previousOptions & option) != !on)
		setOptions(previousOptions ^ option);
}

bool QSshFileDialog::testOption(QFileDialog::Option option) const
{
	Q_D(const QSshFileDialog);
	return d->options->testOption(static_cast<QFileDialog::Option>(option));
}

void QSshFileDialog::setOptions(QFileDialog::Options options)
{
	Q_D(QSshFileDialog);

	// Check for existing connections
	if (!d->client->isConnected()) {
		connect(d->client.data(), &QSshClient::connected, this, [this, options]() { setOptions(options); });
		return;
	} else if (!d->sftp->isConnected()) {
		connect(d->sftp, &QSshSftp::connected, this, [this, options]() { setOptions(options); });
		return;
	}

	QFileDialog::Options changed = (options ^ QSshFileDialog::options());
	d->options->setOptions(QFileDialog::Options(int(options)));

	d->ui->newFolderButton->setEnabled(true);
	d->renameAction->setEnabled(true);
	d->deleteAction->setEnabled(true);

	if (changed & QFileDialog::HideNameFilterDetails)
		setNameFilters(d->options->nameFilters());

	if (changed & QFileDialog::ShowDirsOnly)
		setFilter((options & QFileDialog::ShowDirsOnly) ? filter() & ~QDir::Files : filter() | QDir::Files);
}

QFileDialog::Options QSshFileDialog::options() const
{
	Q_D(const QSshFileDialog);
	return QFileDialog::Options(int(d->options->options()));
}

void QSshFileDialog::open(QObject *receiver, const char *member)
{
	Q_D(QSshFileDialog);
	const char *signal =
	    (fileMode() == QFileDialog::ExistingFiles) ? SIGNAL(filesSelected(QStringList)) : SIGNAL(fileSelected(QString));
	connect(this, signal, receiver, member);
	d->signalToDisconnectOnClose   = signal;
	d->receiverToDisconnectOnClose = receiver;
	d->memberToDisconnectOnClose   = member;

	QDialog::open();
}

QFileDialog::FileMode QSshFileDialog::fileMode() const
{
	Q_D(const QSshFileDialog);
	return static_cast<QFileDialog::FileMode>(d->options->fileMode());
}

QStringList QSshFileDialog::selectedFiles() const
{
	Q_D(const QSshFileDialog);

	QStringList files;
	if (!d->fsModel)
		return files;
	const QList<QUrl> userSelectedFiles = d->userSelectedFiles();
	files.reserve(userSelectedFiles.size());
	for (const QUrl &file : userSelectedFiles)
		files.append(file.toLocalFile());
	if (files.isEmpty() && d->usingWidgets()) {
		const QFileDialog::FileMode fm = fileMode();
		if (fm != QFileDialog::ExistingFile && fm != QFileDialog::ExistingFiles)
			files.append(d->rootIndex().data(QSshFileSystemModel::FilePathRole).toString());
	}
	return files;
}

void QSshFileDialog::selectUrl(const QUrl &url)
{
	Q_D(QSshFileDialog);
	if (!url.isValid())
		return;
	selectFile(url.toLocalFile());
}

QList<QUrl> QSshFileDialog::selectedUrls() const
{
	Q_D(const QSshFileDialog);
	QList<QUrl> urls;
	foreach (const QString &file, selectedFiles())
		urls.append(QUrl::fromLocalFile(file));
	return urls;
}

QStringList qt_make_filter_list(const QString &filter)
{
	if (filter.isEmpty())
		return QStringList();

	QString sep(QLatin1String(";;"));
	int i = filter.indexOf(sep, 0);
	if (i == -1) {
		if (filter.indexOf(QLatin1Char('\n'), 0) != -1)
			sep = QLatin1Char('\n');
	}

	return filter.split(sep);
}

void QSshFileDialog::setNameFilter(const QString &filter)
{
	setNameFilters(qt_make_filter_list(filter));
}

QDir::Filters QSshFileDialog::filter() const
{
	Q_D(const QSshFileDialog);
	if (d->usingWidgets())
		return d->fsModel->filter();
	return d->options->filter();
}

void QSshFileDialog::setFilter(QDir::Filters filters)
{
	Q_D(QSshFileDialog);
	d->options->setFilter(filters);
	d->fsModel->setFilter(filters);
	d->showHiddenAction->setChecked((filters & QDir::Hidden));
}

void QSshFileDialog::setViewMode(QFileDialog::ViewMode mode)
{
	Q_D(QSshFileDialog);
	d->options->setViewMode(static_cast<QFileDialog::ViewMode>(mode));
	if (!d->usingWidgets())
		return;
	if (mode == QFileDialog::Detail)
		d->_q_showDetailsView();
	else
		d->_q_showListView();
}

QFileDialog::ViewMode QSshFileDialog::viewMode() const
{
	Q_D(const QSshFileDialog);
	if (!d->usingWidgets())
		return static_cast<QFileDialog::ViewMode>(d->options->viewMode());
	return (d->ui->stackedWidget->currentWidget() == d->ui->listView->parent() ? QFileDialog::List :
	                                                                             QFileDialog::Detail);
}

// FIXME Qt 5.4: Use upcoming QVolumeInfo class to determine this information?
static inline bool isCaseSensitiveFileSystem(const QString &path)
{
	Q_UNUSED(path)
#if defined(Q_OS_WIN)
	// Return case insensitive unconditionally, even if someone has a case sensitive
	// file system mounted, wrongly capitalized drive letters will cause mismatches.
	return false;
#elif defined(Q_OS_OSX)
	return pathconf(QFile::encodeName(path).constData(), _PC_CASE_SENSITIVE); // FIXME do not use QFile
#else
	return true;
#endif
}

// Determine the file name to be set on the line edit from the path
// passed to selectFile() in mode QFileDialog::AcceptSave.
static inline QString fileFromPath(const QString &rootPath, QString path)
{
#if defined(Q_OS_WIN)
	if (path.size() > 1 && !(path.at(1) == ":"))
		return path;
#else
	if (!path.startsWith("/"))
		return path;
#endif
	if (path.startsWith(rootPath, isCaseSensitiveFileSystem(rootPath) ? Qt::CaseSensitive : Qt::CaseInsensitive))
		path.remove(0, rootPath.size());

	if (path.isEmpty())
		return path;

	if (path.at(0) == QLatin1Char('/'))
		path.remove(0, 1);
	return path;
}

void QSshFileDialog::setAcceptMode(QFileDialog::AcceptMode mode)
{
	Q_D(QSshFileDialog);
	// Check for existing connections
	if (!d->client->isConnected()) {
		connect(d->client.data(), &QSshClient::connected, this, [this, mode]() { setAcceptMode(mode); });
		return;
	} else if (!d->sftp->isConnected()) {
		connect(d->sftp, &QSshSftp::connected, this, [this, mode]() { setAcceptMode(mode); });
		return;
	}

	d->options->setAcceptMode(static_cast<QFileDialog::AcceptMode>(mode));
	// clear WA_DontShowOnScreen so that d->canBeNativeDialog() doesn't return false incorrectly
	setAttribute(Qt::WA_DontShowOnScreen, false);
	if (!d->usingWidgets())
		return;
	QDialogButtonBox::StandardButton button =
	    (mode == QFileDialog::AcceptOpen ? QDialogButtonBox::Open : QDialogButtonBox::Save);
	d->ui->buttonBox->setStandardButtons(button | QDialogButtonBox::Cancel);
	d->ui->buttonBox->button(button)->setEnabled(false);
	d->_q_updateOkButton();
	if (mode == QFileDialog::AcceptSave) {
		d->ui->lookInCombo->setEditable(false);
	}
	d->retranslateWindowTitle();
}

QFileDialog::AcceptMode QSshFileDialog::acceptMode() const
{
	Q_D(const QSshFileDialog);
	return static_cast<QFileDialog::AcceptMode>(d->options->acceptMode());
}

void QSshFileDialog::setDefaultSuffix(const QString &suffix)
{
	Q_D(QSshFileDialog);
	d->options->setDefaultSuffix(suffix);
}

QString QSshFileDialog::defaultSuffix() const
{
	Q_D(const QSshFileDialog);
	return d->options->defaultSuffix();
}

void QSshFileDialog::setHistory(const QStringList &paths)
{
	Q_D(QSshFileDialog);
	if (paths.isEmpty())
		return;
	if (d->usingWidgets())
		d->ui->lookInCombo->setHistory(paths);
}

QStringList QSshFileDialog::history() const
{
	Q_D(const QSshFileDialog);
	if (!d->usingWidgets())
		return QStringList();
	QStringList currentHistory = d->ui->lookInCombo->history();
	QString newHistory         = d->fsModel->rootPath();
	if (!currentHistory.contains(newHistory))
		currentHistory << newHistory;
	return currentHistory;
}

QStringList qt_strip_filters(const QStringList &filters)
{
	QStringList strippedFilters;
	QRegExp r(QString::fromLatin1(QSshFileDialogOptions::filterRegExp));
	for (int i = 0; i < filters.count(); ++i) {
		QString filterName;
		int index = r.indexIn(filters[i]);
		if (index >= 0)
			filterName = r.cap(1);
		strippedFilters.append(filterName.simplified());
	}
	return strippedFilters;
}

void QSshFileDialog::setNameFilters(const QStringList &filters)
{
	Q_D(QSshFileDialog);
	d->defaultFileTypes = (filters == QStringList(QSshFileDialog::tr("All Files (*)")));
	QStringList cleanedFilters;
	for (int i = 0; i < filters.count(); ++i) {
		cleanedFilters << filters[i].simplified();
	}
	d->options->setNameFilters(cleanedFilters);

	d->ui->fileTypeCombo->clear();
	if (cleanedFilters.isEmpty())
		return;

	if (testOption(QFileDialog::HideNameFilterDetails))
		d->ui->fileTypeCombo->addItems(qt_strip_filters(cleanedFilters));
	else
		d->ui->fileTypeCombo->addItems(cleanedFilters);

	d->_q_useNameFilter(0);
}

QStringList QSshFileDialog::nameFilters() const
{
	return d_func()->options->nameFilters();
}

void QSshFileDialog::selectNameFilter(const QString &filter)
{
	Q_D(QSshFileDialog);
	d->options->setInitiallySelectedNameFilter(filter);
	if (!d->usingWidgets()) {
		return;
	}
	int i = -1;
	if (testOption(QFileDialog::HideNameFilterDetails)) {
		const QStringList filters = qt_strip_filters(qt_make_filter_list(filter));
		if (!filters.isEmpty())
			i = d->ui->fileTypeCombo->findText(filters.first());
	} else {
		i = d->ui->fileTypeCombo->findText(filter);
	}
	if (i >= 0) {
		d->ui->fileTypeCombo->setCurrentIndex(i);
		d->_q_useNameFilter(d->ui->fileTypeCombo->currentIndex());
	}
}

QString QSshFileDialog::selectedNameFilter() const
{
	Q_D(const QSshFileDialog);
	if (!d->usingWidgets())
		return QString();

	return d->ui->fileTypeCombo->currentText();
}

#ifndef QT_NO_MIMETYPE

static QString nameFilterForMime(const QString &mimeType)
{
	QMimeDatabase db;
	QMimeType mime(db.mimeTypeForName(mimeType));
	if (mime.isValid()) {
		if (mime.isDefault()) {
			return QSshFileDialog::tr("All files (*)");
		} else {
			const QString patterns = mime.globPatterns().join(QLatin1Char(' '));
			return mime.comment() + " (" + patterns + QLatin1Char(')');
		}
	}
	return QString();
}

void QSshFileDialog::setMimeTypeFilters(const QStringList &filters)
{
	Q_D(QSshFileDialog);
	QStringList nameFilters;
	foreach (const QString &mimeType, filters) {
		const QString text = nameFilterForMime(mimeType);
		if (!text.isEmpty())
			nameFilters.append(text);
	}
	setNameFilters(nameFilters);
	d->options->setMimeTypeFilters(filters);
}

QStringList QSshFileDialog::mimeTypeFilters() const
{
	return d_func()->options->mimeTypeFilters();
}

void QSshFileDialog::selectMimeTypeFilter(const QString &filter)
{
	const QString text = nameFilterForMime(filter);
	if (!text.isEmpty())
		selectNameFilter(text);
}

void QSshFileDialog::selectFile(const QString &filename)
{
	Q_D(QSshFileDialog);
	if (filename.isEmpty())
		return;

	// Check for existing connections
	if (!d->client->isConnected()) {
		connect(d->client.data(), &QSshClient::connected, this, [&, filename]() { selectFile(filename); });
		return;
	} else if (!d->sftp->isConnected()) {
		connect(d->sftp, &QSshSftp::connected, this, [&, filename]() { selectFile(filename); });
		return;
	}

	QSshFileInfo info;
	try {
		info = d->sftp->statFile(filename);
	} catch (QSshSftp::exception &e) {
		qDebug() << "can not statFile to selectFile: " << filename << e.what();
		return;
	}
	QString filenamePath = info.absoluteDirPath();
	if (d->fsModel->rootPath() != filenamePath)
		setDirectory(filenamePath);

	QModelIndex index = d->fsModel->index(filename);
	d->ui->listView->selectionModel()->clear();
	if (!isVisible() || !d->ui->fileNameEdit->hasFocus()) {
		d->ui->fileNameEdit->setText(d->fsModel->isDir(index) ? QString() : index.data().toString());
		d->ui->fileNameEdit->setFocus();
	}
}

#endif // QT_NO_MIMETYPE

QString QSshFileDialog::getOpenFileName(QSshConnectionManager *manager, const QUrl &url, QWidget *parent,
                                        const QString &caption, const QString &dir, const QString &filter,
                                        QString *selectedFilter, QFileDialog::Options options)
{
	const QStringList schemes = QStringList("file");
	const QUrl selectedUrl    = getOpenFileUrl(manager, url, parent, caption, QUrl::fromLocalFile(dir), filter,
                                           selectedFilter, options, schemes);
	return selectedUrl.toLocalFile();
}

QUrl QSshFileDialog::getOpenFileUrl(QSshConnectionManager *manager, const QUrl &url, QWidget *parent,
                                    const QString &caption, const QUrl &dir, const QString &filter,
                                    QString *selectedFilter, QFileDialog::Options options,
                                    const QStringList &supportedSchemes)
{
	Q_UNUSED(supportedSchemes);

	QSshFileDialogArgs args;
	args.parent    = parent;
	args.caption   = caption;
	args.directory = QSshFileDialogPrivate::workingDirectory(dir);
	args.selection = QSshFileDialogPrivate::initialSelection(dir);
	args.filter    = filter;
	args.mode      = QFileDialog::ExistingFile;
	args.options   = options;

	QSshFileDialog dialog(manager, url, args);
	if (selectedFilter && !selectedFilter->isEmpty())
		dialog.selectNameFilter(*selectedFilter);
	if (dialog.exec() == QDialog::Accepted) {
		if (selectedFilter)
			*selectedFilter = dialog.selectedNameFilter();
		return dialog.selectedUrls().value(0);
	}
	return QUrl();
}

QString QSshFileDialog::getSaveFileName(QSshConnectionManager *manager, const QUrl &url, QWidget *parent,
                                        const QString &caption, const QString &dir, const QString &filter,
                                        QString *selectedFilter, QFileDialog::Options options)
{
	const QStringList schemes = QStringList("file");
	const QUrl selectedUrl    = getSaveFileUrl(manager, url, parent, caption, QUrl::fromLocalFile(dir), filter,
                                           selectedFilter, options, schemes);
	return selectedUrl.toLocalFile();
}

QUrl QSshFileDialog::getSaveFileUrl(QSshConnectionManager *manager, const QUrl &url, QWidget *parent,
                                    const QString &caption, const QUrl &dir, const QString &filter,
                                    QString *selectedFilter, QFileDialog::Options options,
                                    const QStringList &supportedSchemes)
{
	Q_UNUSED(supportedSchemes);
	QSshFileDialogArgs args;
	args.parent    = parent;
	args.caption   = caption;
	args.directory = QSshFileDialogPrivate::workingDirectory(dir);
	args.selection = QSshFileDialogPrivate::initialSelection(dir);
	args.filter    = filter;
	args.mode      = QFileDialog::AnyFile;
	args.options   = options;

	QSshFileDialog dialog(manager, url, args);
	if (selectedFilter && !selectedFilter->isEmpty())
		dialog.selectNameFilter(*selectedFilter);
	dialog.setAcceptMode(QFileDialog::AcceptSave);
	if (dialog.exec() == QDialog::Accepted) {
		if (selectedFilter)
			*selectedFilter = dialog.selectedNameFilter();
		return dialog.selectedUrls().value(0);
	}
	return QUrl();
}

QString QSshFileDialog::getExistingDirectory(QSshConnectionManager *manager, const QUrl &url, QWidget *parent,
                                             const QString &caption, const QString &dir, QFileDialog::Options options)
{
	const QStringList schemes = QStringList("file");
	const QUrl selectedUrl =
	    getExistingDirectoryUrl(manager, url, parent, caption, QUrl::fromLocalFile(dir), options, schemes);
	return selectedUrl.toLocalFile();
}

QUrl QSshFileDialog::getExistingDirectoryUrl(QSshConnectionManager *manager, const QUrl &url, QWidget *parent,
                                             const QString &caption, const QUrl &dir, QFileDialog::Options options,
                                             const QStringList &supportedSchemes)
{
	Q_UNUSED(supportedSchemes);

	QSshFileDialogArgs args;
	args.parent    = parent;
	args.caption   = caption;
	args.directory = QSshFileDialogPrivate::workingDirectory(dir);
	args.mode      = (options & QFileDialog::ShowDirsOnly ? QFileDialog::DirectoryOnly : QFileDialog::Directory);
	args.options   = options;

	QSshFileDialog dialog(manager, url, args);
	if (dialog.exec() == QDialog::Accepted)
		return dialog.selectedUrls().value(0);
	return QUrl();
}

QStringList QSshFileDialog::getOpenFileNames(QSshConnectionManager *manager, const QUrl &url, QWidget *parent,
                                             const QString &caption, const QString &dir, const QString &filter,
                                             QString *selectedFilter, QFileDialog::Options options)
{
	const QStringList schemes      = QStringList("file");
	const QList<QUrl> selectedUrls = getOpenFileUrls(manager, url, parent, caption, QUrl::fromLocalFile(dir), filter,
	                                                 selectedFilter, options, schemes);
	QStringList fileNames;
	foreach (const QUrl &url, selectedUrls)
		fileNames << url.toLocalFile();
	return fileNames;
}

QList<QUrl> QSshFileDialog::getOpenFileUrls(QSshConnectionManager *manager, const QUrl &url, QWidget *parent,
                                            const QString &caption, const QUrl &dir, const QString &filter,
                                            QString *selectedFilter, QFileDialog::Options options,
                                            const QStringList &supportedSchemes)
{
	Q_UNUSED(supportedSchemes);
	QSshFileDialogArgs args;
	args.parent    = parent;
	args.caption   = caption;
	args.directory = QSshFileDialogPrivate::workingDirectory(dir);
	args.selection = QSshFileDialogPrivate::initialSelection(dir);
	args.filter    = filter;
	args.mode      = QFileDialog::ExistingFiles;
	args.options   = options;

	QSshFileDialog dialog(manager, url, args);
	if (selectedFilter && !selectedFilter->isEmpty())
		dialog.selectNameFilter(*selectedFilter);
	if (dialog.exec() == QDialog::Accepted) {
		if (selectedFilter)
			*selectedFilter = dialog.selectedNameFilter();
		return dialog.selectedUrls();
	}
	return QList<QUrl>();
}

QSshFileDialog::QSshFileDialog(QSshConnectionManager *manager, const QUrl &url, const QSshFileDialogArgs &args)
    : QDialog(args.parent, nullptr), d_ptr(new QSshFileDialogPrivate(this, manager, url))
{
	d_ptr->init(args.directory, args.filter, args.caption);
	setFileMode(args.mode);
	setOptions(args.options);
	selectFile(args.selection);
}

void QSshFileDialog::done(int result)
{
	Q_D(QSshFileDialog);

	QDialog::done(result);

	if (d->receiverToDisconnectOnClose) {
		disconnect(this, d->signalToDisconnectOnClose, d->receiverToDisconnectOnClose, d->memberToDisconnectOnClose);
		d->receiverToDisconnectOnClose = nullptr;
	}
	d->memberToDisconnectOnClose.clear();
	d->signalToDisconnectOnClose.clear();
}

void QSshFileDialog::accept()
{
	Q_D(QSshFileDialog);
	if (!d->usingWidgets()) {
		const QList<QUrl> urls = selectedUrls();
		if (urls.isEmpty())
			return;
		d->_q_emitUrlsSelected(urls);
		if (urls.count() == 1)
			d->_q_emitUrlSelected(urls.first());
		QDialog::accept();
		return;
	}

	QStringList files = selectedFiles();
	if (files.isEmpty())
		return;
	QString lineEditText = d->ui->fileNameEdit->text();
	// "hidden feature" type .. and then enter, and it will move up a dir
	// special case for ".."
	if (lineEditText == QLatin1String("..")) {
		d->_q_navigateToParent();
		const QSignalBlocker blocker(d->ui->fileNameEdit);
		d->ui->fileNameEdit->selectAll();
		return;
	}

	switch (fileMode()) {
		case QFileDialog::DirectoryOnly:
		case QFileDialog::Directory: {
			QString fn = files.first();
			QSshFileInfo info;
			try {
				info = d->sftp->statFile(fn); // statFile in case file got deleted
			} catch (QSshSftp::exception &e) {
				Q_UNUSED(e);
				// here we catch the exception but an exception might not be a mistake here
				// if the file does not exist, it simply means we have to prepend our
				// root path to "name"
			}
			if (!info.exists()) {
#ifndef QT_NO_MESSAGEBOX
				QString message = tr("%1\nDirectory not found.\nPlease verify the "
				                     "correct directory name was given.");
				QMessageBox::warning(this, windowTitle(), message.arg(info.name));
#endif // QT_NO_MESSAGEBOX
				return;
			}
			if (info.isDir()) {
				d->emitFilesSelected(files);
				QDialog::accept();
			}
			return;
		}

		case QFileDialog::AnyFile: {
			QString fn = files.first();
			QSshFileInfo info;
			try {
				info = d->sftp->statFile(fn);
			} catch (QSshSftp::exception &e) {
				Q_UNUSED(e);
				// here we catch the exception but an exception might not be a mistake here
				// if the file does not exist, it simply means we have to prepend our
				// root path to "name"
			}
			if (info.isDir()) {
				setDirectory(info.name);
				return;
			}

			if (!info.exists()) {
				int maxNameLength = d->maxNameLength(info.name);
				if (maxNameLength >= 0 && info.name.length() > maxNameLength)
					return;
			}

			// check if we have to ask for permission to overwrite the file
			if (!info.exists() || testOption(QFileDialog::DontConfirmOverwrite) ||
			    acceptMode() == QFileDialog::AcceptOpen) {
				d->emitFilesSelected(QStringList(fn));
				QDialog::accept();
#ifndef QT_NO_MESSAGEBOX
			} else {
				if (QMessageBox::warning(this, windowTitle(),
				                         tr("%1 already exists.\nDo you want to replace it?").arg(info.name),
				                         QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
					d->emitFilesSelected(QStringList(fn));
					QDialog::accept();
				}
#endif
			}
			return;
		}

		case QFileDialog::ExistingFile:
		case QFileDialog::ExistingFiles:
			for (int i = 0; i < files.count(); ++i) {
				QSshFileInfo info;
				try {
					info = d->sftp->statFile(files.at(i)); // statFile in case file got deleted
				} catch (QSshSftp::exception &e) {
					Q_UNUSED(e);
					// catching is enough because there is a message
				}
				if (!info.exists()) {
#ifndef QT_NO_MESSAGEBOX
					QString message = tr("%1\nFile not found.\nPlease verify the "
					                     "correct file name was given.");
					QMessageBox::warning(this, windowTitle(), message.arg(info.name));
#endif // QT_NO_MESSAGEBOX
					return;
				}
				if (info.isDir()) {
					setDirectory(info.name);
					d->ui->fileNameEdit->clear();
					return;
				}
			}
			d->emitFilesSelected(files);
			QDialog::accept();
			return;
	}
}

/*!
   // FIXME: this is a hack to avoid propagating key press events
   // to the dialog and from there to the "Ok" button
*/
void QSshFileDialogLineEdit::keyPressEvent(QKeyEvent *e)
{
#ifdef QT_KEYPAD_NAVIGATION
	if (QApplication::navigationMode() == Qt::NavigationModeKeypadDirectional) {
		QLineEdit::keyPressEvent(e);
		return;
	}
#endif // QT_KEYPAD_NAVIGATION

	int key = e->key();
	QLineEdit::keyPressEvent(e);

	if (key != Qt::Key_Escape)
		e->accept();
	if (hideOnEsc && (key == Qt::Key_Escape || key == Qt::Key_Return || key == Qt::Key_Enter)) {
		e->accept();
		d_ptr->currentView()->setFocus(Qt::ShortcutFocusReason);
	}
}

void QSshFileDialogComboBox::setFileDialogPrivate(QSshFileDialogPrivate *d_pointer)
{
	d_ptr                  = d_pointer;
	urlModel               = new QSshUrlModel(this);
	urlModel->showFullPath = true;
	urlModel->setFileSystemModel(d_ptr->fsModel);
	setModel(urlModel);
}

void QSshFileDialogComboBox::showPopup()
{
	if (model()->rowCount() > 1)
		QComboBox::showPopup();

	urlModel->setUrls(QList<QUrl>());
	QList<QUrl> list;
	QModelIndex idx = d_ptr->fsModel->index(d_ptr->fsModel->rootPath());
	while (idx.isValid()) {
		QUrl url = QUrl::fromLocalFile(idx.data(QSshFileSystemModel::FilePathRole).toString());
		if (url.isValid())
			list.append(url);
		idx = idx.parent();
	}
	// add "my computer"
	urlModel->addUrls(list, 0);
	idx = model()->index(model()->rowCount() - 1, 0);

	// append history
	QList<QUrl> urls;
	for (int i = 0; i < m_history.count(); ++i) {
		QUrl path = QUrl::fromLocalFile(m_history.at(i));
		if (!urls.contains(path))
			urls.prepend(path);
	}
	if (urls.count() > 0) {
		model()->insertRow(model()->rowCount());
		idx = model()->index(model()->rowCount() - 1, 0);
		// ### TODO maybe add a horizontal line before this
		model()->setData(idx, QSshFileDialog::tr("Recent Places"));
		QStandardItemModel *m = qobject_cast<QStandardItemModel *>(model());
		if (m) {
			Qt::ItemFlags flags = m->flags(idx);
			flags &= ~Qt::ItemIsEnabled;
			m->item(idx.row(), idx.column())->setFlags(flags);
		}
		urlModel->addUrls(urls, -1, false);
	}
	setCurrentIndex(0);

	QComboBox::showPopup();
}

void QSshFileDialogComboBox::setHistory(const QStringList &paths)
{
	m_history = paths;
	// Only populate the first item, showPopup will populate the rest if needed
	QList<QUrl> list;
	QModelIndex idx = d_ptr->fsModel->index(d_ptr->fsModel->rootPath());

	QUrl url(QUrl::fromLocalFile(idx.data(QSshFileSystemModel::FilePathRole).toString()));
	if (url.isValid())
		list.append(url);
	urlModel->setUrls(list);
}

// Exact same as QComboBox::paintEvent(), except we elide the text.
void QSshFileDialogComboBox::paintEvent(QPaintEvent * /*e*/)
{
	QStylePainter painter(this);
	painter.setPen(palette().color(QPalette::Text));

	// draw the combobox frame, focusrect and selected etc.
	QStyleOptionComboBox opt;
	initStyleOption(&opt);

	QRect editRect  = style()->subControlRect(QStyle::CC_ComboBox, &opt, QStyle::SC_ComboBoxEditField, this);
	int size        = editRect.width() - opt.iconSize.width() - 4;
	opt.currentText = opt.fontMetrics.elidedText(opt.currentText, Qt::ElideMiddle, size);
	painter.drawComplexControl(QStyle::CC_ComboBox, opt);

	// draw the icon and text
	painter.drawControl(QStyle::CE_ComboBoxLabel, opt);
}

QSshUrlModel::QSshUrlModel(QObject *parent) : QStandardItemModel(parent), showFullPath(false), fileSystemModel(nullptr)
{}

QStringList QSshUrlModel::mimeTypes() const
{
	return QStringList(QLatin1String("text/uri-list"));
}

QMimeData *QSshUrlModel::mimeData(const QModelIndexList &indexes) const
{
	QList<QUrl> list;
	for (int i = 0; i < indexes.count(); ++i) {
		if (indexes.at(i).column() == 0)
			list.append(indexes.at(i).data(UrlRole).toUrl());
	}
	QMimeData *data = new QMimeData();
	data->setUrls(list);
	return data;
}
#ifndef QT_NO_DRAGANDDROP

bool QSshUrlModel::canDrop(QDragEnterEvent *event)
{
	if (!event->mimeData()->formats().contains(mimeTypes().constFirst()))
		return false;

	const QList<QUrl> list = event->mimeData()->urls();
	for (int i = 0; i < list.count(); ++i) {
		QModelIndex idx = fileSystemModel->index(list.at(0).toLocalFile());
		if (!fileSystemModel->isDir(idx))
			return false;
	}
	return true;
}

bool QSshUrlModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column,
                                const QModelIndex &parent)
{
	if (!data->formats().contains(mimeTypes().constFirst()))
		return false;
	Q_UNUSED(action);
	Q_UNUSED(column);
	Q_UNUSED(parent);
	addUrls(data->urls(), row);
	return true;
}

#endif // QT_NO_DRAGANDDROP

Qt::ItemFlags QSshUrlModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QStandardItemModel::flags(index);
	if (index.isValid()) {
		flags &= ~Qt::ItemIsEditable;
		// ### some future version could support "moving" urls onto a folder
		flags &= ~Qt::ItemIsDropEnabled;
	}

	if (index.data(Qt::DecorationRole).isNull())
		flags &= ~Qt::ItemIsEnabled;

	return flags;
}

bool QSshUrlModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (value.type() == QVariant::Url) {
		QUrl url             = value.toUrl();
		QModelIndex dirIndex = fileSystemModel->index(url.toLocalFile());
		if (showFullPath) {
			QStandardItemModel::setData(index,
			                            fileSystemModel->data(dirIndex, QSshFileSystemModel::FilePathRole).toString());
		} else {
			QStandardItemModel::setData(
			    index, fileSystemModel->data(dirIndex, QSshFileSystemModel::FilePathRole).toString(), Qt::ToolTipRole);
			QStandardItemModel::setData(index, fileSystemModel->data(dirIndex).toString());
		}
		QStandardItemModel::setData(index, fileSystemModel->data(dirIndex, Qt::DecorationRole), Qt::DecorationRole);
		QStandardItemModel::setData(index, url, UrlRole);
		return true;
	}
	return QStandardItemModel::setData(index, value, role);
}

void QSshUrlModel::setUrls(const QList<QUrl> &list)
{
	removeRows(0, rowCount());
	invalidUrls.clear();
	watching.clear();
	addUrls(list, 0);
}

void QSshUrlModel::addUrls(const QList<QUrl> &list, int row, bool move)
{
	if (row == -1)
		row = rowCount();
	row = qMin(row, rowCount());
	for (int i = list.count() - 1; i >= 0; --i) {
		QUrl url = list.at(i);
		if (!url.isValid())
			continue;
		const QString cleanUrl = url.toLocalFile();
		if (!cleanUrl.isEmpty())
			url = QUrl::fromLocalFile(cleanUrl);

		for (int j = 0; move && j < rowCount(); ++j) {
			QString local = index(j, 0).data(UrlRole).toUrl().toLocalFile();
			if (!cleanUrl.compare(local, Qt::CaseSensitive)) {
				removeRow(j);
				if (j <= row)
					row--;
				break;
			}
		}
		row             = qMax(row, 0);
		QModelIndex idx = fileSystemModel->index(cleanUrl);
		if (!fileSystemModel->isDir(idx))
			continue;
		insertRows(row, 1);
		setUrl(index(row, 0), url, idx);
		watching.append(qMakePair(idx, cleanUrl));
	}
}

QList<QUrl> QSshUrlModel::urls() const
{
	QList<QUrl> list;
	for (int i = 0; i < rowCount(); ++i)
		list.append(data(index(i, 0), UrlRole).toUrl());
	return list;
}

void QSshUrlModel::setFileSystemModel(QSshFileSystemModel *model)
{
	if (model == fileSystemModel)
		return;
	if (fileSystemModel != nullptr) {
		disconnect(model, &QSshFileSystemModel::dataChanged, this, &QSshUrlModel::dataChanged);
		disconnect(model, &QSshFileSystemModel::layoutChanged, this, &QSshUrlModel::layoutChanged);
		disconnect(model, &QSshFileSystemModel::rowsRemoved, this, &QSshUrlModel::layoutChanged);
	}
	fileSystemModel = model;
	if (fileSystemModel != nullptr) {
		connect(model, &QSshFileSystemModel::dataChanged, this, &QSshUrlModel::dataChanged);
		connect(model, &QSshFileSystemModel::layoutChanged, this, &QSshUrlModel::layoutChanged);
		connect(model, &QSshFileSystemModel::rowsRemoved, this, &QSshUrlModel::layoutChanged);
	}
	clear();
	insertColumns(0, 1);
}

void QSshUrlModel::dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
	QModelIndex parent = topLeft.parent();
	for (int i = 0; i < watching.count(); ++i) {
		QModelIndex index = watching.at(i).first;
		if (index.model() && topLeft.model()) {
			Q_ASSERT(index.model() == topLeft.model());
		}
		if (index.row() >= topLeft.row() && index.row() <= bottomRight.row() && index.column() >= topLeft.column() &&
		    index.column() <= bottomRight.column() && index.parent() == parent) {
			changed(watching.at(i).second);
		}
	}
}

void QSshUrlModel::layoutChanged()
{
	QStringList paths;
	for (int i = 0; i < watching.count(); ++i)
		paths.append(watching.at(i).second);
	watching.clear();
	for (const auto &path : qAsConst(paths)) {
		QModelIndex newIndex = fileSystemModel->index(path);
		watching.append(QPair<QModelIndex, QString>(newIndex, path));
		if (newIndex.isValid())
			changed(path);
	}
}

void QSshUrlModel::setUrl(const QModelIndex &index, const QUrl &url, const QModelIndex &dirIndex)
{
	setData(index, url, UrlRole);
	if (url.path().isEmpty()) {
		setData(index, fileSystemModel->myComputer());
		setData(index, fileSystemModel->myComputer(Qt::DecorationRole), Qt::DecorationRole);
	} else {
		QString newName;
		if (showFullPath) {
			newName = (dirIndex.data(QSshFileSystemModel::FilePathRole).toString());
		} else {
			newName = dirIndex.data().toString();
		}

		QIcon newIcon = qvariant_cast<QIcon>(dirIndex.data(Qt::DecorationRole));
		if (!dirIndex.isValid()) {
			newIcon = iconProvider.icon(QFileIconProvider::Folder);
			newName = url.toLocalFile();
			if (!invalidUrls.contains(url))
				invalidUrls.append(url);
			// The bookmark is invalid then we set to false the EnabledRole
			setData(index, false, EnabledRole);
		} else {
			// The bookmark is valid then we set to true the EnabledRole
			setData(index, true, EnabledRole);
		}

		// Make sure that we have at least 32x32 images
		const QSize size = newIcon.actualSize(QSize(32, 32));
		if (size.width() < 32) {
			QPixmap smallPixmap = newIcon.pixmap(QSize(32, 32));
			newIcon.addPixmap(smallPixmap.scaledToWidth(32, Qt::SmoothTransformation));
		}

		if (index.data().toString() != newName)
			setData(index, newName);
		QIcon oldIcon = qvariant_cast<QIcon>(index.data(Qt::DecorationRole));
		if (oldIcon.cacheKey() != newIcon.cacheKey())
			setData(index, newIcon, Qt::DecorationRole);
	}
}

void QSshUrlModel::changed(const QString &path)
{
	for (int i = 0; i < rowCount(); ++i) {
		QModelIndex idx = index(i, 0);
		if (idx.data(UrlRole).toUrl().toLocalFile() == path) {
			setData(idx, idx.data(UrlRole).toUrl());
		}
	}
}

QString QSshFSCompleter::pathFromIndex(const QModelIndex &index) const
{
	const QSshFileSystemModel *dirModel = sourceModel;
	QString currentLocation             = dirModel->rootPath();
	QString path                        = index.data(QSshFileSystemModel::FilePathRole).toString();
	if (!currentLocation.isEmpty() && path.startsWith(currentLocation)) {
		if (currentLocation == QDir::separator())
			return path.mid(currentLocation.length());
		if (currentLocation.endsWith(QLatin1Char('/')))
			return path.mid(currentLocation.length());
		else
			return path.mid(currentLocation.length() + 1);
	}
	return index.data(QSshFileSystemModel::FilePathRole).toString();
}

QStringList QSshFSCompleter::splitPath(const QString &path) const
{
	if (path.isEmpty())
		return QStringList(completionPrefix());

	QString pathCopy = path;
	QString sep      = "/";
#if defined(Q_OS_WIN)
	if (pathCopy == QLatin1String("\\") || pathCopy == QLatin1String("\\\\"))
		return QStringList(pathCopy);
	QString doubleSlash(QLatin1String("\\\\"));
	if (pathCopy.startsWith(doubleSlash))
		pathCopy = pathCopy.mid(2);
	else
		doubleSlash.clear();
#elif defined(Q_OS_UNIX)
	bool expanded;
	pathCopy = qt_tildeExpansion(pathCopy, &expanded);
	if (expanded) {
		QSshFileSystemModel *dirModel = sourceModel;
		dirModel->fetchMore(dirModel->index(pathCopy));
	}
#endif

	QRegExp re(QLatin1Char('[') + QRegExp::escape(sep) + QLatin1Char(']'));

	QStringList parts = pathCopy.split(re);
	if (pathCopy[0] == sep[0]) // read the "/" at the beginning as the split removed it
		parts[0] = sep[0];
	bool startsFromRoot = pathCopy[0] == sep[0];
	if (parts.count() == 1 || (parts.count() > 1 && !startsFromRoot)) {
		const QSshFileSystemModel *dirModel = sourceModel;
		QString currentLocation             = dirModel->rootPath();
		if (currentLocation.contains(sep) && path != currentLocation) {
			QStringList currentLocationList = splitPath(currentLocation);
			while (!currentLocationList.isEmpty() && parts.count() > 0 && parts.at(0) == QLatin1String("..")) {
				parts.removeFirst();
				currentLocationList.removeLast();
			}
			if (!currentLocationList.isEmpty() && currentLocationList.last().isEmpty())
				currentLocationList.removeLast();
			return currentLocationList + parts;
		}
	}
	return parts;
}

#include "moc_qssh_file_dialog.cpp"
