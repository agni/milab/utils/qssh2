/* Example illustrating the use of QSshProcess

 * This is the script used to test the remote process.
 * Copy it into a textfile and choose it in the dialog (dialog is only local).
 * The script writes 10 times the date into the file /tmp/qsshprocess.log.
 *
 * If a connection is established a QSshPocess is used otherwise a QProcess

#!/bin/bash
i=1
echo "$(date)" > /tmp/qsshprocess.log
while [ $i -le 10 ]
do
   echo "$(date)" >> /tmp/qsshprocess.log
   echo "."
   i=$(( $i + 1))
   sleep 1
done

exit 10;

 * -------------------------------------------------------------
 *  SECOND SCRIPT
 * -------------------------------------------------------------
 * The next script accepts an argument as filename in which to write the date
 * The filename can be relative or absolute. The directory for the file will be
 * created if it does not exist.

#!/bin/bash
cd $(dirname $0)

fullname=$1
filename=$(basename "$fullname")
dirname=$(dirname "$fullname")
mkdir -p "$dirname"
cd "$dirname"

i=0
while [ $i -le 10 ]
do
    now=$(date)
    echo "$now" >> "$filename"
    i=$(( $i + 1))
done

#exit $?
exit 5000;

*/

#include <QSshConnectionManager>
#include <QSshSftp>
#include <QSshProcess>

#include <QtWidgets>
#include <QDebug>

class Browser : public QWidget
{
	Q_OBJECT
	QLineEdit *url;
	QLineEdit *args;
	QSshConnectionManager *manager;
	QSshProcess *p = qobject_cast<QSshProcess *>(sender());
	QTextEdit *edit;
	QPushButton *execute;
	QPushButton *bConnect;
	QPushButton *terminate;
	QSshProcess *qssh_process{ nullptr };
	QProcess *process{ nullptr };
	QSshClientSharedPtr client;
	QString exe;

public:
	Browser()
	{
		QLayout *vl = new QVBoxLayout(this);
		QLayout *hl = new QHBoxLayout();
		url         = new QLineEdit(this);
		url->setText("localhost");
		args = new QLineEdit(this);
		args->setPlaceholderText(QString("arguments for the command"));

		edit      = new QTextEdit(this);
		execute   = new QPushButton("execute", this);
		bConnect  = new QPushButton("connect", this);
		terminate = new QPushButton("terminate", this);
		terminate->setEnabled(false);

		hl->addWidget(execute);
		hl->addWidget(bConnect);
		hl->addWidget(url);
		hl->addWidget(terminate);
		resize(1000, 700);
		vl->addItem(hl);
		vl->addWidget(args);
		vl->addWidget(edit);
		connect(execute, &QPushButton::clicked, this, &Browser::toExecute);
		connect(bConnect, &QPushButton::clicked, this, &Browser::toConnect);
		connect(terminate, &QPushButton::clicked, this, &Browser::toTerminate);
		manager = new QSshConnectionManager(this);
	}
	~Browser() override {}

private slots:
	void toExecute()
	{
		QFileDialog dialog(this);
		dialog.setFileMode(QFileDialog::ExistingFile);
		dialog.selectFile(exe);
		if (dialog.exec() != QDialog::Accepted)
			return;

		exe = dialog.selectedFiles().constFirst();
		exe.append(QString(" ") + args->text());

		if (bConnect->isEnabled()) {
			if (!process) {
				process = new QProcess(this);
				process->setProcessChannelMode(QProcess::ForwardedChannels);
				connect(process, &QProcess::started, this, &Browser::onStarted);
				connect(process, qOverload<int>(&QProcess::finished), this, &Browser::onFinished);
				connect(process, &QProcess::readyRead, this, &Browser::readAll);
			}
			process->start(exe);
		} else {
			if (!qssh_process) {
				qssh_process = client->openProcessChannel();
				connect(qssh_process, &QSshProcess::connected, this, &Browser::onStarted);
				connect(qssh_process, &QSshProcess::finished, this, &Browser::onFinished);
				connect(qssh_process, &QSshProcess::readyRead, this, &Browser::readAll);
				connect(qssh_process, &QSshProcess::disconnected, this, &Browser::onDisconnected);
			}
			qssh_process->start(exe);
		}
		execute->setEnabled(false);
	}

	void toConnect()
	{
		client = manager->open(QUrl::fromUserInput(url->text()), true);
		connect(client.data(), &QSshClient::connected, this, &Browser::onConnected);
	}

	void onConnected()
	{
		edit->append("connected to host");
		bConnect->setEnabled(false);
	}
	void onStarted()
	{
		edit->append("started");
		terminate->setEnabled(true);
	}

	void onFinished(int exitCode)
	{
		QString txt = tr("finished with exitcode: %1").arg(exitCode);
		edit->append(txt);
		terminate->setEnabled(false);
		execute->setEnabled(true);
	}

	void readAll()
	{
		QSshProcess *p = qobject_cast<QSshProcess *>(sender());
		if (!p)
			return;
		QByteArray data = p->readAll();
		if (data.size() != 0)
			edit->append(data);
		data.clear();
	}

	void toTerminate()
	{
		if (process) {
			process->terminate();
		} else if (qssh_process) {
			qssh_process->terminate();
		}
	}

	void onDisconnected() { edit->append("terminated"); }
};

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	Browser browser;
	browser.show();
	return app.exec();
}

#include "qssh_process.moc"
