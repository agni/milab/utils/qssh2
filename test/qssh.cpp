/* Example connecting to remote host via terminal only.
 *
 * Opens a remote shell.
 */
#include <QCoreApplication>
#include <QSshClient>
#include <QSshProcess>

#include <QIODevice>
#include <QDebug>
#include <QUrl>
#include <QDir>
#include <QFile>
#include <QStringList>
#include <QThread>

#ifdef Q_OS_UNIX
#include <termios.h>
#endif

class QStdio : public QThread
{
	Q_OBJECT
	QFile input, output;
#ifdef Q_OS_UNIX
	termios restore;
#endif
public:
	QStdio()
	{
#ifdef Q_OS_UNIX
		tcgetattr(1, &restore);
#endif
		input.open(stdin, QIODevice::ReadOnly);
		output.open(stdout, QIODevice::WriteOnly);
	}
	~QStdio() override
	{
		input.close();
		output.close();
	}

	void setEchoEnabled(bool echo)
	{
#ifdef Q_OS_UNIX
		if (!echo) {
			termios term;
			memcpy(&term, &restore, sizeof(termios));
			term.c_lflag &= ~(ICANON | ECHO);
			// term.c_iflag &= ~ICRNL;
			term.c_lflag |= ISIG;
			term.c_cc[VMIN]  = 1;
			term.c_cc[VTIME] = 0;
			tcsetattr(1, 0, &term);
		} else
			tcsetattr(1, 0, &restore);
#endif
	}

	void run() override
	{
		while (true) {
			QByteArray line = input.readLine();
			emit newLine(line);
		}
	}

	qint64 write(const char *data)
	{
		qint64 written = output.write(data);
		output.flush();
		return written;
	}

signals:
	void newLine(const QByteArray &input);
};

class QSshTest : public QObject
{
	Q_OBJECT
public:
	QSshTest()
	{
		QUrl u = QUrl::fromUserInput(qApp->arguments().at(1));
		connect(&client, &QSshClient::connected, this, &QSshTest::connected);
		connect(&client, &QSshClient::authenticationRequired, this, &QSshTest::authenticationRequired);
		connect(&client, &QSshClient::error, this, &QSshTest::error);
		client.loadKnownHosts(QDir::homePath() + "/.ssh/known_hosts");
		client.setKeyFiles(QDir::homePath() + "/.ssh/id_dsa.pub", QDir::homePath() + "/.ssh/id_dsa");
		client.connectToHost(u.host(), u.userName());
	}
private slots:
	void connected()
	{
		qDebug() << "connected!";
		chan = client.openProcessChannel();
		chan->requestPty(QSshProcess::Vt102Terminal);
		chan->startShell();
		connect(chan, &QSshProcess::readyRead, this, &QSshTest::readyRead);
		connect(&stdio, &QStdio::newLine, this, &QSshTest::stdioInput);
		stdio.start();
		connect(chan, &QSshProcess::finished, this, &QSshTest::finish);
	}
	void finish(int exit_code)
	{
		qDebug() << __FUNCTION__;
		stdio.quit();
		client.disconnectFromHost();
		QCoreApplication::exit(exit_code);
	}

	void authenticationRequired(const QList<QSshClient::AuthenticationMethod> &availableMethods)
	{
		qDebug() << "Authentications that can continue:";
		foreach (QSshClient::AuthenticationMethod meth, availableMethods) {
			qDebug() << "\t" << meth;
		}
		stdio.write("password:");
		stdio.setEchoEnabled(false);
		QTextStream qin(stdin);
		client.setPassphrase(qin.readLine());
		stdio.setEchoEnabled(true);
	}
	void error(QSshClient::Error error)
	{
		qDebug() << "error" << error;
		if (error == QSshClient::HostKeyUnknownError) {
			QSshKey k = client.hostKey();
			qDebug() << "The authenticity of host" << client.hostName() << "can't be established.";
			qDebug() << k.type << "key fingerprint is:" << k.hash.toHex();
			stdio.write("Are you sure you want to continue connecting (yes/no)?:");
			QTextStream qin(stdin);
			if (qin.readLine().trimmed() == "yes") {
				client.addKnownHost(client.hostName(), k);
			}
		};
	}

	void readyRead() { stdio.write(chan->readAll()); }
	void stdioInput(const QByteArray &input) { chan->write(input); }

private:
	QSshClient client;
	QSshProcess *chan;
	QStdio stdio;
};

int main(int argc, char **argv)
{
	QCoreApplication app(argc, argv);
	if (app.arguments().count() < 2) {
		qDebug("usage: qssh <user>@<hostname> ");
		return 2;
	}
	QSshTest t;

	int ret = app.exec();
	return ret;
}

#include "qssh.moc"
