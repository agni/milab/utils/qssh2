/* Example illustrating the creation and removal of remote files and directories.

 * mkdir and rmdir are used through the QSshFileSystemModel to show how the view is updated.
 * unlink is used directly on the sftp and the view is not updated (unlink removes files without asking)
 * rmDirRec is used with QSshDirDeleter
 */
#include <QSshConnectionManager>
#include <QSshClient>
#include <QSshSftp>
#include <QSshFileSystemModel>
#include <QSshDir>
#include <QSshDirDeleter>

#include <QtWidgets>
#include <QDebug>

class Browser : public QWidget
{
	Q_OBJECT
	QLineEdit *url;
	QTreeView *view;
	QPushButton *bConnect;
	QPushButton *createDir;
	QPushButton *deleteDir;
	QPushButton *deleteDirRec;
	QPushButton *unlinkFile;
	QSshDirDeleter *deleter;
	QSshConnectionManager *manager;
	QSharedPointer<QSshClient> client;
	QSshSftp *sftp;

public:
	Browser() : client(nullptr), sftp(nullptr)
	{
		QLayout *vl = new QVBoxLayout(this);
		QLayout *hl = new QHBoxLayout();
		url         = new QLineEdit(this);
		url->setText("localhost");
		bConnect     = new QPushButton("Connect", this);
		createDir    = new QPushButton("mkdir", this);
		deleteDir    = new QPushButton("rmdir", this);
		deleteDirRec = new QPushButton("rmdir -r", this);
		unlinkFile   = new QPushButton("rm", this);

		connect(bConnect, &QPushButton::clicked, this, &Browser::doConnect);
		hl->addWidget(url);
		hl->addWidget(bConnect);
		hl->addWidget(createDir);
		hl->addWidget(deleteDir);
		hl->addWidget(deleteDirRec);
		hl->addWidget(unlinkFile);
		view = new QTreeView(this);
		resize(1000, 700);
		vl->addItem(hl);
		vl->addWidget(view);

		view->setEnabled(false);
		createDir->setEnabled(false);
		deleteDir->setEnabled(false);
		deleteDirRec->setEnabled(false);
		unlinkFile->setEnabled(false);

		connect(createDir, &QPushButton::clicked, this, &Browser::mkDir);
		connect(deleteDir, &QPushButton::clicked, this, &Browser::rmDir);
		connect(deleteDirRec, &QPushButton::clicked, this, &Browser::rmDirRec);
		connect(unlinkFile, &QPushButton::clicked, this, &Browser::unlink);

		manager = new QSshConnectionManager(this);
		connect(manager, &QSshConnectionManager::connectionFailed, this, &Browser::error);
	}

private slots:
	void doConnect()
	{
		client.reset();

		QUrl url = QUrl::fromUserInput(this->url->text());
		client   = manager->open(url, true);
		connect(client.data(), &QSshClient::connected, this, &Browser::connected);
	}

	void connected()
	{
		if (view->model())
			view->model()->deleteLater();
		sftp                         = client->openSftpChannel();
		QSshFileSystemModel *fsModel = new QSshFileSystemModel(sftp, view);
		/* if the sftp is not open when setting the root path it will be neccessary to connect
		 * to signal rootPathChanged(QString) and then set the rootIndex of the view to the correct index */
		connect(fsModel, &QSshFileSystemModel::rootPathChanged, this, &Browser::onRootPathChanged);
		view->setModel(fsModel);
		view->setRootIndex(fsModel->setRootPath("/homes"));
		deleter = new QSshDirDeleter(sftp, this);
		connect(deleter, &QSshDirDeleter::finished, this, &Browser::rmDirRecFinished);

		view->setEnabled(true);
		createDir->setEnabled(true);
		deleteDir->setEnabled(true);
		deleteDirRec->setEnabled(true);
		unlinkFile->setEnabled(true);
		bConnect->setEnabled(false);
	}

	void error(const QString &client, int /*unused*/, QSshClient::Error error) { qDebug() << "error" << error; }

	void onRootPathChanged()
	{
		const QSshFileSystemModel *m = static_cast<const QSshFileSystemModel *>(view->model());
		view->setRootIndex(m->index(m->rootPath()));
	}

	QString dirPath(QModelIndex index)
	{
		const QSshFileSystemModel *m = static_cast<const QSshFileSystemModel *>(view->model());
		if (!index.isValid())
			return m->rootPath();

		if (m->fileInfo(index).type != QSshFileInfo::FileTypeDirectory)
			index = index.parent();
		return m->data(index, QSshFileSystemModel::FilePathRole).toString();
	}

	void mkDir()
	{
		QModelIndex index = view->selectionModel()->currentIndex();
		Q_ASSERT(dynamic_cast<QSshFileSystemModel *>(view->model()));
		QSshFileSystemModel *m = static_cast<QSshFileSystemModel *>(view->model());
		QModelIndex newDir     = m->mkdir(index, url->text());
		view->setCurrentIndex(newDir);
	}

	void rmDir()
	{
		QModelIndex index = view->selectionModel()->currentIndex();
		if (!index.isValid())
			return;

		Q_ASSERT(dynamic_cast<QSshFileSystemModel *>(view->model()));
		QSshFileSystemModel *m = static_cast<QSshFileSystemModel *>(view->model());
		m->rmDir(view->selectionModel()->currentIndex());
	}

	void rmDirRec()
	{
		QModelIndex index = view->selectionModel()->currentIndex();
		if (!index.isValid())
			return;

		Q_ASSERT(dynamic_cast<const QSshFileSystemModel *>(index.model()));
		const QSshFileSystemModel *m = static_cast<const QSshFileSystemModel *>(index.model());

		deleteDirRec->setEnabled(false);
		deleter->rm(m->data(index, QFileSystemModel::FilePathRole).toString());
	}

	void rmDirRecFinished(int err) { deleteDirRec->setEnabled(true); }
	// unlink removes files without asking
	void unlink()
	{
		QModelIndex index = view->selectionModel()->currentIndex();
		if (!index.isValid())
			return;

		Q_ASSERT(dynamic_cast<const QSshFileSystemModel *>(index.model()));
		const QSshFileSystemModel *m = static_cast<const QSshFileSystemModel *>(index.model());

		try {
			sftp->unlink(m->data(index, QSshFileSystemModel::FilePathRole).toString());
		} catch (QSshSftp::exception e) {
			qDebug() << "error in unlink(): " << e.what();
		}
	}
};

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	Browser browser;
	browser.show();
	return app.exec();
}

#include "qssh_dir.moc"
