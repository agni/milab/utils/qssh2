#include <QSshConnectionManager>
#include <QSshFileDialog>
#include <QSshFile>

#include <QtWidgets>
#include <QDebug>
#include <QtXml>

#include <memory>
#include <utility>

class Browser : public QWidget
{
	Q_OBJECT
	QLineEdit *url{ nullptr };
	QPushButton *bconnect{ nullptr };
	QPushButton *open{ nullptr };
	QPushButton *create{ nullptr };

	QSshConnectionManager *manager{ nullptr };
	QSshSftp *sftp{ nullptr };
	QSshClientSharedPtr client;

public:
	Browser()
	{
		QLayout *vl = new QVBoxLayout(this);
		QLayout *hl = new QHBoxLayout();
		url         = new QLineEdit(this);
		QString host;
#if defined(Q_OS_WIN)
		host = "compute";
#else
		host = "localhost";
#endif
		url->setText(host);

		bconnect = new QPushButton("Connect", this);
		bconnect->setEnabled(true);
		open = new QPushButton("open", this);
		open->setEnabled(false);
		create = new QPushButton("create", this);
		create->setEnabled(false);

		hl->addWidget(url);
		hl->addWidget(bconnect);
		hl->addWidget(open);
		hl->addWidget(create);
		resize(1000, 700);
		vl->addItem(hl);

		manager = new QSshConnectionManager(this);

		connect(bconnect, &QPushButton::clicked, this, &Browser::doConnect);
		connect(open, &QPushButton::clicked, this, &Browser::onOpen);
		connect(manager, &QSshConnectionManager::authenticationCancelled, this, &Browser::connectCancelled);
		connect(create, &QPushButton::clicked, this, &Browser::onCreate);
	}
	~Browser() {}

private slots:
	void doConnect()
	{
		bconnect->setDisabled(true);
		// create client
		if (!client) {
			QUrl url = QUrl::fromUserInput(this->url->text());
			client   = manager->open(url, true);
		}
		// wait for it to be connected
		if (!client->isConnected()) {
			connect(client.data(), &QSshClient::connected, this, [=]() { doConnect(); });
			return;
		}
		disconnect(client.data(), 0, this, 0);
		// create sftp
		if (!sftp)
			sftp = client->openSftpChannel();
		// wait for it to be connected
		if (!sftp->isConnected()) {
			connect(sftp, &QSshSftp::connected, this, [=]() { doConnect(); });
			return;
		}
		disconnect(sftp, 0, this, 0);
		// everything is fine and we can do things now
		open->setEnabled(true);
		create->setEnabled(true);
	}

	void connectCancelled(const QSshClient *client)
	{
		if (client == this->client)
			this->client.reset();
	}

	void onOpen()
	{
		QUrl url             = QUrl::fromUserInput(this->url->text());
		QString filename     = QSshFileDialog::getOpenFileName(manager, url, this, "getFileName", "/homes",
                                                         "ANY files (*.*);;Text files(*.txt)");
		QSshFile *fileHandle = sftp->file(filename);
		fileHandle->open(QIODevice::ReadOnly);
		QByteArray data;
		if (!fileHandle->waitForReadyRead(1000))
			qDebug() << "failed to open " << filename;
		else {
			while (!fileHandle->atEnd() && fileHandle->waitForReadyRead(100)) {
				data.append(fileHandle->read(1024));
			}
			qDebug() << (fileHandle->atEnd() ? "DONE" : "ERROR");
		}
		delete fileHandle;
		QDomDocument dom;
		QString error;
		int line;
		int column;
		if (!dom.setContent(data, &error, &line, &column)) {
			qDebug() << "Error:" << error << "in line " << line << "column" << column;
			return;
		}
		// extract the root
		QDomElement root = dom.documentElement();
		// get root names and attributes
		QString type = root.tagName();
		QString name = root.attribute("name", QString("error getting name"));
		// display root data
		qDebug() << "Type: " << type << " and name: " << name;
		QDomElement component = root.firstChild().toElement();
		// loop while there is a child
		while (!component.isNull()) {
			if ((component.tagName() == "Parameters")) {
				// get first child
				QDomElement child = component.firstChild().toElement();
				while (!child.isNull()) {
					QString name;
					QString value;
					if (child.tagName() == "Param") {
						name  = child.attribute("name", "error getting name");
						value = child.attribute("value", "error getting value");
						qDebug() << QString("Name: %1 has value %2").arg(name).arg(value);
					}
					child = child.nextSibling().toElement();
				}
			}
			component = component.nextSibling().toElement();
		}
	}

	void onCreate()
	{
		QUrl url         = QUrl::fromUserInput(this->url->text());
		QString filename = QSshFileDialog::getSaveFileName(manager, url, this, "Enter Filename of new File", "/homes",
		                                                   "ANY files (*.*);;Text files(*.txt)");
		qDebug() << filename;
		std::unique_ptr<QSshFile> fileHandle(sftp->file(filename));
		fileHandle->open(QIODevice::WriteOnly);
		if (!fileHandle->waitForOpened(5000)) {
			qDebug() << "failed to open " << filename;
			return;
		}
		QDomDocument doc;
		QDomElement root;
		QDomProcessingInstruction header = doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
		doc.appendChild(header);
		root             = doc.createElement("TestDocument");
		QDomElement name = doc.createElement("Name");
		name.setAttribute("VALUE", "This is the value");
		root.appendChild(name);
		doc.appendChild(root);
		fileHandle->write(doc.toByteArray());
	}
};

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	Browser browser;
	browser.show();
	return app.exec();
}
#include "xml_rw.moc"
