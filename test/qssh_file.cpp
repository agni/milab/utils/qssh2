/* Example for QSshFileDialog and QSshFileWatcher.
 *
 * You can either first connect to the host and then open the dialog or open the dialog and
 * then open the connection during display of QSshFileDialog.
 *
 * You can use static functions or create an object regularly and use it.
 *
 * Creation of links is shown (QSshFileDialog does not follow links)
 *
 * Watching files with QSshFileWatcher is shown. If you try to watch a file the QSshFileWatcher watches /homes
 * but connecting to the host is not enforced. Watching starts automatically after connection is established
 */

#include <QSshConnectionManager>
#include <QSshClient>
#include <QSshSftp>
#include <QSshFileSystemModel>
#include <QSshDir>
#include <QSshDirDeleter>
#include <QSshFileWatcher>
#include <QSshFileDialog>

#include <QtWidgets>
#include <QDebug>

class Browser : public QWidget
{
	Q_OBJECT
	QLineEdit *path;
	QTreeView *view;
	QTreeView *view2;
	QPushButton *bconnect;
	QPushButton *watchFile;
	QPushButton *unwatchFile;
	QSshConnectionManager *manager;
	QSshThreadedFileWatcher *watcher = nullptr;
	QSharedPointer<QSshClient> client;
	QSshSftp *sftp;
	QTextBrowser *browser;
	QPushButton *open;
	QPushButton *openLocal;
	QPushButton *link;
	QSshFileSystemModel *fsModel;

public:
	Browser() : client(nullptr), sftp(nullptr)
	{
		QLayout *vl = new QVBoxLayout(this);
		QLayout *hl = new QHBoxLayout();
		path        = new QLineEdit(this);
		QString host;
#if defined(Q_OS_WIN)
		host = "compute";
#else
		host = "colossus";
#endif
		path->setText(host);
		bconnect    = new QPushButton("Connect", this);
		watchFile   = new QPushButton("watch file", this);
		unwatchFile = new QPushButton("unwatch file", this);
		open        = new QPushButton("open", this);
		openLocal   = new QPushButton("open local", this);
		browser     = new QTextBrowser(this);
		link        = new QPushButton("link", this);

		hl->addWidget(path);
		hl->addWidget(bconnect);
		hl->addWidget(watchFile);
		hl->addWidget(unwatchFile);
		hl->addWidget(open);
		hl->addWidget(openLocal);
		hl->addWidget(link);
		view = new QTreeView(this);
		resize(1000, 700);
		vl->addItem(hl);
		vl->addWidget(view);
		vl->addWidget(browser);

		view->setEnabled(false);
		watchFile->setEnabled(false);
		unwatchFile->setEnabled(false);
		open->setEnabled(true);
		link->setEnabled(false);

		connect(bconnect, &QPushButton::clicked, this, &Browser::doConnect);
		connect(watchFile, &QPushButton::clicked, this, &Browser::onWatch);
		connect(unwatchFile, &QPushButton::clicked, this, &Browser::onUnwatch);
		connect(openLocal, &QPushButton::clicked, this, &Browser::onLocal);
		connect(link, &QPushButton::clicked, this, &Browser::onLink);

		manager = new QSshConnectionManager(this);
		connect(open, &QAbstractButton::clicked, this, &Browser::onOpen);
		connect(manager, &QSshConnectionManager::authenticationCancelled, this, &Browser::connectCancelled);
	}

private slots:
	void doConnect()
	{
		QUrl url = QUrl::fromUserInput(this->path->text());
		client   = manager->open(url, true);

		if (client->isConnected())
			connected();
		else
			connect(client.data(), &QSshClient::connected, this, &Browser::connected);
	}

	void connected()
	{
		connect(client.data(), &QSshClient::error, this, &Browser::error);

		if (view->model())
			view->model()->deleteLater();
		sftp = client->openSftpChannel();
		if (!sftp->isConnected())
			connect(sftp, &QSshSftp::connected, this, &Browser::sftpConnected);
		else
			sftpConnected();
	}

	void sftpConnected()
	{
		// create model and display it
		fsModel = new QSshFileSystemModel(sftp, view);
		view->setModel(fsModel);
		fsModel->setRootPath("/homes");
		view->setRootIndex(fsModel->index(fsModel->rootPath()));
		QHeaderView *treeHeader = view->header();
		QFontMetrics fm(view->font());
		treeHeader->resizeSection(0, fm.width(QLatin1String(" some data has really long names so this is big")));
		treeHeader->resizeSection(1, fm.width(QLatin1String("123456")));
		treeHeader->resizeSection(2, fm.width(QLatin1String("123456")));
		treeHeader->resizeSection(3, fm.width(QLatin1String("128.28 bytes")));
		treeHeader->resizeSection(4, fm.width(QLatin1String(" rwx rwx rwx ")));
		treeHeader->resizeSection(5, fm.width(QLatin1String(" 07.07.17 12:00 ")));

		connect(view->selectionModel(), &QItemSelectionModel::currentRowChanged, this,
		        [this](const QModelIndex &current) {
			        path->setText(current.data(QSshFileSystemModel::FilePathRole).toString());
		        });
		QUrl url;
		url.setHost(client->hostName());
		watcher = new QSshThreadedFileWatcher(url, manager, this);
		connect(watcher, &QSshThreadedFileWatcher::update, this, &Browser::onUpdate);

		view->setEnabled(true);
		watchFile->setEnabled(true);
		unwatchFile->setEnabled(true);
		bconnect->setEnabled(false);
		open->setEnabled(true);
		link->setEnabled(true);
	}

	void connectCancelled(const QSshClient *client)
	{
		if (client == this->client)
			this->client.reset();
	}

	void error(QSshClient::Error error) { qDebug() << "error" << error; }

	void onUpdate(const QVector<QSshFileInfo> &infos)
	{
		for (const auto &info : infos)
			browser->append("Name: " + info.name + "\t" + "Size: " + QString::number(info.size));
	}

	void onWatch() { watcher->watch(this->path->text()); }

	void onUnwatch() { watcher->unwatch(this->path->text()); }

	void onOpen()
	{
		QUrl url;
		if (client)
			url.setHost(client->hostName());
		url.setPath(path->text());
		QSshFileDialog *fd = new QSshFileDialog(manager, url, this, "Open Directory");
		fd->setDirectory(url.path());
		fd->setFileMode(QFileDialog::Directory);
		if (fd->exec() == QDialog::Accepted)
			qDebug() << (fd->selectedFiles().constFirst());

		fd->setWindowTitle("Open File");
		fd->selectFile(qgetenv("HOME") + "/.bashrc");
		fd->setFileMode(QFileDialog::ExistingFile);
		if (fd->exec() == QDialog::Accepted)
			qDebug() << (fd->selectedFiles().constFirst());

		qDebug() << QSshFileDialog::getSaveFileName(
		    manager, url, this, "getSaveFileName", "/homes",
		    "Images (*.png *.xpm *.jpg);;Text files (*.txt);;XML files (*.xml);;ANY files (*.*)");

		qDebug() << QSshFileDialog::getExistingDirectory(manager, url, this, "getExistingDirectory", "/homes");

		qDebug() << QSshFileDialog::getOpenFileName(manager, url, this, "getOpenFileName", "/homes",
		                                            "Images (*.png *.xpm *.jpg);;Text files (*.txt);;XML files (*.xml)");
	}

	void onLocal()
	{
		QString dir;
#if defined(Q_OS_WIN)
		dir = "C:Users";
#else
		dir  = "/homes";
#endif
		QFileDialog dialog(this);
		dialog.setFileMode(QFileDialog::Directory);
		dialog.setDirectory("/homes");
		if (dialog.exec() == QDialog::Accepted)
			qDebug() << (dialog.selectedFiles().constFirst());

		dialog.setWindowTitle("Open File");
		dialog.setFileMode(QFileDialog::ExistingFile);
		dialog.selectFile(qgetenv("HOME") + "/.bashrc");
		if (dialog.exec() == QDialog::Accepted)
			qDebug() << (dialog.selectedFiles().constFirst());
	}
	// creates a link of the chosen file in /tmp called testFile
	// QSshFileDialog does not follow symlinks
	void onLink()
	{
		if (!fsModel)
			return;
		sftp->link(fsModel->data(view->currentIndex(), QSshFileSystemModel::FilePathRole).toString(), "/tmp/testFile");
	}
};

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	Browser browser;
	browser.show();
	return app.exec();
}

#include "qssh_file.moc"
