/* Example illustrating how to read and write to a file via ssh
 *
 * It does not use the QSshConnection manager and estasblishes the Connection on its own.
 * It loads known hosts and sets key files manually.
 */

#include <QSshClient>
#include <QSshSftp>
#include <QSshFileSystemModel>
#include <QSshFile>

#include <QDebug>
#include <QtWidgets>

class Browser : public QWidget
{
	Q_OBJECT
	QLineEdit *url;
	QTreeView *view;
	QTextEdit *te;
	QPushButton *bConnect;
	QPushButton *bWrite;

	QSshClient *client;
	QSshSftp *sftp;

public:
	Browser() : client(nullptr), sftp(nullptr)
	{
		QLayout *vl = new QVBoxLayout(this);
		QLayout *hl = new QHBoxLayout();
		url         = new QLineEdit(this);
		url->setText("localhost");
		bConnect = new QPushButton("Connect", this);
		bWrite   = new QPushButton("Save to File", this);
		connect(bConnect, &QPushButton::clicked, this, &Browser::doConnect);
		hl->addWidget(url);
		hl->addWidget(bConnect);
		hl->addWidget(bWrite);

		view = new QTreeView(this);
		resize(1000, 700);
		vl->addItem(hl);
		vl->addWidget(view);

		te = new QTextEdit(this);
		vl->addWidget(te);
		client = new QSshClient(this);

		view->setEnabled(false);
		bWrite->setEnabled(false);

		connect(view, &QTreeView::doubleClicked, this, qOverload<QModelIndex>(&Browser::readFile));
		connect(bWrite, &QPushButton::clicked, this, &Browser::writeToFile);
		connect(client, &QSshClient::connected, this, &Browser::connected);
		connect(client, &QSshClient::authenticationRequired, this, &Browser::authenticationRequired);
		connect(client, &QSshClient::error, this, &Browser::error);
		client->loadKnownHosts(QDir::homePath() + "/.ssh/known_hosts");
		client->setKeyFiles(QDir::homePath() + "/.ssh/id_dsa.pub", QDir::homePath() + "/.ssh/id_dsa");
	}

private slots:
	void doConnect()
	{
		QUrl url = QUrl::fromUserInput(this->url->text());
		client->connectToHost(url.host(), url.userName());
	}

	void connected()
	{
		if (view->model())
			view->model()->deleteLater();
		sftp                         = client->openSftpChannel();
		QSshFileSystemModel *fsModel = new QSshFileSystemModel(sftp, view);

		view->setModel(fsModel);
		view->setRootIndex(fsModel->setRootPath("/homes"));
		connect(fsModel, &QSshFileSystemModel::rootPathChanged, this, &Browser::rootPathChanged);

		view->setEnabled(true);
		bWrite->setEnabled(true);
		bConnect->setEnabled(false);
	}

	void authenticationRequired(const QList<QSshClient::AuthenticationMethod> &availableMethods)
	{
		QString password = QInputDialog::getText(nullptr, QObject::tr("SSH Authentication"), QObject::tr("password:"),
		                                         QLineEdit::Password);
		if (password.isNull())
			client->disconnectFromHost();
		else
			client->setPassphrase(password);
	}

	void error(QSshClient::Error error)
	{
		if (error == QSshClient::HostKeyUnknownError) {
			QSshKey k = client->hostKey();
			client->addKnownHost(client->hostName(), k);
		} else if (error != 0)
			qDebug() << "error" << error;
	}

	void readFile(QModelIndex index)
	{
		readFile(
		    static_cast<QSshFileSystemModel *>(view->model())->data(index, QSshFileSystemModel::FilePathRole).toString());
	}

	void readFile(const QString &path)
	{
		QSshFile *fileHandle = sftp->file(path);
		fileHandle->open(QIODevice::ReadOnly);
		if (!fileHandle->waitForReadyRead(500))
			qDebug() << "failed to open " << path;
		else {
			while (!fileHandle->atEnd() && fileHandle->waitForReadyRead(100)) {
				te->append(fileHandle->read(1024));
			}
			te->append(fileHandle->atEnd() ? "DONE" : "ERROR");
		}
		delete fileHandle;
	}

	void writeToFile()
	{
		QByteArray data(
		    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore "
		    "et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. "
		    "Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit "
		    "amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna "
		    "aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd "
		    "gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\n\n");
		QString path = "/tmp/ssh.txt";

		QSshFile *fileHandle = sftp->file(path);
		fileHandle->open(QIODevice::WriteOnly);
		if (!fileHandle->waitForOpened(500))
			qDebug() << "failed to open " << path;
		else {
			for (int i = 0; i < 100; ++i) {
				fileHandle->write(data, 100);
				fileHandle->waitForBytesWritten(500);
			}
		}
		delete fileHandle;

		// Read what has been written
		te->clear();
		readFile(path);
	}

	void rootPathChanged(const QString &newPath)
	{
		QSshFileSystemModel *model = static_cast<QSshFileSystemModel *>(view->model());
		view->setRootIndex(model->index(newPath));
	}
};

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	Browser browser;
	browser.show();
	return app.exec();
}

#include "qssh_rw.moc"
