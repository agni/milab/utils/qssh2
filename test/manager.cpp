#include <QSshConnectionManager>
#include <QSshClient>

#include <qapplication.h>
#include <qdebug.h>
#include <qurl.h>
#include <qtimer.h>
#include <qthread.h>

#define NUM_THREADS 2
#define NUM_CLIENTS 2

class Client : public QObject
{
	Q_OBJECT
	QSshConnectionManager *mgr = nullptr;

public:
	Client(QSshConnectionManager *mgr) : mgr(mgr) {}

public slots:
	void connect()
	{
		ConnectionInfo info(QUrl::fromUserInput("localhost"));
		auto client = mgr->open(info, true);
		Q_EMIT created(client, client->isConnected());
		if (!client)
			return;
		QObject::connect(client.data(), &QSshClient::connected, this, &Client::onConnected);
		QObject::connect(client.data(), &QSshClient::destroyed, this, &Client::disconnected);
	}
	void onConnected() { qDebug() << "connected" << sender(); }

signals:
	void created(bool valid, bool connected);
	void disconnected();
};

class Test : public QObject
{
	Q_OBJECT

	QSshConnectionManager *mgr;
	QList<QThread *> threads;
	QList<Client *> clients;
	uint num         = 0;
	uint num_created = 0;

public:
	Test(QSshConnectionManager *mgr) : mgr(mgr)
	{
		for (int i = 0; i < NUM_THREADS; ++i) {
			threads.push_back(new QThread(this));
			threads.back()->start();
		}
	}
	~Test() override
	{
		for (auto t : qAsConst(threads)) {
			t->quit();
			t->wait();
		}
		qDeleteAll(clients);
	}

public slots:
	void start()
	{
		for (int i = 0; i < 5; ++i) {
			Client *c;
			clients.push_back(c = new Client(mgr));
			c->moveToThread(threads.at(i % threads.size()));
			connect(c, &Client::created, [=](bool valid, bool connected) {
				qDebug() << "created" << valid << connected;
				num_created += valid ? 1 : 0;

				// clear manager when all clients have been connected
				if (++num == clients.size()) {
					QTimer::singleShot(10000, [=]() {
						qDebug() << "stopping";
						if (num_created)
							mgr->clear();
						else
							Q_EMIT finished();
					});
				}
			});
			connect(c, &Client::disconnected, [=]() {
				qDebug() << "disconnected";
				if (--num_created == 0)
					Q_EMIT finished();
			});

			// trigger connect of client (to be executed in client's thread)
			QTimer::singleShot(0, c, SLOT(connect()));
		}
	}

signals:
	void finished();
};

int main(int argc, char *argv[])
{
	// register types to allow QSshClients to be used in threads
	qRegisterMetaType<QSshClient::Error>();
	qRegisterMetaType<QList<QSshClient::AuthenticationMethod>>();

	QApplication app(argc, argv);
	app.setQuitOnLastWindowClosed(false);

	QSshConnectionManager *manager = new QSshConnectionManager(&app);
	Test test(manager);
	QObject::connect(&test, &Test::finished, &app, &QApplication::quit);

	QTimer::singleShot(0, &test, &Test::start);
	return app.exec();
}

#include "manager.moc"
