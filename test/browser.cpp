/* Basic example to use of a QSshFilesystemModel and a QTreeView to browse the filesystem. */

#include <QSshClient>
#include <QSshSftp>
#include <QSshFileSystemModel>

#include <QDebug>
#include <QtWidgets>
#include <assert.h>

class Browser : public QWidget
{
	Q_OBJECT
	QLineEdit *url;
	QTreeView *view;
	QSshClient *client;

public:
	Browser()
	{
		QLayout *vl = new QVBoxLayout(this);
		QLayout *hl = new QHBoxLayout();
		url         = new QLineEdit(this);
		url->setText("localhost");
		QPushButton *b = new QPushButton("Connect", this);
		connect(b, &QPushButton::clicked, this, &Browser::doConnect);
		hl->addWidget(url);
		hl->addWidget(b);

		view = new QTreeView(this);
		resize(1000, 700);
		vl->addItem(hl);
		vl->addWidget(view);

		client = new QSshClient(this);

		connect(client, &QSshClient::connected, this, &Browser::connected);
		connect(client, &QSshClient::authenticationRequired, this, &Browser::authenticationRequired);
		connect(client, &QSshClient::error, this, &Browser::error);
		client->loadKnownHosts(QDir::homePath() + "/.ssh/known_hosts");
		client->setKeyFiles(QDir::homePath() + "/.ssh/id_dsa.pub", QDir::homePath() + "/.ssh/id_dsa");
	}

	~Browser() override {}

private slots:
	void doConnect()
	{
		QUrl url = QUrl::fromUserInput(this->url->text());
		client->connectToHost(url.host(), url.userName());
	}

	void connected()
	{
		if (view->model())
			view->model()->deleteLater();
		view->setModel(new QSshFileSystemModel(client->openSftpChannel(), this));
		assert(dynamic_cast<QSshFileSystemModel *>(view->model()));
		QSshFileSystemModel *m = static_cast<QSshFileSystemModel *>(view->model());
		m->setRootPath("/homes");
	}

	void authenticationRequired(const QList<QSshClient::AuthenticationMethod> &availableMethods)
	{
		QString password = QInputDialog::getText(nullptr, QObject::tr("SSH Authentication"), QObject::tr("password:"),
		                                         QLineEdit::Password);
		if (password.isNull())
			client->disconnectFromHost();
		else
			client->setPassphrase(password);
	}

	void error(QSshClient::Error error)
	{
		if (error == QSshClient::HostKeyUnknownError) {
			QSshKey k = client->hostKey();
			client->addKnownHost(client->hostName(), k);
		} else if (error != 0)
			qDebug() << "error" << error;
	}
};

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	Browser browser;
	browser.show();
	return app.exec();
}

#include "browser.moc"
