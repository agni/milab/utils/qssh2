/* Example creating two QSshProcesses in a single channel.
 * Opens a shell and can be used to invoke shell commands like cd and ls on the remote server
 */

#include <QSshConnectionManager>
#include <QSshClient>
#include <QSshSftp>
#include <QSshFileSystemModel>
#include <QSshProcess>

#include <QIODevice>
#include <QDebug>
#include <QUrl>
#include <QDir>
#include <QFile>
#include <QStringList>
#include <QThread>

#include <QDebug>
#include <QtWidgets>

class Browser : public QWidget
{
	Q_OBJECT
	QLineEdit *url;
	QLineEdit *cmd;
	QPushButton *bCmd;
	QTreeView *view1;
	QTreeView *view2;
	QTextBrowser *tb1;
	QTextBrowser *tb2;
	QSshConnectionManager *manager;
	QSshClientSharedPtr client;
	QSshProcess *chan1;
	QSshProcess *chan2;

public:
	Browser()
	{
		QLayout *vl  = new QVBoxLayout(this);
		QLayout *hl  = new QHBoxLayout();
		QLayout *hl2 = new QHBoxLayout();
		url          = new QLineEdit(this);
		url->setText("localhost");
		QPushButton *b = new QPushButton("Connect", this);
		connect(b, &QPushButton::clicked, this, &Browser::doConnect);

		cmd  = new QLineEdit(this);
		bCmd = new QPushButton("execute", this);
		hl->addWidget(cmd);
		hl->addWidget(bCmd);
		hl->addWidget(url);
		hl->addWidget(b);

		view1 = new QTreeView(this);
		resize(1000, 700);
		vl->addItem(hl);
		hl2->addWidget(view1);

		view2 = new QTreeView(this);
		resize(1000, 700);
		hl2->addWidget(view2);
		vl->addItem(hl2);

		tb1 = new QTextBrowser(this);
		vl->addWidget(tb1);
		tb2 = new QTextBrowser(this);
		vl->addWidget(tb2);

		manager = new QSshConnectionManager(this);
		connect(manager, &QSshConnectionManager::connectionFailed, this, &Browser::error);
	}

private slots:
	void doConnect()
	{
		client.reset();

		QUrl url = QUrl::fromUserInput(this->url->text());
		client   = manager->open(url, true);
		connect(client.data(), &QSshClient::connected, this, &Browser::connected);
	}

	void connected()
	{
		// first filesystem view
		if (view1->model())
			view1->model()->deleteLater();
		view1->setModel(new QSshFileSystemModel(client->openSftpChannel(), view1));

		// second filesystem view
		if (view2->model())
			view2->model()->deleteLater();
		view2->setModel(new QSshFileSystemModel(client->openSftpChannel(), view2));

		// first process
		chan1 = client->openProcessChannel();
		chan1->requestPty(QSshProcess::Vt102Terminal);
		chan1->startShell();
		connect(chan1, &QSshProcess::readyRead, this, &Browser::readyRead);
		connect(chan1, &QSshProcess::finished, this, &Browser::onFinished);

		// second channel
		chan2 = client->openProcessChannel();
		chan2->requestPty(QSshProcess::Vt102Terminal);
		chan2->startShell();
		connect(chan2, &QSshProcess::readyRead, this, &Browser::readyRead);
		connect(chan2, &QSshProcess::finished, this, &Browser::onFinished);

		connect(bCmd, &QPushButton::pressed, this, &Browser::writeCmd);
	}

	void error(const QString &client, int /*unused*/, QSshClient::Error error) { qDebug() << "error" << error; }

	void readyRead()
	{
		QSshProcess *p = qobject_cast<QSshProcess *>(sender());
		if (!p)
			return;
		QTextBrowser *tb = (p == chan1) ? tb1 : tb2;
		QByteArray data  = p->readAll();
		if (data.size())
			tb->append(data);
	}

	void onFinished(int exit_status)
	{
		QSshProcess *p = qobject_cast<QSshProcess *>(sender());
		if (!p)
			return;
		QTextBrowser *tb = (p == chan1) ? tb1 : tb2;
		tb->append(QString("FINISHED: %1").arg(exit_status));
	}

	void writeCmd()
	{
		QByteArray command = cmd->text().toUtf8() + "\n";
		chan1->write(command);
		chan2->write(command);
	}
};

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	Browser browser;
	browser.show();
	return app.exec();
}

#include "channeltest.moc"
